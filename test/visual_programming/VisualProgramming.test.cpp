#include <catch2/catch.hpp>

#include <QApplication>
#include "visual_programming/VisualProgramming.hpp"

TEST_CASE("VisualProgramming generateCode returns empty string with no components attached", "[visual_programming][VisualProgramming]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    QString result = vp.generateCode();
    QString expected = "";

    REQUIRE(result == expected);
}

#include "visual_programming/view/components/VisualComponentItemIf.hpp"
#include "visual_programming/view/components/VisualComponentItemFunctionCall.hpp"
#include "visual_programming/model/components/VisualComponentIf.hpp"
#include "visual_programming/model/components/VisualComponentFunctionCall.hpp"

TEST_CASE("VisualGraphScene addComponent adds a component", "[visual_programming][view][VisualGraphScene]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualGraphScene *vgs = vp.vgs();

    VisualComponent *vc = new VisualComponentFunctionCall("function",{});
    VisualComponentItem *vci = vc->getViewItem();
    vci->_enabled = true;
    vp.vgs()->addComponent(vci);

    vgs->start()->setNextComponent(vci, vgs->start()->da());

    QString result = vp.generateCode();
    QString expected = "function();\n";

    REQUIRE(result == expected);
}

TEST_CASE("VisualProgramming generateCode returns the correct string when components are attached", "[visual_programming][VisualProgramming]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualComponent *vc1 = new VisualComponentIf("condition");
    VisualComponent *vc2 = new VisualComponentFunctionCall("function",{"arg1","arg2","arg3"});

    VisualComponentItem *vci1 = vc1->getViewItem();
    vci1->_enabled = true;
    vp.vgs()->addComponent(vci1);

    VisualComponentItem *vci2 = vc2->getViewItem();
    vci2->_enabled = true;
    vp.vgs()->addComponent(vci2);

    VisualComponentItemIf *vci1b = dynamic_cast<VisualComponentItemIf*>(vci1);
    vci1b->setNextComponent(vci2, vci1b->daBlock());

    vp.vgs()->start()->setNextComponent(vci1, vp.vgs()->start()->da());

    QString result = vp.generateCode();
    QString expected = "if()\n{\nfunction(, , );\n}\n";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponentPicker method pick adds the component to the VisualGraphScene", "[visual_programming][view][VisualComponentPicker]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualComponentPicker *vcp = vp.vcp();

    VisualComponent *vc = new VisualComponentFunctionCall("function",{});
    vcp->pick(vc->getViewItem());

    VisualComponentItem *vci;

    for(auto i : vp.vgs()->items()) {
        if(dynamic_cast<VisualComponentItemFunctionCall*>(i)) {
            vci = dynamic_cast<VisualComponentItem*>(i);
            break;
        }
    }

    vp.vgs()->start()->setNextComponent(vci, vp.vgs()->start()->da());

    QString result = vp.generateCode();
    QString expected = "function();\n";

    REQUIRE(result == expected);
}
