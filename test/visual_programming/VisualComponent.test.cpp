#include <catch2/catch.hpp>

#include "visual_programming/model/VisualComponent.hpp"

TEST_CASE("VisualComponent method generateCode returns empty string for empty component", "[visual_programming][model][VisualComponent]")
{
    VisualComponent vc;
    QString result = vc.generateCode();
    QString expected = "";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponent method clone returns an object with the same componentType", "[visual_programming][model][VisualComponent]")
{
    VisualComponent vc;
    vc.componentType = ComponentType::DoubleBlock;
    VisualComponent *vc_copy = vc.clone();
    ComponentType result = vc_copy->componentType;
    ComponentType expected = ComponentType::DoubleBlock;

    REQUIRE(result == expected);
    REQUIRE_FALSE(&vc == vc_copy);
}

TEST_CASE("VisualComponent method getViewItem returns a VisualComponentItem object with the clone of the VisualComponent attached", "[visual_programming][model][VisualComponent]")
{
    VisualComponent vc;
    vc.componentType = ComponentType::DoubleBlock;
    VisualComponentItem *vci = vc.getViewItem();
    ComponentType result = vci->_vc->componentType;
    ComponentType expected = ComponentType::DoubleBlock;

    REQUIRE(result == expected);
    REQUIRE_FALSE(&vc == vci->_vc);
}

#include "visual_programming/model/components/VisualComponentFunctionCall.hpp"

TEST_CASE("VisualComponentFunctionCall method generateCode returns string with correct code", "[visual_programming][model][VisualComponentFunctionCall]")
{
    VisualComponentFunctionCall vc("function",{"arg1","arg2","arg3"});
    QString result = vc.generateCode();
    QString expected = "function(arg1, arg2, arg3);";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponentFunctionCall method getViewItem returns a VisualComponentItem object with the clone of the VisualComponent attached", "[visual_programming][model][VisualComponentFunctionCall]")
{
    VisualComponentFunctionCall vc("function",{"arg1","arg2","arg3"});
    VisualComponentItem *vci = vc.getViewItem();
    QString result = vci->_vc->generateCode();
    QString expected = "function(arg1, arg2, arg3);";

    REQUIRE(result == expected);
    REQUIRE_FALSE(&vc == vci->_vc);
}

#include "visual_programming/model/components/VisualComponentIf.hpp"

TEST_CASE("VisualComponentIf method generateCode returns string with correct code", "[visual_programming][model][VisualComponentIf]")
{
    VisualComponentIf vc("x == 5");
    QString result = vc.generateCode();
    QString expected = "if(x == 5)";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponentIf method getViewItem returns a VisualComponentItem object with the clone of the VisualComponent attached", "[visual_programming][model][VisualComponentIf]")
{
    VisualComponentIf vc("x == 5");
    VisualComponentItem *vci = vc.getViewItem();
    QString result = vci->_vc->generateCode();
    QString expected = "if(x == 5)";

    REQUIRE(result == expected);
    REQUIRE_FALSE(&vc == vci->_vc);
}

#include "visual_programming/model/components/VisualComponentVariableAssignment.hpp"

TEST_CASE("VisualComponentVariableAssignment method generateCode returns string with correct code", "[visual_programming][model][VisualComponentVariableAssignment]")
{
    VisualComponentVariableAssignment vc("x","5");
    QString result = vc.generateCode();
    QString expected = "x = 5;";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponentVariableAssignment method getViewItem returns a VisualComponentItem object with the clone of the VisualComponent attached", "[visual_programming][model][VisualComponentVariableAssignment]")
{
    VisualComponentVariableAssignment vc("x","5");
    VisualComponentItem *vci = vc.getViewItem();
    QString result = vci->_vc->generateCode();
    QString expected = "x = 5;";

    REQUIRE(result == expected);
    REQUIRE_FALSE(&vc == vci->_vc);
}

#include "visual_programming/model/components/VisualComponentWhile.hpp"

TEST_CASE("VisualComponentWhile method generateCode returns string with correct code", "[visual_programming][model][VisualComponentWhile]")
{
    VisualComponentWhile vc("i < 10");
    QString result = vc.generateCode();
    QString expected = "while(i < 10)";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponentWhile method getViewItem returns a VisualComponentItem object with the clone of the VisualComponent attached", "[visual_programming][model][VisualComponentWhile]")
{
    VisualComponentWhile vc("i < 10");
    VisualComponentItem *vci = vc.getViewItem();
    QString result = vci->_vc->generateCode();
    QString expected = "while(i < 10)";

    REQUIRE(result == expected);
    REQUIRE_FALSE(&vc == vci->_vc);
}

