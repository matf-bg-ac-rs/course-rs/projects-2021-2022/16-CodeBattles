#include <catch2/catch.hpp>

#include <QApplication>
#include "visual_programming/view/VisualComponentItem.hpp"

TEST_CASE("VisualComponentItem method getModelCode returns empty string for empty component", "[visual_programming][view][VisualComponentItem]")
{
    VisualComponentItem vci;
    QString result = vci.getModelCode();
    QString expected = "";

    REQUIRE(result == expected);
}

TEST_CASE("VisualComponentItem method setNextComponent connect both components", "[visual_programming][view][VisualComponentItem]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualComponentItem *vci1 = new VisualComponentItem(true);
    vp.vgs()->addComponent(vci1);
    VisualComponentItem *vci2 = new VisualComponentItem(true);
    vp.vgs()->addComponent(vci2);

    vci1->setNextComponent(vci2, vci1->da());

    REQUIRE(vci1->nextComponent() == vci2);
}

TEST_CASE("VisualComponentItem method setNextComponent works when nullptr is passed", "[visual_programming][view][VisualComponentItem]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualComponentItem *vci1 = new VisualComponentItem(true);
    vp.vgs()->addComponent(vci1);

    vci1->setNextComponent(nullptr, vci1->da());

    REQUIRE(vci1->nextComponent() == nullptr);
}

TEST_CASE("VisualComponentItem method removeNextComponent disconnects the component", "[visual_programming][view][VisualComponentItem]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualComponentItem *vci1 = new VisualComponentItem(true);
    vp.vgs()->addComponent(vci1);
    VisualComponentItem *vci2 = new VisualComponentItem(true);
    vp.vgs()->addComponent(vci2);

    vci1->setNextComponent(vci2, vci1->da());
    vci1->removeNextComponent(vci1->da());

    REQUIRE(vci1->nextComponent() == nullptr);
}

TEST_CASE("VisualComponentItem and DropArea targetThisArea interaction", "[visual_programming][view][VisualComponentItem][DropArea]")
{
    int argc = 0;
    QApplication a(argc, nullptr);
    VisualProgramming vp(new QGraphicsView(),new QGraphicsView());

    VisualComponentItem *vci = new VisualComponentItem(true);
    vp.vgs()->addComponent(vci);

    REQUIRE(vci->targeted() == false);

    vci->da()->targetThisArea();

    REQUIRE(vci->targeted() == true);

    vci->da()->untargetThisArea();

    REQUIRE(vci->targeted() == false);

}
