#include <catch2/catch.hpp>
#include "src/game_engine/Physics.hpp"
#include "src/game_engine/EntityManager.hpp"

TEST_CASE("Detects collision between two valid entities", "[physics][math]") {
    auto entity_manager = EntityManager{};
    auto a = entity_manager.addEntity(EntityTag::Bot);
    auto a_size = Vec2{50, 50};
    a.addComponent<CBoundingBox>(a_size);
    auto a_pos = Vec2{};
    a.addComponent<CTransform>(a_pos, Vec2{}, 0.0f);
    auto b = entity_manager.addEntity(EntityTag::Bot);
    auto b_size = Vec2{50, 50};
    b.addComponent<CBoundingBox>(b_size);
    auto b_pos = Vec2{10, 10};
    b.addComponent<CTransform>(b_pos, Vec2{}, 0.0f);

    REQUIRE(Physics::isCollision(a, b));
}

TEST_CASE("Detects no collision between two valid entities", "[physics][math]") {
    auto entity_manager = EntityManager{};
    auto a = entity_manager.addEntity(EntityTag::Bot);
    auto a_size = Vec2{50, 50};
    a.addComponent<CBoundingBox>(a_size);
    auto a_pos = Vec2{};
    a.addComponent<CTransform>(a_pos, Vec2{}, 0.0f);
    auto b = entity_manager.addEntity(EntityTag::Bot);
    auto b_size = Vec2{50, 50};
    b.addComponent<CBoundingBox>(b_size);
    auto b_pos = Vec2{100, 10};
    b.addComponent<CTransform>(b_pos, Vec2{}, 0.0f);

    REQUIRE(!Physics::isCollision(a, b));
}

TEST_CASE("Throws with entity missing a bounding box", "[physics][math]") {
    auto entity_manager = EntityManager{};
    auto a = entity_manager.addEntity(EntityTag::Bot);
    auto a_pos = Vec2{};
    a.addComponent<CTransform>(a_pos, Vec2{}, 0.0f);
    auto b = entity_manager.addEntity(EntityTag::Bot);
    auto b_size = Vec2{50, 50};
    b.addComponent<CBoundingBox>(b_size);
    auto b_pos = Vec2{10, 10};
    b.addComponent<CTransform>(b_pos, Vec2{}, 0.0f);

    REQUIRE_THROWS(Physics::isCollision(a, b));
}

TEST_CASE("Throws with entity missing a position", "[physics][math]") {
    auto entity_manager = EntityManager{};
    auto a = entity_manager.addEntity(EntityTag::Bot);
    auto a_size = Vec2{50, 50};
    a.addComponent<CBoundingBox>(a_size);
    auto a_pos = Vec2{};
    a.addComponent<CTransform>(a_pos, Vec2{}, 0.0f);
    auto b = entity_manager.addEntity(EntityTag::Bot);
    auto b_size = Vec2{50, 50};
    b.addComponent<CBoundingBox>(b_size);

    REQUIRE_THROWS(Physics::isCollision(a, b));
}

TEST_CASE("Correctly computes the overlap between two valid entities", "[physics][math]") {
    auto entity_manager = EntityManager{};
    auto a = entity_manager.addEntity(EntityTag::Bot);
    auto a_size = Vec2{50, 50};
    a.addComponent<CBoundingBox>(a_size);
    auto a_pos = Vec2{};
    a.addComponent<CTransform>(a_pos, Vec2{}, 0.0f);
    auto b = entity_manager.addEntity(EntityTag::Bot);
    auto b_size = Vec2{50, 50};
    b.addComponent<CBoundingBox>(b_size);
    auto b_pos = Vec2{10, 10};
    b.addComponent<CTransform>(b_pos, Vec2{}, 0.0f);

    auto overlap = Physics::detectCollision(a, b);
    REQUIRE(overlap.has_value());
    if (overlap.has_value()) {
        auto o = overlap.value();
        REQUIRE(o == Vec2{40.0f, 40.0f});
    }
}
