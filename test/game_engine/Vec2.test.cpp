#include <catch2/catch.hpp>
#include "src/game_engine/Vec2.hpp"

TEST_CASE("Vec2 is created successfully", "[Vec2][math]") {
    SECTION("No args") {
        auto v = Vec2{};
        REQUIRE(v.x() == 0);
        REQUIRE(v.y() == 0);
    }
    SECTION("With args") {
        auto v = Vec2{1, 2};
        REQUIRE(v.x() == 1);
        REQUIRE(v.y() == 2);
    }
}

TEST_CASE("Vec2 can be compared for equality", "[Vec2][math]") {
    auto v = Vec2{1, 2};
    auto u = Vec2{1, 2};
    auto w = Vec2{1, 3};
    auto t = Vec2{3, 2};
    REQUIRE(v == u); // same
    REQUIRE(v != w); // different on second arg
    REQUIRE(v != t); // different on first arg
}

TEST_CASE("Vec2 supports arithmetic operations with Vec2", "[Vec2][math]") {
    SECTION("Addition") {
        auto v = Vec2{1, 2};
        auto u = Vec2{3, 4};

        auto w = v + u;
        REQUIRE(w.x() == Approx(4));
        REQUIRE(w.y() == Approx(6));
    }
    SECTION("Subtraction") {
        auto v = Vec2{1, 2};
        auto u = Vec2{3, 4};

        auto w = v - u;
        REQUIRE(w.x() == Approx(-2));
        REQUIRE(w.y() == Approx(-2));
    }
    SECTION("Multiplication") {
        auto v = Vec2{1, 2};
        auto u = Vec2{3, 4};

        auto w = v * u;
        REQUIRE(w.x() == Approx(3));
        REQUIRE(w.y() == Approx(8));
    }
    SECTION("Division") {
        auto v = Vec2{1, 2};
        auto u = Vec2{3, 4};

        auto w = v / u;
        REQUIRE(w.x() == Approx(1.0f/3.0f));
        REQUIRE(w.y() == Approx(2.0f/4.0f));
    }
}


TEST_CASE("Vec2 supports arithmetic operations with floats", "[Vec2][math]") {
    SECTION("Multiplication") {
        auto v = Vec2{1, 2};
        float k = 3.0f;

        auto w = v * k;
        REQUIRE(w.x() == Approx(3));
        REQUIRE(w.y() == Approx(6));

        w = k * v;
        REQUIRE(w.x() == Approx(3));
        REQUIRE(w.y() == Approx(6));
    }
    SECTION("Division") {
        auto v = Vec2{1, 2};
        float k = 3.0f;

        auto w = v / k;
        REQUIRE(w.x() == Approx(1.0f/3.0f));
        REQUIRE(w.y() == Approx(2.0f/3.0f));
    }
    SECTION("Division by zero") {
        auto v = Vec2{1, 2};
        // it's legal to divide by zero with floating point numbers
        REQUIRE_NOTHROW(v / 0.0f);
    }
}


TEST_CASE("Vec2 correctly computes dot product", "[Vec2][math]") {
    auto v = Vec2{1, 2};
    auto u = Vec2{3, 4};
    auto w = Vec2{};
    auto t = Vec2{0, 1};
    REQUIRE(Vec2::dot(v, u) == Approx(11));
    REQUIRE(Vec2::dot(v, w) == Approx(0));
    REQUIRE(Vec2::dot(v, t) == Approx(2));
}

TEST_CASE("Vec2 correctly computes cross product", "[Vec2][math]") {
    auto v = Vec2{1, 2};
    auto u = Vec2{3, 4};
    auto w = Vec2{};
    auto t = Vec2{0, 1};
    REQUIRE(Vec2::cross(v, u) == Approx(-2));
    REQUIRE(Vec2::cross(v, w) == Approx(0));
    REQUIRE(Vec2::cross(v, t) == Approx(1));
}

TEST_CASE("Vec2 correctly computes abs", "[Vec2][math]") {
    REQUIRE(Vec2::abs(Vec2{1, 2}) == Vec2{1, 2});
    REQUIRE(Vec2::abs(Vec2{-1, 2}) == Vec2{1, 2});
    REQUIRE(Vec2::abs(Vec2{1, -2}) == Vec2{1, 2});
    REQUIRE(Vec2::abs(Vec2{-1, -2}) == Vec2{1, 2});
}

TEST_CASE("Vec2 correctly converts between polar and cartesian coordinates", "[Vec2][math]") {
    auto v = Vec2{1, 2};
    auto u = Vec2::polarToCartesian(Vec2::cartesianToPolar(v));
    REQUIRE(v.x() == Approx(u.x()));
    REQUIRE(v.y() == Approx(u.y()));
}

TEST_CASE("Vec2 correctly computes length", "[Vec2][math]") {
    auto v = Vec2{4, 3};
    REQUIRE(v.length() == Approx(5));
    REQUIRE(v.length() * v.length() == Approx(v.squareLength()));
}

TEST_CASE("Vec2 correctly converts between degrees and radians", "[Vec2][math]") {
    float deg = 90;
    REQUIRE(Vec2::degToRad(deg) == Approx(M_PI / 2));
    REQUIRE(Vec2::radToDeg(Vec2::degToRad(deg)) == Approx(deg));
}
