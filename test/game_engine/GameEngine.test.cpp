#include <catch2/catch.hpp>
#include "src/game_engine/GameEngine.hpp"
#include "Window.hpp"

TEST_CASE("GameEngine is set up successfully", "[game_engine]") {
    int argc = 0;
    QApplication a{argc, nullptr};
    auto window = std::make_unique<Window>();

    GameEngine game_engine(window.get());
    REQUIRE(!game_engine.isRunning());
    REQUIRE(game_engine.window() == window.get());
}

TEST_CASE("GameEngine runs only when done with setup", "[game_engine]") {
    int argc = 0;
    QApplication a{argc, nullptr};
    auto window = std::make_unique<Window>();

    GameEngine game_engine(window.get());
    REQUIRE(!game_engine.isRunning());
    game_engine.run();
    REQUIRE(game_engine.isRunning());
}

TEST_CASE("GameEngine can manipulate assets", "[game_engine]") {
    int argc = 0;
    QApplication a{argc, nullptr};
    auto window = std::make_unique<Window>();

    GameEngine game_engine(window.get());

    SECTION("Access") {
        REQUIRE_NOTHROW(game_engine.assets());
    }
    SECTION("Add program") {
        const std::string program = "swim(10, 10);";
        game_engine.assets().addProgram(program);
        const auto& programs = game_engine.assets().getPrograms();
        REQUIRE(programs.size() == 1);
        REQUIRE(programs.front() == program);
    }
}
