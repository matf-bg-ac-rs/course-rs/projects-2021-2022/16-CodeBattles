#include <catch2/catch.hpp>
#include "src/game_engine/EntityManager.hpp"
#include "algorithm"

auto isEntityTag = [](EntityTag tag) {
    return [=](Entity bot) { return bot.tag() == tag; };
};
auto isBot = isEntityTag(EntityTag::Bot);
auto isBullet = isEntityTag(EntityTag::Bullet);

TEST_CASE("Entity manager is successfully constructed", "[entity][EntityManager]") {
    REQUIRE_NOTHROW(EntityManager{});
}

TEST_CASE("Entity manager successfully adds and retrieves entities", "[entity][EntityManager]") {
    SECTION("Same entities") {
        auto entity_manager = EntityManager{};
        entity_manager.addEntity(EntityTag::Bot);
        entity_manager.addEntity(EntityTag::Bot);

        entity_manager.update();

        auto& entities = entity_manager.getEntities();
        REQUIRE(entities.size() == 2);
        REQUIRE(isBot(entities[0]));
        REQUIRE(isBot(entities[1]));
    }

    SECTION("Different entities") {
        auto entity_manager = EntityManager{};
        entity_manager.addEntity(EntityTag::Bot);
        entity_manager.addEntity(EntityTag::Bullet);

        entity_manager.update();

        auto& entities = entity_manager.getEntities();
        REQUIRE(entities.size() == 2);
        REQUIRE(isBot(entities[0]));
        REQUIRE(isBullet(entities[1]));
    }
}

TEST_CASE("Entity manager successfully adds and retrieves entities by tag", "[entity][EntityManager]") {
    auto entity_manager = EntityManager{};
    entity_manager.addEntity(EntityTag::Bot);
    entity_manager.addEntity(EntityTag::Bullet);
    entity_manager.addEntity(EntityTag::Bot);
    entity_manager.addEntity(EntityTag::Bullet);
    entity_manager.addEntity(EntityTag::Bot);

    entity_manager.update();

    auto& bots = entity_manager.getEntities(EntityTag::Bot);
    auto& bullets = entity_manager.getEntities(EntityTag::Bullet);

    REQUIRE(bots.size() == 3);
    REQUIRE(bullets.size() == 2);
    REQUIRE(std::all_of(bots.begin(), bots.end(), isBot));
    REQUIRE(std::all_of(bullets.begin(), bullets.end(), isBullet));
}

TEST_CASE("Entity manager adds entities only after update", "[entity][EntityManager]") {
    auto entity_manager = EntityManager{};
    entity_manager.addEntity(EntityTag::Bot);
    entity_manager.addEntity(EntityTag::Bullet);
    entity_manager.addEntity(EntityTag::Bot);
    entity_manager.addEntity(EntityTag::Bullet);
    entity_manager.addEntity(EntityTag::Bot);

    auto& entities = entity_manager.getEntities();
    auto& bots = entity_manager.getEntities(EntityTag::Bot);
    auto& bullets = entity_manager.getEntities(EntityTag::Bullet);

    REQUIRE(entities.empty());
    REQUIRE(bots.empty());
    REQUIRE(bullets.empty());

    entity_manager.update();

    entities = entity_manager.getEntities();
    bots = entity_manager.getEntities(EntityTag::Bot);
    bullets = entity_manager.getEntities(EntityTag::Bullet);

    REQUIRE(!entities.empty());
    REQUIRE(!bots.empty());
    REQUIRE(!bullets.empty());
}

TEST_CASE("Entity manager removes destroyed entities", "[entity][EntityManager]") {
    auto entity_manager = EntityManager{};
    entity_manager.addEntity(EntityTag::Bot);
    entity_manager.addEntity(EntityTag::Bullet);
    auto to_die = entity_manager.addEntity(EntityTag::Bot);

    entity_manager.update();

    auto& entities = entity_manager.getEntities();
    auto& bots = entity_manager.getEntities(EntityTag::Bot);
    auto& bullets = entity_manager.getEntities(EntityTag::Bullet);
    REQUIRE(entities.size() == 3);
    REQUIRE(bots.size() == 2);
    REQUIRE(bullets.size() == 1);

    to_die.destroy();

    entity_manager.update();

    entities = entity_manager.getEntities();
    bots = entity_manager.getEntities(EntityTag::Bullet);
    REQUIRE(entities.size() == 2);
    REQUIRE(bots.size() == 1);
    REQUIRE(bullets.size() == 1);
}
