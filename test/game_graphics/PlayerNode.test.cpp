#include <catch2/catch.hpp>

#include <QGraphicsScene>
#include <QDir>

#include "game_graphics/PlayerPixmapNode.hpp"
#include "game_graphics/RocketNode.hpp"
#include "game_graphics/AlienShipNode.hpp"
#include "game_engine/EntityManager.hpp"
#include "Window.hpp"

TEST_CASE("PlayerNode throws an error when it is used if"
          " the entity doesn't have a health component.",
          "[ge_graphics][Node][PlayerNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 60, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{0, 0}, Vec2{0, 0}, 0.0f);

    REQUIRE_THROWS_WITH(node.checkHasHealth(),
                        "PlayerNode's entity needs to have health.");
}

TEST_CASE("PlayerNode doesn't throw an error when accessing health if"
          " the entity has all the required components.",
          "[ge_graphics][Node][PlayerNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 60, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{0, 0}, Vec2{0, 0}, 0.0f);
    bot.addComponent<CHealth>(100);

    SECTION("Checks health")
    {
        REQUIRE_NOTHROW(node.checkHasHealth());
    }
}
