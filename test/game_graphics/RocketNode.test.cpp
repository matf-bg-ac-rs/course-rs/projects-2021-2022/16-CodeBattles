#include <catch2/catch.hpp>

#include <QGraphicsScene>
#include <QDir>

#include "game_graphics/PlayerPixmapNode.hpp"
#include "game_graphics/RocketNode.hpp"
#include "game_graphics/AlienShipNode.hpp"
#include "game_engine/EntityManager.hpp"
#include "Window.hpp"

TEST_CASE("RocketNode constructor doesn't throw an error.",
          "[ge_graphics][Node][RocketNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    SECTION("With an entity without components.")
    {
        Entity bot = manager.addEntity(EntityTag::Bot);

        REQUIRE_NOTHROW(RocketNode{bot});
    }
    SECTION("With an entity with components.")
    {
        Entity bot = manager.addEntity(EntityTag::Bot);
        bot.addComponent<CTransform>(Vec2{0, 0}, Vec2{0, 0}, 0.0f);
        bot.addComponent<CHealth>(100);

        REQUIRE_NOTHROW(RocketNode{bot});
    }
}
