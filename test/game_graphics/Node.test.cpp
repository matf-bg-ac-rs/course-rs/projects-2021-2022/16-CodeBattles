#include <catch2/catch.hpp>

#include <QGraphicsScene>
#include <QDir>

#include "game_graphics/PlayerPixmapNode.hpp"
#include "game_graphics/RocketNode.hpp"
#include "game_graphics/AlienShipNode.hpp"
#include "game_engine/EntityManager.hpp"
#include "Window.hpp"

TEST_CASE("Node's utility functions for corner positions in local"
          " coordiante system return the correct values")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 80, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{1, 2}, Vec2{2, 3}, 0.0f);
    bot.addComponent<CHealth>(100);

    SECTION("Checks minX")
    {
        REQUIRE(node.localMinX() == -30.);
    }
    SECTION("Checks minY")
    {
        REQUIRE(node.localMinY() == -40.);
    }
    SECTION("Checks maxX")
    {
        REQUIRE(node.localMaxX() == 30.);
    }
    SECTION("Checks maxY")
    {
        REQUIRE(node.localMaxY() == 40.);
    }
}

TEST_CASE("Node's boundingRect function"
          "returns the correct values")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 80, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{1, 2}, Vec2{2, 3}, 0.0f);
    bot.addComponent<CHealth>(100);

    auto rect = node.boundingRect();
    REQUIRE(rect.x() == -30);
    REQUIRE(rect.y() == -40);
    REQUIRE(rect.width() == 60);
    REQUIRE(rect.height() == 80);
}
