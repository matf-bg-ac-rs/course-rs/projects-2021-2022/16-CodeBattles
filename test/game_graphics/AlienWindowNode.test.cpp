#include <catch2/catch.hpp>

#include <QGraphicsScene>
#include <QDir>

#include "game_graphics/PlayerPixmapNode.hpp"
#include "game_graphics/RocketNode.hpp"
#include "game_graphics/AlienShipNode.hpp"
#include "game_engine/EntityManager.hpp"
#include "Window.hpp"

TEST_CASE("AlienWindowNode's constructor doesn't throw an error.",
          "[ge_graphics][Node][AlienWindowNode]")
{
    auto parent_x = 10.;
    auto parent_y = 15.;
    auto r1 = 5.;
    auto r2 = 12.;
    auto rotation = 45.;

    REQUIRE_NOTHROW(AlienWindowNode(parent_x, parent_y,
                         r1, r2, rotation, nullptr));
}

TEST_CASE("AlienWindowNode's utility functions for position in parent's"
          " coordiante system return the correct values",
          "[ge_graphics][Node][AlienWindowNode]")
{
    auto parent_x = 10.;
    auto parent_y = 15.;
    auto r1 = 5.;
    auto r2 = 12.;
    auto rotation = 45.;

    AlienWindowNode node(parent_x, parent_y,
                    r1, r2, rotation, nullptr);

    SECTION("Checks X coordinate")
    {
        REQUIRE(node.parentX() == 10.);
    }
    SECTION("Checks Y coordinate")
    {
        REQUIRE(node.parentY() == 15.);
    }
    SECTION("Checks rotation")
    {
        REQUIRE(node.parentRotation() == 45.);
    }
}

TEST_CASE("AlienWindowNode's utility functions for size in the local"
          " coordiante system return the correct values",
          "[ge_graphics][Node][AlienWindowNode]")
{
    auto parent_x = 10.;
    auto parent_y = 15.;
    auto r1 = 5.;
    auto r2 = 12.;
    auto rotation = 45.;

    AlienWindowNode node(parent_x, parent_y,
                    r1, r2, rotation, nullptr);

    SECTION("Checks width")
    {
        REQUIRE(node.localWidth() == 10.);
    }
    SECTION("Checks height")
    {
        REQUIRE(node.localHeight() == 24.);
    }
}

