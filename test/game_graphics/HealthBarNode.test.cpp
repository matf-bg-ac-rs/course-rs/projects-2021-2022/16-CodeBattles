#include <catch2/catch.hpp>

#include <QGraphicsScene>
#include <QDir>

#include "game_graphics/PlayerPixmapNode.hpp"
#include "game_graphics/RocketNode.hpp"
#include "game_graphics/AlienShipNode.hpp"
#include "game_engine/EntityManager.hpp"
#include "Window.hpp"

TEST_CASE("HealthBarNode constructor doesn't throw an error.",
          "[ge_graphics][Node][HealthBarNode]")
{
    auto health = 50;
    auto parent_x = -20;
    auto parent_y = 0;
    auto width = 10;
    auto height = 40;
    auto rotation = 30;

    REQUIRE_NOTHROW(HealthBarNode(health, parent_x, parent_y,
                         width, height, rotation, nullptr));
}

TEST_CASE("HealthBarNode's utility functions for position in parent's"
          " coordiante system return the correct values",
          "[ge_graphics][Node][HealthBarNode]")
{
    auto health = 50;
    auto parent_x = -20.;
    auto parent_y = 0.;
    auto width = 10.;
    auto height = 40.;
    auto rotation = 30.;

    HealthBarNode node(health, parent_x, parent_y,
                         width, height, rotation, nullptr);

    SECTION("Checks X coordinate")
    {
        REQUIRE(node.parentX() == -20.);
    }
    SECTION("Checks Y coordinate")
    {
        REQUIRE(node.parentY() == 0.);
    }
    SECTION("Checks rotation")
    {
        REQUIRE(node.parentRotation() == 30.);
    }
}

TEST_CASE("HealthBarNode's utility functions for size in the local"
          " coordiante system return the correct values",
          "[ge_graphics][Node][HealthBarNode]")
{
    auto health = 50;
    auto parent_x = -20.;
    auto parent_y = 0.;
    auto width = 10.;
    auto height = 40.;
    auto rotation = 30.;

    HealthBarNode node(health, parent_x, parent_y,
                         width, height, rotation, nullptr);

    SECTION("Checks width")
    {
        REQUIRE(node.localWidth() == 10.);
    }
    SECTION("Checks height")
    {
        REQUIRE(node.localHeight() == 40.);
    }
}

