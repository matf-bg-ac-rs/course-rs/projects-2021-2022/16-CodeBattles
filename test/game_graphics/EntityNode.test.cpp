#include <catch2/catch.hpp>

#include <QGraphicsScene>
#include <QDir>

#include "game_graphics/PlayerPixmapNode.hpp"
#include "game_graphics/RocketNode.hpp"
#include "game_graphics/AlienShipNode.hpp"
#include "game_engine/EntityManager.hpp"
#include "Window.hpp"

#define PIX_PATH "../16-CodeBattles/resources/rocket2.png"

TEST_CASE("EntityNode throws an error when it is used if"
          " the entity doesn't have the required components.",
          "[ge_graphics][Node][EntityNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    SECTION("With an entity without components.")
    {
        auto bot = manager.addEntity(EntityTag::Bot);
        auto node = RocketNode{bot};

        REQUIRE_THROWS(node.parentX());
        REQUIRE_THROWS(node.localWidth());
    }
    SECTION("With an entity lacking width and height.")
    {
        auto bot = manager.addEntity(EntityTag::Bot);
        auto node = RocketNode{bot};
        bot.addComponent<CTransform>(Vec2{0, 0}, Vec2{0, 0}, 0.0f);

        REQUIRE_THROWS_WITH(node.boundingRect(),
                       "Node's entity needs to have width and height.");
    }
    SECTION("With an entity lacking position.")
    {
        auto bot = manager.addEntity(EntityTag::Bot);
        auto node = AlienShipNode(bot);
        auto graphics_node = QtGraphicsNode{60, 60, &node};
        bot.addComponent<CQtGraphicsNode>(graphics_node);

        REQUIRE_THROWS_WITH(node.parentX(),
                       "Node's entity needs to have position and rotation.");
    }
}

TEST_CASE("PlayerNode doesn't throw an error when it is used if"
          " the entity has all the required components.",
          "[ge_graphics][Node][EntityNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 60, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{0, 0}, Vec2{0, 0}, 0.0f);
    bot.addComponent<CHealth>(100);

    SECTION("Checks positioning.")
    {
        REQUIRE_NOTHROW(node.checkHasPosition());
    }
    SECTION("Checks dimensions")
    {
        REQUIRE_NOTHROW(node.checkHasDimensions());
    }
}

TEST_CASE("EnitityNode's utility functions for position in parent's"
          " coordiante system return the correct values",
          "[ge_graphics][Node][EntityNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 60, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{1, 2}, Vec2{2, 3}, 0.0f);
    bot.addComponent<CHealth>(100);

    SECTION("Checks X coordinate")
    {
        REQUIRE(node.parentX() == 1.);
    }
    SECTION("Checks Y coordinate")
    {
        REQUIRE(node.parentY() == 2.);
    }
    SECTION("Checks rotation")
    {
         REQUIRE(node.parentRotation() == 0.);
    }
}

TEST_CASE("EntityNode's utility functions for size in the local"
          " coordiante system return the correct values",
          "[ge_graphics][Node][EntityNode]")
{
    int argc = 0;
    QApplication a{argc, nullptr};
    EntityManager manager;

    auto bot = manager.addEntity(EntityTag::Bot);
    auto node = AlienShipNode(bot);
    auto graphics_node = QtGraphicsNode{60, 80, &node};
    bot.addComponent<CQtGraphicsNode>(graphics_node);
    bot.addComponent<CTransform>(Vec2{1, 2}, Vec2{2, 3}, 0.0f);
    bot.addComponent<CHealth>(100);

    SECTION("Checks width")
    {
        REQUIRE(node.localWidth() == 60.);
    }
    SECTION("Checks height")
    {
        REQUIRE(node.localHeight() == 80.);
    }
}

