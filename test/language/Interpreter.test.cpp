﻿#include <catch2/catch.hpp>

#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <antlr4-runtime.h>

#include <language/interpreter/Callable.hpp>
#include <language/interpreter/Interpreter.hpp>
#include <language/ast/ast.hpp>
#include <language/parser/AstBuilder.hpp>
#include <language/parser/generated/CodeBattlesLexer.h>
#include <language/parser/generated/CodeBattlesParser.h>

class AccumulatingPrint : public Callable
{
private:
    std::vector<std::string> outputs_;

public:
    AccumulatingPrint() = default;
    virtual void call(std::shared_ptr<Environment> /* environment */, const std::vector<double>& args) override
    {
        std::stringstream stream;
        if (args.size() > 0) {
            stream << args[0];

            for (std::size_t i = 1; i < args.size(); ++i) {
                stream << ", " << args[i];
            }
        }

        outputs_.push_back(stream.str());
    }

    const std::vector<std::string>& outputs() const
    {
        return outputs_;
    }
};

std::vector<std::string> runInterpreter(const char* program)
{
    Interpreter interpreter{std::string{program}};
    auto print = std::make_shared<AccumulatingPrint>();
    interpreter.link(std::string{"print"}, std::static_pointer_cast<Callable>(print));

    interpreter.interpret();
    return print->outputs();
}

TEST_CASE("Interpreter is constructed", "[language][interpreter][Interpreter]")
{
    Interpreter interpreter{""};
    REQUIRE_NOTHROW(interpreter.interpret());
}

TEST_CASE("Interpreter can link native functions", "[language][interpreter][Interpreter]")
{
    Interpreter interpreter{""};
    interpreter.link(std::string{"print"},
                     std::static_pointer_cast<Callable>(std::make_shared<AccumulatingPrint>()));
}

TEST_CASE("Interpreter can execute an empty statement", "[language][interpreter][Interpreter]")
{
    Interpreter interpreter{";"};
    REQUIRE_NOTHROW(interpreter.interpret());
}

//TEST_CASE("Interpreter can print numbers", "[language][interpreter][Interpreter]")
//{
//    const char* program = \
//            "print(0.1);"
//            "print(1.1);";
//    std::vector<std::string> expected{"0.1", "1.1"};

//    auto output = runInterpreter(program);

//    REQUIRE(output.size() == expected.size());
//    for (std::size_t i = 0; i < expected.size(); ++i)
//        REQUIRE(output[i] == expected[i]);
//}

TEST_CASE("Interpreter can assign and access defined variables", "[language][interpreter][Interpreter]")
{
    const char* program = \
            "x = 5;"
            "print(x);";
    std::vector<std::string> expected{"5"};

    auto output = runInterpreter(program);

    REQUIRE(output.size() == expected.size());
    for (std::size_t i = 0; i < expected.size(); ++i)
        REQUIRE(output[i] == expected[i]);
}

TEST_CASE("Operations are correctly correctly executed", "[language][interpreter][Interpreter][Instruction]")
{
    SECTION("Addition is correctly computed")
    {
        const char* program = \
                "print(1.0 + 1.0);";
        std::vector<std::string> expected{"2"};

        auto output = runInterpreter(program);

        REQUIRE(output.size() == expected.size());
        for (std::size_t i = 0; i < expected.size(); ++i)
            REQUIRE(output[i] == expected[i]);
    }

    SECTION("Subtraction is correctly computed")
    {
        const char* program = \
                "print(3.0 - 1.0);";
        std::vector<std::string> expected{"2"};

        auto output = runInterpreter(program);

        REQUIRE(output.size() == expected.size());
        for (std::size_t i = 0; i < expected.size(); ++i)
            REQUIRE(output[i] == expected[i]);
    }

    SECTION("Multiplication is correctly computed")
    {
        const char* program = \
                "print(15 * 15);";
        std::vector<std::string> expected{"225"};

        auto output = runInterpreter(program);

        REQUIRE(output.size() == expected.size());
        for (std::size_t i = 0; i < expected.size(); ++i)
            REQUIRE(output[i] == expected[i]);
    }

    SECTION("Division is correctly computed")
    {
        SECTION("Normal division is correctly computed")
        {
            const char* program = \
                    "print(5 / 2);";
            std::vector<std::string> expected{"2.5"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }

        SECTION("Division by zero throws an exception")
        {
            const char* program = \
                    "print(15 / 0);";

            REQUIRE_THROWS(runInterpreter(program));
        }
    }

    SECTION("Remainder is correctly computed")
    {
        SECTION("Normal division is correctly computed")
        {
            const char* program = \
                    "print(5 % 2);";
            std::vector<std::string> expected{"1"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }

        SECTION("Remainder when dividing by zero throws an exception")
        {
            const char* program = \
                    "print(15 % 0);";

            REQUIRE_THROWS(runInterpreter(program));
        }
    }

    SECTION("Negation is correctly computed")
    {
        SECTION("Negation of non-zero values is zero")
        {
            SECTION("Negation of one is zero")
            {
                const char* program = \
                        "print(!1);";
                std::vector<std::string> expected{"0"};

                auto output = runInterpreter(program);

                REQUIRE(output.size() == expected.size());
                for (std::size_t i = 0; i < expected.size(); ++i)
                    REQUIRE(output[i] == expected[i]);
            }

            SECTION("Negation of any non-zero value is zero")
            {
                const char* program = \
                        "print(!2);";
                std::vector<std::string> expected{"0"};

                auto output = runInterpreter(program);

                REQUIRE(output.size() == expected.size());
                for (std::size_t i = 0; i < expected.size(); ++i)
                    REQUIRE(output[i] == expected[i]);
            }
        }

        SECTION("Negation of zero is one")
        {
            const char* program = \
                    "print(!0.0);";
            std::vector<std::string> expected{"1"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }

        SECTION("Double negation of a non-zero value is one")
        {
            const char* program = \
                    "print(!!2);";
            std::vector<std::string> expected{"1"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }
    }

    SECTION("Conjunction is correctly computed")
    {
        SECTION("The conjunction table is correctly computed")
        {
            const char* program = \
                    "print(0 && 0);"
                    "print(0 && 1);"
                    "print(1 && 0);"
                    "print(1 && 1);";
            std::vector<std::string> expected{"0", "0", "0", "1"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }

        SECTION("Conjunction is lazily evaluated from left to right")
        {
            const char* program = \
                    "print(0 && (10 / 0));";
            std::vector<std::string> expected{"0"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }
    }

    SECTION("Disjunction is correctly computed")
    {
        SECTION("The disjunction table is correctly computed")
        {
            const char* program = \
                    "print(0 || 0);"
                    "print(0 || 1);"
                    "print(1 || 0);"
                    "print(1 || 1);";
            std::vector<std::string> expected{"0", "1", "1", "1"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }

        SECTION("Disjunction is lazily evaluated from left to right")
        {
            const char* program = \
                    "print(1 || (10 / 0));";
            std::vector<std::string> expected{"1"};

            auto output = runInterpreter(program);

            REQUIRE(output.size() == expected.size());
            for (std::size_t i = 0; i < expected.size(); ++i)
                REQUIRE(output[i] == expected[i]);
        }
    }
}

TEST_CASE("Interpreter correctly runs some example programs", "[language][interpreter][Interpreter]")
{
//    SECTION("Multiple number formats are accepted")
//    {
//        const char* program =
//                "print(0.1); \
//                 print(1.1); \
//                 print(.1); \
//                 print(.0); \
//                 print(1e5); \
//                 print(.5E5); \
//                 print(-.5E5); \
//                 print(20); \
//                 print(-20);";
//         std::vector<std::string> expected{"0.1", "1.1", "0.1", "0", "100000", "50000", "-50000", "20", "-20"};

//         auto output = runInterpreter(program);

//         REQUIRE(output.size() == expected.size());
//         for (std::size_t i = 0; i < expected.size(); ++i)
//             REQUIRE(output[i] == expected[i]);
//    }

    SECTION("Operator precedence is supported")
    {
        const char* program =
                "print(3 + 11 % 5);";
         std::vector<std::string> expected{"4"};

         auto output = runInterpreter(program);

         REQUIRE(output.size() == expected.size());
         for (std::size_t i = 0; i < expected.size(); ++i)
             REQUIRE(output[i] == expected[i]);
    }

    SECTION("else is paired with the 'most recent' if")
    {
        // Explanation:
        // If the `else` is paired with the inner `if`:
        // - numbers smaller than 10 are reduced by 5 (-10 +5)  | 5  -> 0
        // - the number 10 is not changed (-5 +5)               | 10 -> 10
        // - numbers greater than 10 are increased by 5 (-0 +5) | 15 -> 20
        //
        // If the `else` is paired with the outer `if`:
        // - numbers smaller than 10 are reduced by 5 (-10 +5)  | 5 -> 0
        // - the number 10 is increased by 5 (-0 +5)            | 10 -> 15
        // - numbers greater than 10 are not changed            | 15 -> 15
        const char* program =
                "i = 5;"
                "while (i <= 15) {"
                "   n = i;"
                "   if (n <= 10)"
                "       if (n < 10)"
                "           n = n - 10;"
                "       else"
                "           n = n - 5;"
                "   n = n + 5;"
                "   print(n);"
                "   i = i + 5;"
                "}";
        std::vector<std::string> expected{"0", "10", "20"};

        auto output = runInterpreter(program);

        REQUIRE(output.size() == expected.size());
        for (std::size_t i = 0; i < expected.size(); ++i)
            REQUIRE(output[i] == expected[i]);
    }

    SECTION("Conditions can be placed in loops")
    {
        const char* program =
                "i = 0;"
                "while (i < 10) {"
                "   if (i % 2 == 0)"
                "       print(0);"
                "   else"
                "       print(1);"
                "   i = i + 1;"
                "}";
        std::vector<std::string> expected{"0", "1", "0", "1", "0", "1", "0", "1", "0", "1"};

        auto output = runInterpreter(program);

        REQUIRE(output.size() == expected.size());
        for (std::size_t i = 0; i < expected.size(); ++i)
            REQUIRE(output[i] == expected[i]);
    }

    SECTION("The first 10 Fibonacci numbers are correctly computed")
    {
        const char* program = \
                "n = 10;"
                "print(0);"
                "print(1);"
                "prev = 0;"
                "curr = 1;"
                "i = 2;"
                "while (i < n) {"
                "   next = prev + curr;"
                "   prev = curr;"
                "   curr = next;"
                "   i = i + 1;"
                "   print(curr);"
                "}";
        std::vector<std::string> expected{"0", "1", "1", "2", "3", "5", "8", "13", "21", "34"};

        auto output = runInterpreter(program);

        REQUIRE(output.size() == expected.size());
        for (std::size_t i = 0; i < expected.size(); ++i)
            REQUIRE(output[i] == expected[i]);
    }
}
