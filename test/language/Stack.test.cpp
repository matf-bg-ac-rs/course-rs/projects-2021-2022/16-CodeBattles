#include <catch2/catch.hpp>

#include <language/interpreter/Stack.hpp>

TEST_CASE("Stack is constructed", "[language][interpreter][Stack]")
{
    Stack<double> stack;
}

TEST_CASE("Stack can push and pop elements", "[language][interpreter][Stack]")
{
    Stack<double> stack;
    double input = 1.0, expected = 1.0;

    stack.push(input);
    double output = stack.pop();

    REQUIRE(output == expected);
}

TEST_CASE("Stack can push and pop multiple elements", "[language][interpreter][Stack]")
{
    Stack<int> stack;

    for (int i = 1; i <= 10; ++i)
        stack.push(i);

    for (int expected = 10; expected > 0; --expected) {
        int top = stack.pop();
        REQUIRE(top == expected);
    }
}

TEST_CASE("Stack throws when trying to pop when it is empty", "[language][interpreter][Stack]")
{
    Stack<int> stack;
    REQUIRE_THROWS(stack.pop());
}

TEST_CASE("Stack knows its size", "[language][interpreter][Stack]")
{
    Stack<int> stack;

    SECTION("Empty stack")
    {
        REQUIRE(stack.empty());
        REQUIRE(stack.size() == 0);
    }

    SECTION("Stack with elements")
    {
        for (int i = 0; i < 10; ++i)
            stack.push(i);

        REQUIRE(!stack.empty());
        REQUIRE(stack.size() == 10);
    }

    SECTION("Alternating push and pop")
    {
        for (int i = 0; i < 10; ++i) {
            stack.push(2 * i);
            stack.push(2 * i + 1);
            stack.pop();
        }

        REQUIRE(!stack.empty());
        REQUIRE(stack.size() == 10);
    }
}

TEST_CASE("Stack can be cleared", "[language][interpreter][Stack]")
{
    Stack<int> stack;

    for (int i = 0; i < 10; ++i)
        stack.push(i);

    stack.clear();

    REQUIRE(stack.empty());
}
