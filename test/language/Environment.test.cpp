#include <catch2/catch.hpp>

#include <language/interpreter/Environment.hpp>

TEST_CASE("Environment is constructed", "[language][interpreter][Environment]")
{
    Environment env;
}

TEST_CASE("Environment can store and retrieve doubles", "[language][interpreter][Environment]")
{
    Environment env;

    env.set("x", 5.0);
    double x = env.get("x");

    REQUIRE(x == 5.0);
}

TEST_CASE("Environment can store and retrieve multiple doubles", "[language][interpreter][Environment]")
{
    Environment env;

    env.set("x", 5.0);
    env.set("y", 10.0);
    double x = env.get("x");
    double y = env.get("y");

    REQUIRE(x == 5.0);
    REQUIRE(y == 10.0);
}

TEST_CASE("Environments throw an exception when they cannot find the target variable", "[language][interpreter][Environment]")
{
    Environment env;

    REQUIRE_THROWS(env.get("x"));
}
