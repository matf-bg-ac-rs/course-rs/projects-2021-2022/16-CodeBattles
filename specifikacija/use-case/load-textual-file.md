# Loading a textual file

**Description**: The player selects the file which will be opened for editing inside of the built-in text editor 

**Actors**:
- The Player: selects the file.

**Preconditions**: The application is running, the *"Textual programming"*
tab is opened and player clicked on the *"Load from a file"* button

**Postconditions**: The file is loaded inside of the built-in text editor and ready to be edited by the player.

**Main scenario**:
1. Player selects the textual file they want to load using the native file system dialog
2. If the Player clicks the *"Ok"* button:
    1. The application loads the contents of the file into the built-in text editor
    2. Go to step 4
3. If the Player clicks the *"Cancel"* button:
    1. The application closes the native file system dialog 
    2. The use case ends here
4. The application notifies the player that the file was loaded successfully

**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.