# Starting the code battle

**Description**: After the robot programming is done, the player can
initiate a battle, where the user's robot will compete against other
robots.

**Actors**:
- The Player: initiates the battle and watches how their robot
  performs.

**Preconditions**: The *"Programming the bot"* use case.

**Postconditions**: The game is completed and the winner is
determined.

**Main scenario**:
1. The Player starts the battle by pressing the *"Start battle!"*
   button.
2. The application creates the game with the robots' programs.
3. While the game is not over, the following steps are repeated:
   1. The game updates the robots and projectiles, following the
      robots' programs and game logic.
      1. The game moves the robots.
      2. The game evaluates the robots' radar scans.
      3. The game moves the existing projectiles.
      4. If a projectile has reached its target position, the game
         causes the projectile to explode.
      5. The game updates the robots' health points.
      6. If a robot has fired a projectile, the game adds a new
         projectile.
   2. The game checks whether a robot's health points have reached
      zero.
      1. If the health points of a robot have reached zero, the game
         removes that robot from the game.
   3. If there is a single robot left, the game is marked as over,
      and that robot is the winner.
4. When the game is finished, the application shows information about
   the winner.

**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
