# Textual programming

**Description**: The player implements robot's behaviour using a simple, 
custom-made textual programming language in a built-in text editor.

**Actors**:
- The Player: types code into the text editor.

**Preconditions**: The application is running and the *"Textual programming"*
tab is opened.

**Postconditions**: The player has implemented the desired behaviour in the 
textual programming language. 

**Main scenario**:
1. If the Player clicks on the *"Load from a file"* button:
    1. Use case "Loading a file with code" is started. At the end,
    the application continues from step 2.
2. Until the Player presses the *"Play"* button, opens the *"Visual Programming"* 
tab or closes the application, the following steps are repeated:
    1. The Player makes changes in the text editor.
    2. If the Player clicks on the *"Save"* button:
        1. If the current file has already been saved, or loaded from a file:
            1. The new state of the code is saved in the file.
        2. Otherwise: the Player is prompted with a "Save File Dialog" in which
        he chooses a location to store the code.
    3. If the Player clicks on the *"Save As"* button:
        1. The Player is prompted with a "Save File Dialog" in which
        he chooses a location to store the code.


**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
