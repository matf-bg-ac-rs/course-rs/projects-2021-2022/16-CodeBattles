# Visual Programming

**Description**: The player can use a visual programming system to program the behaviour of their robot.

**Actors**: 
- The Player

**Preconditions**: The "Visual" programming tab is selected.

**Postconditions**: The visual components are connected and the graph has an internal representation.

**Main scenario**: 
1. While the Player doesn't press the "Play" button, the following steps are repeated: 
    1. The Player does one of the following changes to the component graph: 
        1. The Player places one of the components onto the canvas.
        2. The Player moves one of the components on the canvas.
        3. The Player changes an argument value in one of the components on the canvas.
        4. The Player removes one of the components from the canvas.
    2. The application checks if a component is placed underneath another component and if it is it snaps into place and they become connected. 

**Alternative scenarios**:
- A1: **Unexpected application exit**: The graph is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
