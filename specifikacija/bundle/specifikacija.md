# CodeBattles

*"CodeBattles"* is a game where the players design and implement a
program which their robot will execute during the game, with the goal
of triumphing over the other robots in the game, each running their
own program.

Upon lauching the application, two sections are presented:
- a section containing an overview of the game;
- a section in which the user develops the program for their robot.

This concept can be applied for different games, but the project
focuses on an implementation of one specific game.

## The game

The game's arena is square-shaped, with four robots placed on it at
the start. Each of the robots has its own program it's running during
the game, with one robot running the user's program.

The game is initiated by the user, after completing the robot's
program, via a button.

The game can be summarized as follows:
1. Every robot has health points, and starts the battle with full health;
2. The robots interact with the environment in the following ways:
	- They can move on the terrain in a specified direction;
	- They can fire a projectile in a specified direction with the
      distance when the projectile will explode;
	- They can use a radar to detect the enemy robots;
	- They can read their position on the field;
	- They can read their velocity;
	- They can read their health points;
3. The game progresses by running the programs of all robots and
   updating the state of the game;
4. The goal is for the user's robot to survive the attacks from the
   enemy robots and destroy them.

## Programming the robot

The user can develop the robot's program in two ways:
- via the graphical user interface, which provides a way to program
  using graphical programming elements, or;
- via writing code in a simple programming language.

## Visual robot programming

The user has many different kinds of programming elements at their disposal.

The user can pick the type of element to be added to the program from
an element overview section. The selected element can then be placed
on the scene, and composed with other elements to get the desired
behaviour.

In order to correct mistakes, the user can delete and move the elements.

## Robot programming via code

The user can write code for the robot in a simple programming
language, in the section provided for the purpose.

# UML diagrams

## Component diagram

<img src="./img/component-diagram.drawio.png" />

## Use case and sequence diagrams

<img src="./img/use_case_diagram.drawio.png" />

### Textual programming

**Description**: The player implements robot's behaviour using a simple, 
custom-made textual programming language in a built-in text editor.

**Actors**:
- The Player: types code into the text editor.

**Preconditions**: The application is running and the *"Textual programming"*
tab is opened.

**Postconditions**: The player has implemented the desired behaviour in the 
textual programming language. 

**Main scenario**:
1. If the Player clicks on the *"Load from a file"* button:
    1. Use case "Loading a file with code" is started. At the end,
    the application continues from step 2.
2. Until the Player presses the *"Play"* button, opens the *"Visual Programming"* 
tab or closes the application, the following steps are repeated:
    1. The Player makes changes in the text editor.
    2. If the Player clicks on the *"Save"* button:
        1. If the current file has already been saved, or loaded from a file:
            1. The new state of the code is saved in the file.
        2. Otherwise: the Player is prompted with a "Save File Dialog" in which
        he chooses a location to store the code.
    3. If the Player clicks on the *"Save As"* button:
        1. The Player is prompted with a "Save File Dialog" in which
        he chooses a location to store the code.


**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./img/textual-programming.drawio.png" />

### Loading a textual file

**Description**: The player selects the file which will be opened for editing inside of the built-in text editor 

**Actors**:
- The Player: selects the file.

**Preconditions**: The application is running, the *"Textual programming"*
tab is opened and player clicked on the *"Load from a file"* button

**Postconditions**: The file is loaded inside of the built-in text editor and ready to be edited by the player.

**Main scenario**:
1. Player selects the textual file they want to load using the native file system dialog
2. If the Player clicks the *"Ok"* button:
    1. The application loads the contents of the file into the built-in text editor
    2. Go to step 4
3. If the Player clicks the *"Cancel"* button:
    1. The application closes the native file system dialog 
    2. The use case ends here
4. The application notifies the player that the file was loaded successfully

**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./img/open-textual-file-sequence-diagram.drawio.png" />

### Starting the code battle

**Description**: After the robot programming is done, the player can
initiate a battle, where the user's robot will compete against other
robots.

**Actors**:
- The Player: initiates the battle and watches how their robot
  performs.

**Preconditions**: The *"Programming the bot"* use case.

**Postconditions**: The game is completed and the winner is
determined.

**Main scenario**:
1. The Player starts the battle by pressing the *"Start battle!"*
   button.
2. The application creates the game with the robots' programs.
3. While the game is not over, the following steps are repeated:
   1. The game updates the robots and projectiles, following the
      robots' programs and game logic.
      1. The game moves the robots.
      2. The game evaluates the robots' radar scans.
      3. The game moves the existing projectiles.
      4. If a projectile has reached its target position, the game
         causes the projectile to explode.
      5. The game updates the robots' health points.
      6. If a robot has fired a projectile, the game adds a new
         projectile.
   2. The game checks whether a robot's health points have reached
      zero.
      1. If the health points of a robot have reached zero, the game
         removes that robot from the game.
   3. If there is a single robot left, the game is marked as over,
      and that robot is the winner.
4. When the game is finished, the application shows information about
   the winner.

**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./img/starting-the-code-battle.png" />

### Visual Programming

**Description**: The player can use a visual programming system to program the behaviour of their robot.

**Actors**: 
- The Player

**Preconditions**: The "Visual" programming tab is selected.

**Postconditions**: The visual components are connected and the graph has an internal representation.

**Main scenario**: 
1. While the Player doesn't press the "Play" button, the following steps are repeated: 
    1. The Player does one of the following changes to the component graph: 
        1. The Player places one of the components onto the canvas.
        2. The Player moves one of the components on the canvas.
        3. The Player changes an argument value in one of the components on the canvas.
        4. The Player removes one of the components from the canvas.
    2. The application checks if a component is placed underneath another component and if it is it snaps into place and they become connected. 

**Alternative scenarios**:
- A1: **Unexpected application exit**: The graph is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./img/visual-programming-diagram.png" />

## Class diagram

<img src="./img/class.drawio.png" />
