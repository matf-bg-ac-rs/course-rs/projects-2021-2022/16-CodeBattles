QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 no_keywords

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += src/

SOURCES += \
    src/game_engine/BattleSceneInterpreterConfigurator.cpp \
    src/visual_programming/VisualProgramming.cpp \
    src/visual_programming/model/VisualComponent.cpp \
    src/visual_programming/model/VisualGraph.cpp \
    src/visual_programming/model/components/VisualComponentFunctionCall.cpp \
    src/visual_programming/model/components/VisualComponentIf.cpp \
    src/visual_programming/model/components/VisualComponentVariableAssignment.cpp \
    src/visual_programming/model/components/VisualComponentWhile.cpp \
    src/visual_programming/view/VisualComponentItem.cpp \
    src/visual_programming/view/VisualComponentItemBlock.cpp \
    src/visual_programming/view/VisualComponentPicker.cpp \
    src/visual_programming/view/VisualGraphScene.cpp \
    src/visual_programming/view/components/VisualComponentItemFunctionCall.cpp \
    src/visual_programming/view/components/VisualComponentItemIf.cpp \
    src/visual_programming/view/components/VisualComponentItemStart.cpp \
    src/visual_programming/view/components/VisualComponentItemVariableAssignment.cpp \
    src/visual_programming/view/components/VisualComponentItemWhile.cpp \
    src/game_engine/Assets.cpp \
    src/game_engine/BattleScene.cpp \
    src/game_engine/Component.cpp \
    src/game_engine/Entity.cpp \
    src/game_engine/EntityManager.cpp \
    src/game_engine/EntityMemoryPool.cpp \
    src/game_engine/GameEngine.cpp \
    src/game_engine/Physics.cpp \
    src/game_engine/QtGraphicsNode.cpp \
    src/game_engine/Scene.cpp \
    src/game_engine/Vec2.cpp \
    src/game_graphics/AlienShipNode.cpp \
    src/game_graphics/AlienWindowNode.cpp \
    src/game_engine/Sound.cpp \
    src/game_graphics/EntityNode.cpp \
    src/game_graphics/GraphicsScene.cpp \
    src/game_graphics/HealthBarNode.cpp \
    src/game_graphics/Node.cpp \
    src/game_graphics/PlayerNode.cpp \
    src/game_graphics/PlayerPixmapNode.cpp \
    src/game_graphics/ProjectilePixmapNode.cpp \
    src/game_graphics/RocketFireNode.cpp \
    src/game_graphics/RocketNode.cpp \
    src/game_graphics/SoundEffect.cpp \
    src/main.cpp \
    src/Window.cpp \
    src/language/ast/ast.cpp \
    src/language/parser/AstBuilder.cpp \
    src/language/interpreter/Callable.cpp \
    src/language/interpreter/Environment.cpp \
    src/language/interpreter/Execution.cpp \
    src/language/interpreter/Exception.cpp \
    src/language/interpreter/Instruction.cpp \
    src/language/interpreter/InstructionBuilder.cpp \
    src/language/interpreter/Interpreter.cpp \
    src/language/parser/generated/CodeBattlesLexer.cpp \
    src/language/parser/generated/CodeBattlesParser.cpp \
    src/language/parser/generated/CodeBattlesParserVisitor.cpp \
    src/language/parser/generated/CodeBattlesParserBaseVisitor.cpp

HEADERS += \
    src/game_engine/Assets.hpp \
    src/game_engine/BattleSceneInterpreterConfigurator.hpp \
    src/game_engine/QtGraphicsNode.hpp \
    src/game_graphics/AlienShipNode.hpp \
    src/game_graphics/AlienWindowNode.hpp \
    src/game_engine/Sound.hpp \
    src/game_graphics/EntityNode.hpp \
    src/game_graphics/GraphicsScene.hpp \
    src/game_graphics/HealthBarNode.hpp \
    src/game_graphics/Node.hpp \
    src/game_graphics/PlayerNode.hpp \
    src/game_graphics/PlayerPixmapNode.hpp \
    src/game_graphics/ProjectilePixmapNode.hpp \
    src/game_graphics/RocketFireNode.hpp \
    src/game_graphics/RocketNode.hpp \
    src/game_graphics/SoundEffect.hpp \
    src/Window.hpp \
    src/visual_programming/VisualProgramming.hpp \
    src/visual_programming/model/VisualComponent.hpp \
    src/visual_programming/model/VisualGraph.hpp \
    src/visual_programming/model/components/VisualComponentFunctionCall.hpp \
    src/visual_programming/model/components/VisualComponentIf.hpp \
    src/visual_programming/model/components/VisualComponentVariableAssignment.hpp \
    src/visual_programming/model/components/VisualComponentWhile.hpp \
    src/visual_programming/view/VisualComponentItem.hpp \
    src/visual_programming/view/VisualComponentItemBlock.hpp \
    src/visual_programming/view/VisualComponentPicker.hpp \
    src/visual_programming/view/VisualGraphScene.hpp \
    src/visual_programming/view/components/VisualComponentItemFunctionCall.hpp \
    src/visual_programming/view/components/VisualComponentItemIf.hpp \
    src/visual_programming/view/components/VisualComponentItemStart.hpp \
    src/visual_programming/view/components/VisualComponentItemVariableAssignment.hpp \
    src/visual_programming/view/components/VisualComponentItemWhile.hpp \
    src/game_engine/BattleScene.hpp \
    src/game_engine/Component.hpp \
    src/game_engine/Entity.hpp \
    src/game_engine/EntityManager.hpp \
    src/game_engine/EntityMemoryPool.hpp \
    src/game_engine/GameEngine.hpp \
    src/game_engine/Physics.hpp \
    src/game_engine/Scene.hpp \
    src/game_engine/Vec2.hpp \
    src/language/ast/ast.hpp \
    src/language/ast/Visitor.hpp \
    src/language/parser/AstBuilder.hpp \
    src/language/interpreter/Callable.hpp \
    src/language/interpreter/Environment.hpp \
    src/language/interpreter/Execution.hpp \
    src/language/interpreter/Exception.hpp \
    src/language/interpreter/Stack.hpp \
    src/language/interpreter/Instruction.hpp \
    src/language/interpreter/InstructionBuilder.hpp \
    src/language/interpreter/Interpreter.hpp \
    src/language/parser/generated/CodeBattlesLexer.h \
    src/language/parser/generated/CodeBattlesParser.h \
    src/language/parser/generated/CodeBattlesParserVisitor.h \
    src/language/parser/generated/CodeBattlesParserBaseVisitor.h

FORMS += \
    form/Window.ui

OTHER_FILES += \
    src/language/grammar/CodeBattlesLexer.g4 \
    src/language/grammar/CodeBattlesParser.g4

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/res.qrc

unix|win32: LIBS += -lantlr4-runtime
unix:!android: INCLUDEPATH += /usr/include/antlr4-runtime
