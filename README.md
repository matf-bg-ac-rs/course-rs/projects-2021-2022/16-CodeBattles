# CodeBattles

*"CodeBattles"* is a game where the players design and implement a
program which their robot will execute during the game, with the goal
of triumphing over the other robots in the game, each running their
own program.

Upon lauching the application, two sections are presented:
- a section containing an overview of the game;
- a section in which the user develops the program for their robot.

This concept can be applied for different games, but the project
focuses on an implementation of one specific game.

## Video demo

- https://drive.google.com/file/d/1kHkKl3UWnQgAwxJwaKov5ykCGKVgJ3gT/view?usp=sharing

## The game

The game's arena is square-shaped, with four robots placed on it at
the start. Each of the robots has its own program it's running during
the game, with one robot running the user's program.

The game is initiated by the user, after completing the robot's
program, via a button.

The game can be summarized as follows:
1. Every robot has health points, and starts the battle with full health;
2. The robots interact with the environment in the following ways:
	- They can move on the terrain in a specified direction;
	- They can fire a projectile in a specified direction with the
      distance when the projectile will explode;
	- They can use a radar to detect the enemy robots;
	- They can read their position on the field;
	- They can read their velocity;
	- They can read their health points;
3. The game progresses by running the programs of all robots and
   updating the state of the game;
4. The goal is for the user's robot to survive the attacks from the
   enemy robots and destroy them.

## Programming the robot

The user can develop the robot's program in two ways:
- via the graphical user interface, which provides a way to program
  using graphical programming elements, or;
- via writing code in a simple programming language.

### Visual robot programming

The user has many different kinds of programming elements at their disposal.

The user can pick the type of element to be added to the program from
an element overview section. The selected element can then be placed
on the scene, and composed with other elements to get the desired
behaviour.

In order to correct mistakes, the user can delete and move the elements.

### Robot programming via code

The user can write code for the robot in a simple programming
language, in the section provided for the purpose.

## Dev setup

### Prerequisites

- Qt 6.2
- ANTLR 4 tool
- ANTLR 4 C++ runtime

Qt Multimedia [Add-On module](https://doc-snapshots.qt.io/qt6-dev/qtmodules.html) can be added from the MaintananceTool (located in Qt's install directory).

The (non-Qt) prerequisites can be installed with:

- Ubuntu
```bash
sudo apt install antlr4 libantlr4-runtime-dev
```
- Fedora
```bash
sudo dnf install antlr4 antlr4-cpp-runtime-devel
```
- Arch
```bash
sudo pacman -S antlr4 antlr4-runtime 
```

### Setup

- Navigate to the `dev` folder and run the setup script
```bash
   $ cd dev 
   $ ./setup.sh
   $ ./generate_parser.sh
```
- Currently tested and working only on `Linux`

## Developers

- [Slobodan Jenko, 6/2018](https://gitlab.com/jenko.slobodan)
- [Ivan Pop-Jovanov, 85/2018](https://gitlab.com/ivanpopjovanov)
- [Aleksandar Šmigić, 28/2019](https://gitlab.com/smiga287)
- [Petar Nikić, 49/2018](https://gitlab.com/pearpanda)
