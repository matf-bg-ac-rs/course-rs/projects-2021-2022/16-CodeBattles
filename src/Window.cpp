#include "Window.hpp"
#include "ui_Window.h"

#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>

#include "game_graphics/GraphicsScene.hpp"
#include "game_graphics/PlayerNode.hpp"
#include "game_graphics/ProjectilePixmapNode.hpp"
#include "visual_programming/VisualProgramming.hpp"

#include <iostream>

Window::Window(QWidget *parent) : QMainWindow(parent), ui(new Ui::Window), game_engine(this) {
    ui->setupUi(this);
    setupVisualProgramming();

    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Window::renderGame);
    timer->start(1000 / FPS);

    connect(ui->actionSave, &QAction::triggered, this, &Window::saveTextProgram);
    connect(ui->actionLoad, &QAction::triggered, this, &Window::loadTextProgram);
    connect(ui->actionSaveAs, &QAction::triggered, this, &Window::saveAsTextProgram);
}

Window::~Window() {
    delete ui;
    delete vp;
}

Vec2 Window::getGraphicsSceneSize() const { return graphics_scene_size_; }

void Window::disablePlayButton() {
    ui->pbPlay->setDisabled(true);
}

void Window::renderGame() {
    game_engine.update();

    static int calls = 0;
    calls++;
    if (calls % FPS == 0) {
        int frames = calls / FPS;
//        std::cout << frames << std::endl;
    }
}

void Window::setupGraphicsScene(GraphicsScene* graphics_scene_ptr) {
    ui->codeEditor->setFocus();
    ui->tabWidget->setTabEnabled(2, false);

    graphics_scene_ptr->setSceneRect(ui->gvArena->rect());
    ui->gvArena->setScene(graphics_scene_ptr);

    ui->gvArena->setRenderHint(QPainter::Antialiasing);
    ui->gvArena->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    //    connect(ui->tabWidget, &QTabWidget::tabBarClicked, arena, &Arena::startGame);
    qInfo() << "Arena set up.";
}

void Window::setupVisualProgramming()
{
    ui->codeEditor->setFocus();
    ui->tabWidget->setTabEnabled(2, false);
    tab = ui->tabWidget->currentIndex();

    vp = new VisualProgramming(ui->gvVisualProgramming, ui->gvComponentPicker);

    connect(ui->tabWidget, &QTabWidget::tabBarClicked, this, &Window::run);

    qInfo() << "Visual Programming set up.";
}

std::string readTextResource(const char* path)
{
    QFile file{path};
    file.open(QIODevice::ReadOnly);
    auto contents = file.readAll();
    return contents.toStdString();
}

void Window::run(int index)
{
    if (index != -1) {
        tab = index;
        return;
    }

    // TODO: check out why this doesn't work
    disablePlayButton();

    QString program = "";
    if (tab == 0) {
        program = vp->generateCode();
    }
    if (tab == 1) {
        program = ui->codeEditor->toPlainText();
    }
    qDebug() << program;
//    game_engine.assets().addProgram(program.toStdString());

    auto rook = readTextResource(":/bots/rook.cb");
    auto sniper = readTextResource(":/bots/sniper.cb");
    auto counter = readTextResource(":/bots/counter.cb");

    auto first = readTextResource(":/bots/first.cb");
    auto second = readTextResource(":/bots/second.cb");
    auto third = readTextResource(":/bots/third.cb");
    auto fourth = readTextResource(":/bots/fourth.cb");

    auto programs = {first, second, third, fourth};

#if 0
    QString qRook = QString::fromStdString(rook);
    QString qSniper = QString::fromStdString(sniper);
    QString qCounter = QString::fromStdString(counter);

    qDebug() << "Rook:\n" << qRook;
    qDebug() << "Sniper:\n" << qSniper;
    qDebug() << "Counter:\n" << qCounter;
#endif

    for (auto program : programs) {
        game_engine.assets().addProgram(program);
    }
//    game_engine.assets().addProgram(rook);
//    game_engine.assets().addProgram(sniper);
//    game_engine.assets().addProgram(counter);

    game_engine.run();
}

void Window::saveTextProgram()
{
    if (ui->tabWidget->currentIndex() != 1) {
        QMessageBox box;
        box.setText("Saving visual programming solutions is not supported.");
        box.exec();
        ui->tabWidget->setCurrentIndex(1);
    } else {
        if (openBufferPath.size() == 0) {
            saveAsTextProgram();
        } else {
            save(openBufferPath);
        }
    }
}

void Window::saveAsTextProgram()
{
    if (ui->tabWidget->currentIndex() != 1) {
        QMessageBox box;
        box.setText("Saving visual programming solutions is not supported.");
        box.exec();
        ui->tabWidget->setCurrentIndex(1);
    } else {
        auto path = QFileDialog::getSaveFileName(this, tr("Open Bot code file"),
                                                 QString{}, tr("CodeBattles source file (*.cb)"));
        save(path);
    }
}

void Window::loadTextProgram()
{
    auto path = QFileDialog::getOpenFileName(this, tr("Open Bot code file"),
                                             QString{}, tr("CodeBattles source file (*.cb)"));
    load(path);
    ui->tabWidget->setCurrentIndex(1);
}

void Window::save(const QString& path)
{
    auto text = ui->codeEditor->toPlainText();
    QFile file{path};
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out << text;
        file.close();
        openBufferPath = path;
    }
}

void Window::load(const QString& path)
{
    QFile file{path};
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);
        ui->codeEditor->setPlainText(in.readAll());
        file.close();
        openBufferPath = path;
    }
}
