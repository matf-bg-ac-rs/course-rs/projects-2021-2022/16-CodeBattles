#ifndef ENTITYNODE_H
#define ENTITYNODE_H

#include <QGraphicsItem>

#include "../game_engine/Entity.hpp"
#include "Node.hpp"

class EntityNode : public Node {
public:
    void checkHasDimensions() const;
    void checkHasPosition() const;

    // Node interface
    qreal parentX() const override;
    qreal parentY() const override;
    qreal parentRotation() const override;
    qreal localWidth() const override;
    qreal localHeight() const override;

protected:
    EntityNode(Entity entity);
    Entity entity_;

    friend class Arena;
};

#endif // ENTITYNODE_H
