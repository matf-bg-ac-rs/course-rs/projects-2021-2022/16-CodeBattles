#include <QPainter>
#include <QDebug>

#include "PlayerNode.hpp"
#include "../game_engine/Entity.hpp"
#include "../game_engine/Component.hpp"

PlayerNode::PlayerNode(Entity entity)
    : EntityNode(entity)
    , health_bar(nullptr)
{
}

void PlayerNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter)
    Q_UNUSED(option)
    Q_UNUSED(widget)
    if (health_bar == nullptr)
        createHealthBar();
}

void PlayerNode::createHealthBar()
{
    checkHasHealth();
    auto hb_width = 6;
    auto hb_height = 30;
    auto hb_parent_x = localMinX() - 1.5 * hb_width;
    health_bar = new HealthBarNode(entity_.getComponent<CHealth>().health, hb_parent_x, 0,
                                   hb_width, hb_height, 0, this);
}

void PlayerNode::checkHasHealth() const
{
    if (!entity_.hasComponent<CHealth>()) {
        throw std::runtime_error("PlayerNode's entity needs to have health.");
    }
}


