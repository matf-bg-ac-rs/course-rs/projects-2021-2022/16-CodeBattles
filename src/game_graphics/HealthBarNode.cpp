#include "HealthBarNode.hpp"

#include <QBrush>
#include <QPen>
#include <QPainter>

HealthBarNode::HealthBarNode(int health, qreal parent_x, qreal parent_y, qreal width, qreal height, qreal parent_rotation, QGraphicsItem *parent)
    : Node(parent),
      health(health), width(width), height(height),
      parent_x(parent_x), parent_y(parent_y), parent_rotation(parent_rotation)
{
    setZValue(-1);
    refreshDisplay();
}

void HealthBarNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    int whiteH = localHeight() * (100 - health) / 100;
    int redH = localHeight() - whiteH;
    auto whiteRect = QRectF(localMinX(), localMinY(), localWidth(), whiteH);
    auto redRect = QRectF(localMinX(), localMinY() + whiteH, localWidth(), redH);
    painter->fillRect(whiteRect, Qt::white);
    painter->fillRect(redRect, Qt::red);
}
