#include "RocketNode.hpp"

#include <QPainter>

#define PIX_PATH ":/images/rocket2.png"

RocketNode::RocketNode(Entity entity)
    : PlayerPixmapNode(entity, PIX_PATH)
    , fire(nullptr)
{
}

void RocketNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    PlayerPixmapNode::paint(painter, option, widget);
    if (fire == nullptr) {
        initializeFire();
    }
}

void RocketNode::initializeFire()
{
    auto fire_w = localWidth() / 3;
    auto fire_h = localHeight() / 3;
    auto fire_x = 0;
    auto fire_y = localMaxY() + fire_h / 2 - 5;
    fire = new RocketFireNode(fire_x, fire_y, fire_w, fire_h, 0, this);
}
