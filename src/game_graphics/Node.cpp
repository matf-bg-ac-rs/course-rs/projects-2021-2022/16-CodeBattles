#include "Node.hpp"

Node::Node()
{
}

Node::Node(QGraphicsItem *parent)
    :QGraphicsItem(parent)
{
}

qreal Node::localMinX() const
{
    return - localWidth() / 2;
}

qreal Node::localMinY() const
{
    return -localHeight() / 2;
}

qreal Node::localMaxX() const
{
    return localWidth() / 2;
}

qreal Node::localMaxY() const
{
    return localHeight() / 2;
}

void Node::refreshDisplay()
{
    setRotation(parentRotation());
    setPos(parentX(), parentY());
}

QRectF Node::boundingRect() const
{
    return QRectF(localMinX(), localMinY(), localWidth(), localHeight());
}
