#include "SoundEffect.hpp"

SoundEffect::SoundEffect(const QString &path)
{
    qt_sound_effect = std:: unique_ptr<QSoundEffect>{new QSoundEffect()};
    qt_sound_effect->setSource(QUrl::fromLocalFile(path));
    qt_sound_effect->setVolume(15);
}

void SoundEffect::play()
{
    qt_sound_effect->play();
}
