#ifndef ROCKETFIRENODE_HPP
#define ROCKETFIRENODE_HPP

#include "Node.hpp"

class RocketFireNode : public Node
{
public:
    RocketFireNode(qreal parent_x, qreal parent_y, qreal width, qreal height,
                   qreal parent_rotation, QGraphicsItem* parent);

    // QGraphicsItem interface
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;


protected:
    // Node interface
    qreal parentX() const override {
        return parent_x;
    }
    qreal parentY() const override {
        return parent_y;
    }
    qreal parentRotation() const override {
        return parent_rotation;
    }
    qreal localWidth() const override {
        return width;
    }
    qreal localHeight() const override {
        return height;
    }

private:
    qreal parent_x, parent_y;
    qreal width, height;
    qreal parent_rotation;
    QVector<QPointF> path_points;

    QPainterPath createPath(QVector<QPointF> points, qreal scale, QVector<QPointF> wiggle);
    QVector<QPointF> generateWiggle(double range, int n_points);
};

#endif // ROCKETFIRENODE_HPP
