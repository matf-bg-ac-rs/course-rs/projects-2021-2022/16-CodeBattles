#ifndef PROJECTILEPIXMAPNODE_H
#define PROJECTILEPIXMAPNODE_H

#include <QGraphicsPixmapItem>

#include "SoundEffect.hpp"
#include "EntityNode.hpp"

class ProjectilePixmapNode : public EntityNode {
public:
    ProjectilePixmapNode(Entity entity);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    std::unique_ptr<SoundEffect> fire_sound;
};

#endif // PROJECTILEPIXMAPNODE_H
