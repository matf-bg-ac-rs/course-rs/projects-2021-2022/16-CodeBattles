#include <QDebug>
#include <QTimer>
#include <QCoreApplication>

#include "GraphicsScene.hpp"
#include "PlayerNode.hpp"
#include "ProjectilePixmapNode.hpp"
#include "PlayerPixmapNode.hpp"
#include "RocketNode.hpp"
#include "AlienShipNode.hpp"
#include "game_engine/Component.hpp"

#define FPS 20

GraphicsScene::GraphicsScene(QObject* parent) : QGraphicsScene(parent) {
    setBackgroundBrush(QBrush(QImage(":/images/space-background.jpg")));
//    background_music_ = Sound{":/sounds/superepic.mp3"};
}

void GraphicsScene::addEntity(Entity entity) {
    if (!entity.hasComponent<CQtGraphicsNode>()) {
         throw std::runtime_error("Unsupported entity");
    }
    auto& graphics_node = entity.getComponent<CQtGraphicsNode>().graphics_node;
    addItem(graphics_node.getQtNode());
}

void startGame()
{
//    qInfo() << "Game started.";
//    QTimer *timer = new QTimer(this);
//    connect(timer, &QTimer::timeout, this, &Arena::render);
//    timer->start(1000 / FPS);

//    const auto playerNode1 = new AlienShipNode{EntityMockup{70, 70, 70, 0, 60, 60}};
//    addItem(playerNode1);
//    const auto playerNode2 = new RocketNode{EntityMockup{350, 70, 90, 45, 40, 70}};
//    addItem(playerNode2);
//    const auto playerNode3 = new RocketNode{EntityMockup{100, 350, 50, 90, 70, 110}};
//    addItem(playerNode3);
//    const auto playerNode4 = new AlienShipNode{EntityMockup{350, 350, 20, 0, 80, 80}};
//    addItem(playerNode4);

//    const auto projectileNode1 = new ProjectilePixmapNode{EntityMockup{50, 160, 0, 0, 20, 20}};
//    const auto projectileNode2 = new ProjectilePixmapNode{EntityMockup{180, 180, 0, 0, 20, 20}};
//    const auto projectileNode3 = new ProjectilePixmapNode{EntityMockup{220, 50, 0, 0, 20, 20}};
//    addItem(projectileNode1);
//    addItem(projectileNode2);
//    addItem(projectileNode3);
}

Vec2 GraphicsScene::size() const {
    float w = width();
    float h = height();
    return {w, h};
}
