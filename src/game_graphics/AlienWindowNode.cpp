#include "AlienWindowNode.hpp"

#include <QColor>
#include <QPainter>

AlienWindowNode::AlienWindowNode(qreal parent_x, qreal parent_y, qreal r1, qreal r2, qreal parent_rotation, QGraphicsItem *parent)
    : Node(parent),
      r1(r1), r2(r2),
      parent_x(parent_x), parent_y(parent_y), parent_rotation(parent_rotation)
{
    createGlowAnimation();
    refreshDisplay();
}

void AlienWindowNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::gray);
    auto green1 = scaledColor(QColor(84, 239, 125), color_scale);
    auto green2 = scaledColor(QColor(8, 124, 39), color_scale);
    auto windowGrad = QRadialGradient(0, 0, localWidth() / 2);
    windowGrad.setColorAt(0, green1);
    windowGrad.setColorAt(0.7, green2);

    painter->setBrush(windowGrad);
    painter->drawEllipse(QPointF(0, 0), r1, r2);
}

QColor AlienWindowNode::scaledColor(QColor col, qreal scale) const {
    auto rgb = col.rgb();
    return QColor(qMin(scale * qRed(rgb), 255.),
                 qMin(scale * qGreen(rgb), 255.),
                 qMin(scale * qBlue(rgb), 255.));
}

void AlienWindowNode::createGlowAnimation()
{
    glowAnim = new QPropertyAnimation(this);
    glowAnim->setTargetObject(this);
    glowAnim->setPropertyName("color_scale");
    glowAnim->setStartValue(0.7);
    glowAnim->setEndValue(1.2);
    glowAnim->setDuration(3000);
    glowAnim->setEasingCurve(QEasingCurve::SineCurve);
    glowAnim->setLoopCount(-1);
}

