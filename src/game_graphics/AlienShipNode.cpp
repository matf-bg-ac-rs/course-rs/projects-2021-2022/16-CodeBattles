#include "AlienShipNode.hpp"

#include <QColor>
#include <QPainter>
#include <QtMath>
#include <QParallelAnimationGroup>

#include "AlienWindowNode.hpp"

AlienShipNode::AlienShipNode(Entity entity) : PlayerNode(entity) {

}

void AlienShipNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    PlayerNode::paint(painter, option, widget);
    if (windows.size() == 0) {
        initializeWindows();
    }

    auto shipColor = QColor(130, 130, 130);
    painter->setPen(Qt::gray);
    painter->setBrush(shipColor);
    painter->drawEllipse(localMinX(), localMinY(), localWidth(), localHeight());
}

void AlienShipNode::initializeWindows()
{
    createWindows();
    groupAnimations();
    root_animation->start();
}

void AlienShipNode::createWindows()
{
    addWindow(0.4, 0.4, 0, 0);

    int n_little = 6;
    qreal angle = 0;
    for(int i = 0; i < n_little; i++) {
        addWindow(0.2, 0.15, angle, 0.7);
        angle += 2 * M_PI / n_little;
    }
}

void AlienShipNode::addWindow(qreal r1, qreal r2, qreal angle, qreal offset)
{
    auto wX = qCos(angle) * offset * localWidth() / 2;
    auto wY = qSin(angle) * offset * localHeight() / 2;
    windows.append(new AlienWindowNode(wX, wY, r1 * localWidth() / 2, r2 * localHeight() / 2,
                        qRadiansToDegrees(angle), this));
}

void AlienShipNode::groupAnimations()
{
    auto group = new QParallelAnimationGroup();
    for (auto& window: windows) {
        group->addAnimation(window->getRootAnim());
    }
    root_animation.reset(group);
}
