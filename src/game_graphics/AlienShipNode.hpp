#ifndef ALIENSHIPNODE_H
#define ALIENSHIPNODE_H

#include "PlayerNode.hpp"
#include "AlienWindowNode.hpp"

#include <QVector>
#include <QApplication>

class AlienShipNode : public PlayerNode {
public:
    AlienShipNode(Entity entity);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    QVector<AlienWindowNode*> windows;
    std::shared_ptr<QAbstractAnimation> root_animation;

    void initializeWindows();
    void createWindows();
    void addWindow(qreal r1, qreal r2, qreal angle, qreal offset);
    void groupAnimations();
};

#endif // ALIENSHIPNODE_H
