#ifndef ALIENWINDOWNODE_HPP
#define ALIENWINDOWNODE_HPP

#include <QPropertyAnimation>

#include "Node.hpp"

class AlienWindowNode : public QObject, public Node
{
    Q_OBJECT
    Q_PROPERTY(qreal color_scale READ getColorScale WRITE setColorScale NOTIFY colorScaleChanged)

public:
    AlienWindowNode(qreal parent_x, qreal parent_y, qreal r1, qreal r2,
                    qreal parent_rotation, QGraphicsItem* parent);

    // QGraphicsItem interface
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    QAbstractAnimation* getRootAnim() {
        return glowAnim;
    }

    // Node interface
    qreal parentX() const override {
        return parent_x;
    }
    qreal parentY() const override {
        return parent_y;
    }
    qreal parentRotation() const override {
        return parent_rotation;
    }
    qreal localWidth() const override {
        return 2 * r1;
    }
    qreal localHeight() const override {
        return 2 * r2;
    }

    qreal getColorScale() const {
        return color_scale;
    };
    void setColorScale(const qreal &new_stop_point) {
        if (color_scale == new_stop_point) {
            return;
        }
        color_scale = new_stop_point;
        Q_EMIT colorScaleChanged(color_scale);
    }

Q_SIGNALS:
    void colorScaleChanged(qreal);

private:
    qreal r1, r2;
    qreal parent_x, parent_y;
    qreal parent_rotation;
    qreal color_scale = 0;
    QPropertyAnimation* glowAnim;

    void createGlowAnimation();
    QColor scaledColor(QColor col, qreal scale) const;
};

#endif // ALIENWINDOWNODE_HPP
