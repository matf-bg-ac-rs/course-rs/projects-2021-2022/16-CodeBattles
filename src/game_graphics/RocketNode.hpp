#ifndef ROCKETNODE_HPP
#define ROCKETNODE_HPP

#include "PlayerPixmapNode.hpp"
#include "RocketFireNode.hpp"

class RocketNode : public PlayerPixmapNode {
public:
    RocketNode(Entity entity);

    // QGraphicsItem interface
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
private:
    RocketFireNode *fire;
    void initializeFire();
};

#endif // ROCKETNODE_HPP
