#include "RocketFireNode.hpp"

#include <QPainter>
#include <QRandomGenerator>

RocketFireNode::RocketFireNode(qreal parent_x, qreal parent_y, qreal width, qreal height,
                               qreal parent_rotation, QGraphicsItem *parent)
    : Node(parent),
      parent_x(parent_x), parent_y(parent_y), width(width), height(height),
      parent_rotation(parent_rotation)
{
    path_points = QVector<QPointF> {
        QPointF(localMinX() * 0.5, 0),
        QPointF(localMinX() * 1, localHeight() * 0.7),
        QPointF(localMinX() * 0.4, localHeight() * 0.4),
        QPointF(0, localHeight() * 1),
        QPointF(localMaxX() * 0.4, localHeight() * 0.4),
        QPointF(localMaxX() * 1, localHeight() * 0.7),
        QPointF(localMaxX() * 0.5, 0)
    };

    refreshDisplay();
}

void RocketFireNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    auto wiggleVec = generateWiggle(2, path_points.length());
    painter->setBrush(QColor("red"));
    painter->setPen(Qt::NoPen);
    painter->drawPath(createPath(path_points, 1, wiggleVec));

    painter->setBrush(QColor("orange"));
    painter->setPen(Qt::NoPen);
    painter->drawPath(createPath(path_points, 0.6, wiggleVec));

    painter->setBrush(QColor("yellow"));
    painter->setPen(Qt::NoPen);
    painter->drawPath(createPath(path_points, 0.3, wiggleVec));
}

QPainterPath RocketFireNode::createPath(QVector<QPointF> points, qreal scale, QVector<QPointF> wiggle)
{
    QPainterPath path;
    path.moveTo(points[0] * scale + QPointF(0, localMinY()));
    for (int i = 1; i < points.size(); i++)
        path.lineTo((points[i] + wiggle[i]) * scale + QPointF(0, localMinY()));
    path.closeSubpath();

    return path;
}

QVector<QPointF> RocketFireNode::generateWiggle(double range, int n_points)
{
    QVector<QPointF> ret;
    ret.append(QPointF(0, 0));
    for (int i = 1; i < n_points - 1; i++) {
        ret.append(QPointF(QRandomGenerator::global()->bounded(2 * range) - range,
                           QRandomGenerator::global()->bounded(2 * range) - range));
    }
    ret.append(QPointF(0, 0));
    return ret;
}
















