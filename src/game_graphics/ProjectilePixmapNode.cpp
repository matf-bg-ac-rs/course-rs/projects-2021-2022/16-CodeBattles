#include <QPainter>
#include "ProjectilePixmapNode.hpp"

ProjectilePixmapNode::ProjectilePixmapNode(Entity entity) : EntityNode(entity) {
    fire_sound = std::unique_ptr<SoundEffect>{
            new SoundEffect{"../16-CodeBattles/resources/laser-gun.wav"}};
    fire_sound->play();
}

void ProjectilePixmapNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    auto pixmap = QPixmap("../16-CodeBattles/resources/laser-shot.png");
    painter->drawPixmap(localMinX(), localMinY(),
                        localWidth(), localHeight(), pixmap);
}

