#ifndef PLAYERPIXMAPNODE_H
#define PLAYERPIXMAPNODE_H

#include "PlayerNode.hpp"

class PlayerPixmapNode : public PlayerNode {
public:
    PlayerPixmapNode(Entity entity, QString pixmapPath);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
private:
    QPixmap pixmap;
};

#endif // PLAYERPIXMAPNODE_H
