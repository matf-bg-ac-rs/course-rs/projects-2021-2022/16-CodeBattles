#ifndef NODE_HPP
#define NODE_HPP

#include <QGraphicsItem>

class Node : public QGraphicsItem
{

public:
    void refreshDisplay();
    virtual QRectF boundingRect() const override;

    virtual qreal parentX() const = 0;
    virtual qreal parentY() const = 0;
    virtual qreal parentRotation() const = 0;

    virtual qreal localWidth() const = 0;
    virtual qreal localHeight() const = 0;
    qreal localMinX() const;
    qreal localMinY() const;
    qreal localMaxX() const;
    qreal localMaxY() const;
protected:
    Node();
    Node(QGraphicsItem* parent);
};

#endif // NODE_HPP
