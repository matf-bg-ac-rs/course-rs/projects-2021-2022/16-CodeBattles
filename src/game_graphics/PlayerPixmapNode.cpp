#include <QPainter>
#include <QDebug>
#include <QBrush>

#include "PlayerPixmapNode.hpp"

PlayerPixmapNode::PlayerPixmapNode(Entity entity, QString pixmapPath)
    : PlayerNode(entity)
{
    pixmap = QPixmap(pixmapPath);
}

void PlayerPixmapNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    PlayerNode::paint(painter, option, widget);
    painter->drawPixmap(localMinX(), localMinY(),
                        localWidth(), localHeight(), pixmap);
}

