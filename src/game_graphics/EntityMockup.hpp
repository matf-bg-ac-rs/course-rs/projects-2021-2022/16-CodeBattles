#ifndef ENTITYMOCKUP_H
#define ENTITYMOCKUP_H

#include<tuple>

#include "game_engine/Component.hpp"

class Entity
{
public:
    Entity(float x, float y, int health,
               float orientation, int w, int h)
    {
        components = std::make_tuple(CTransform{{true}, Vec2{x, y}, Vec2{0, 0}, orientation, w, h},
                                     CHealth{{true}, health});
    }


    template <typename T> T& getComponent()
    {
        return std::get<T>(components);
    }

    template <typename T> const T& getComponent() const
    {
        return std::get<T>(components);
    }

    template <typename T> bool hasComponent() const
    {
        return true;
    }

private:
    std::tuple<CTransform, CHealth> components;
};

#endif // ENTITYMOCKUP_H
