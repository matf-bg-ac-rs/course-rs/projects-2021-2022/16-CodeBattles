#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QSoundEffect>

#include "../game_engine/Sound.hpp"
#include "SoundEffect.hpp"
#include "../game_engine/Entity.hpp"

class PlayerNode;
class ProjectilePixmapNode;

class GraphicsScene : public QGraphicsScene {
public:
    explicit GraphicsScene(QObject* parent = nullptr);
    void addEntity(Entity bot);
    Vec2 size() const;
};

#endif // GRAPHICSSCENE_H
