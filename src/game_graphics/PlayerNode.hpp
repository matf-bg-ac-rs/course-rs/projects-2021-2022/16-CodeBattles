#ifndef PLAYERNODE_H
#define PLAYERNODE_H

#include <QGraphicsPixmapItem>
#include <QSize>

#include "EntityNode.hpp"
#include "HealthBarNode.hpp"

class PlayerNode : public EntityNode {
public:
    void checkHasHealth() const;
protected:
    PlayerNode(Entity entity);

    // QGraphicsItem interface
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
private:
    HealthBarNode *health_bar;
    void createHealthBar();
};

#endif // PLAYERNODE_H
