#include "EntityNode.hpp"
#include "../game_engine/Component.hpp"
#include "../game_engine/Entity.hpp"

EntityNode::EntityNode(Entity entity)
    : entity_(entity)
{
}

qreal EntityNode::parentX() const {
    checkHasPosition();
    float x = entity_.getComponent<CTransform>().pos.x();
    return x;
}

qreal EntityNode::parentY() const {
    checkHasPosition();
    float y = entity_.getComponent<CTransform>().pos.y();
    return y;
}

qreal EntityNode::localWidth() const {
    checkHasDimensions();
    auto& graphics_node = entity_.getComponent<CQtGraphicsNode>().graphics_node;
    return graphics_node.size().x();
}

qreal EntityNode::localHeight() const {
    checkHasDimensions();
    auto& graphics_node = entity_.getComponent<CQtGraphicsNode>().graphics_node;
    return graphics_node.size().y();
}

void EntityNode::checkHasDimensions() const
{
    if (!entity_.hasComponent<CQtGraphicsNode>()) {
        throw std::runtime_error("Node's entity needs to have width and height.");
    }
}

void EntityNode::checkHasPosition() const
{
    if (!entity_.hasComponent<CTransform>()) {
        throw std::runtime_error("Node's entity needs to have position and rotation.");
    }
}

qreal EntityNode::parentRotation() const {
    checkHasPosition();
    return entity_.getComponent<CTransform>().angle;
}
