#ifndef HEALTHBARNODE_HPP
#define HEALTHBARNODE_HPP

#include "Node.hpp"

#include <QGraphicsRectItem>

class HealthBarNode: public Node {
public:
    HealthBarNode(int health, qreal parent_x, qreal parent_y, qreal width, qreal height,
                  qreal parent_rotation, QGraphicsItem* parent);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void setHealth(int new_health) {
        health = new_health;
    }

    // Node interface
    qreal parentX() const override {
        return parent_x;
    }
    qreal parentY() const override {
        return parent_y;
    }
    qreal parentRotation() const override {
        return parent_rotation;
    }
    qreal localWidth() const override {
        return width;
    }
    qreal localHeight() const override {
        return height;
    }

private:
    int health;
    qreal width, height;
    qreal parent_x, parent_y;
    qreal parent_rotation;
};

#endif // HEALTHBARNODE_HPP
