#ifndef SOUNDEFFECT_HPP
#define SOUNDEFFECT_HPP

#include <QString>
#include <QSoundEffect>
#include <memory>

class SoundEffect
{
public:
    SoundEffect(const QString& path);
    void play();

private:
    std::unique_ptr<QSoundEffect> qt_sound_effect;
};

#endif // SOUNDEFFECT_HPP
