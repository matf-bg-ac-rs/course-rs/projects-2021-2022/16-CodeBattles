parser grammar CodeBattlesParser;

options {
    tokenVocab = CodeBattlesLexer;
}

program: statement* EOF;

block: OCB statement* CCB;

statement:
    SEMICOLON                                                                  #EmptyStmt
    | expression SEMICOLON                                                     #ExprStmt
    | block                                                                    #BlockStmt
    | IF OP cond = expression CP ifBr = statement                              #IfStmt
    | IF OP cond = expression CP ifBr = statement ELSE elseBr = statement      #IfElseStmt
    | WHILE OP expression CP statement                                         #WhileStmt
    ;

expression:
    OP e = expression CP                                                       #ParenExpr
    | op = (NOT | SUB | ADD) e = expression                                    #UnaryOp
    | lhs = expression op = (MUL | DIV | MOD) rhs = expression                 #BinaryMulOp
    | lhs = expression op = (ADD | SUB) rhs = expression                       #BinaryAddOp
    | lhs = expression op = (EQ | NEQ | LT | GT | LTE | GTE) rhs = expression  #BinaryRelOp
    | lhs = expression op = (AND | OR) rhs = expression                        #BinaryLogOp
    | <assoc = right> IDENTIFIER ASSIGN expression                             #AssignOp
    | IDENTIFIER OP arguments CP                                               #CallExpr
    | IDENTIFIER                                                               #VarExpr
    | NUMBER                                                                   #LiteralExpr
    ;

arguments:
    expression (COMMA expression)*
    |
    ;
