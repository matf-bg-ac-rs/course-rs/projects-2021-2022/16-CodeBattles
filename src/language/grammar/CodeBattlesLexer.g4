lexer grammar CodeBattlesLexer;

IF: 'if';
ELSE: 'else';
WHILE: 'while';

DOT: '.';
COLON: ':';
SEMICOLON: ';';
COMMA: ',';

OP: '(';
CP: ')';
OBR: '[';
CBR: ']';
OCB: '{';
CCB: '}';

ADD: '+';
SUB: '-';
MUL: '*';
DIV: '/';
MOD: '%';

LT: '<';
GT: '>';
LTE: '<=';
GTE: '>=';
EQ: '==';
NEQ: '!=';

NOT: '!';
AND: '&&';
OR: '||';

ASSIGN: '=';

fragment NONZERODIGIT: [1-9];
fragment DIGIT: [0-9];
fragment INTEGER: DIGIT+;
fragment EXPONENT: [eE] INTEGER;

NUMBER: ((INTEGER? '.' INTEGER) | INTEGER) EXPONENT?;

fragment LOWERCASE: [a-z];
fragment UPPERCASE: [A-Z];
fragment LETTER: LOWERCASE | UPPERCASE;
fragment WORD_CHARACTER: LETTER | DIGIT | '_';
IDENTIFIER: (LETTER | '_') WORD_CHARACTER*;

fragment NEWLINE: '\r' | '\n' | '\r\n';
WHITESPACE: ([ \t] | NEWLINE) -> skip;
