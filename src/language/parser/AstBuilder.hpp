#ifndef CODEBATTLES_LANGUAGE_PARSER_ASTBUILDER_HPP_
#define CODEBATTLES_LANGUAGE_PARSER_ASTBUILDER_HPP_

#include <language/parser/generated/CodeBattlesParserBaseVisitor.h>

class AstBuilder : public CodeBattlesParserBaseVisitor
{
public:
    AstBuilder() = default;
    virtual antlrcpp::Any visitProgram(CodeBattlesParser::ProgramContext* ctx) override;
    virtual antlrcpp::Any visitBlock(CodeBattlesParser::BlockContext* ctx) override;
    virtual antlrcpp::Any visitEmptyStmt(CodeBattlesParser::EmptyStmtContext* ctx) override;
    virtual antlrcpp::Any visitExprStmt(CodeBattlesParser::ExprStmtContext* ctx) override;
    virtual antlrcpp::Any visitBlockStmt(CodeBattlesParser::BlockStmtContext* ctx) override;
    virtual antlrcpp::Any visitIfStmt(CodeBattlesParser::IfStmtContext* ctx) override;
    virtual antlrcpp::Any visitIfElseStmt(CodeBattlesParser::IfElseStmtContext* ctx) override;
    virtual antlrcpp::Any visitWhileStmt(CodeBattlesParser::WhileStmtContext* ctx) override;
    virtual antlrcpp::Any visitUnaryOp(CodeBattlesParser::UnaryOpContext* ctx) override;
    virtual antlrcpp::Any visitBinaryAddOp(CodeBattlesParser::BinaryAddOpContext* ctx) override;
    virtual antlrcpp::Any visitParenExpr(CodeBattlesParser::ParenExprContext* ctx) override;
    virtual antlrcpp::Any visitBinaryLogOp(CodeBattlesParser::BinaryLogOpContext* ctx) override;
    virtual antlrcpp::Any visitBinaryMulOp(CodeBattlesParser::BinaryMulOpContext* ctx) override;
    virtual antlrcpp::Any visitLiteralExpr(CodeBattlesParser::LiteralExprContext* ctx) override;
    virtual antlrcpp::Any visitVarExpr(CodeBattlesParser::VarExprContext* ctx) override;
    virtual antlrcpp::Any visitCallExpr(CodeBattlesParser::CallExprContext *ctx) override;
    virtual antlrcpp::Any visitBinaryRelOp(CodeBattlesParser::BinaryRelOpContext* ctx) override;
    virtual antlrcpp::Any visitAssignOp(CodeBattlesParser::AssignOpContext* ctx) override;
};

#endif // CODEBATTLES_LANGUAGE_PARSER_ASTBUILDER_HPP_
