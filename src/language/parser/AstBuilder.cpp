#include <language/parser/AstBuilder.hpp>

#include <language/ast/ast.hpp>

using namespace ast;

antlrcpp::Any AstBuilder::visitProgram(CodeBattlesParser::ProgramContext* ctx)
{
    auto program = new BlockStatement;
    for (const auto& stmt : ctx->statement()) {
        auto statement = visit(stmt).as<Statement*>();
        program->append(std::unique_ptr<Statement>{statement});
    }
    return static_cast<Node*>(program);
}

antlrcpp::Any AstBuilder::visitBlock(CodeBattlesParser::BlockContext* ctx)
{
    auto block = new BlockStatement;
    for (const auto& stmt : ctx->statement()) {
        auto statement = visit(stmt).as<Statement*>();
        block->append(std::unique_ptr<Statement>{statement});
    }
    return static_cast<Statement*>(block);
}

antlrcpp::Any AstBuilder::visitEmptyStmt(CodeBattlesParser::EmptyStmtContext* /* ctx */)
{
    return static_cast<Statement*>(new EmptyStatement);
}

antlrcpp::Any AstBuilder::visitExprStmt(CodeBattlesParser::ExprStmtContext* ctx)
{
    auto expression = visit(ctx->expression()).as<Expression*>();
    auto statement = new ExpressionStatement{std::unique_ptr<Expression>(expression)};
    return static_cast<Statement*>(statement);
}

antlrcpp::Any AstBuilder::visitBlockStmt(CodeBattlesParser::BlockStmtContext* ctx)
{
    return visit(ctx->block());
}

antlrcpp::Any AstBuilder::visitIfStmt(CodeBattlesParser::IfStmtContext* ctx)
{
    auto cond = visit(ctx->cond).as<Expression*>();
    auto ifBranch = visit(ctx->ifBr).as<Statement*>();
    auto ifStmt = new BranchStatement{std::unique_ptr<Expression>{cond},
                                      std::unique_ptr<Statement>{ifBranch},
                                      nullptr};
    return static_cast<Statement*>(ifStmt);
}

antlrcpp::Any AstBuilder::visitIfElseStmt(CodeBattlesParser::IfElseStmtContext* ctx)
{
    auto cond = visit(ctx->cond).as<Expression*>();
    auto ifBranch = visit(ctx->ifBr).as<Statement*>();
    auto elseBranch = visit(ctx->elseBr).as<Statement*>();
    auto ifStmt = new BranchStatement{std::unique_ptr<Expression>{cond},
                                      std::unique_ptr<Statement>{ifBranch},
                                      std::unique_ptr<Statement>{elseBranch}};
    return static_cast<Statement*>(ifStmt);
}

antlrcpp::Any AstBuilder::visitWhileStmt(CodeBattlesParser::WhileStmtContext* ctx)
{
    auto cond = visit(ctx->expression()).as<Expression*>();
    auto loop = visit(ctx->statement()).as<Statement*>();
    auto whileStmt = new LoopStatement{std::unique_ptr<Expression>{cond},
                                       std::unique_ptr<Statement>{loop}};
    return static_cast<Statement*>(whileStmt);
}

antlrcpp::Any AstBuilder::visitParenExpr(CodeBattlesParser::ParenExprContext* ctx)
{
    return visit(ctx->expression());
}

antlrcpp::Any AstBuilder::visitLiteralExpr(CodeBattlesParser::LiteralExprContext* ctx)
{
    auto text = ctx->NUMBER()->getText();
    auto value = std::strtod(text.c_str(), nullptr);
    auto literal = new NumberExpression{value};
    return static_cast<Expression*>(literal);
}

antlrcpp::Any AstBuilder::visitVarExpr(CodeBattlesParser::VarExprContext* ctx)
{
    auto name = ctx->IDENTIFIER()->getText();
    auto variable = new VariableExpression{name};
    return static_cast<Expression*>(variable);
}

antlrcpp::Any AstBuilder::visitCallExpr(CodeBattlesParser::CallExprContext* ctx)
{
    auto call = new CallExpression{ctx->IDENTIFIER()->getText()};
    for (auto arg : ctx->arguments()->expression())
    {
        auto any = visit(arg);
        auto expr = any.as<Expression*>();
        call->arguments()->emplace_back(expr);
    }
    return static_cast<Expression*>(call);
}

antlrcpp::Any AstBuilder::visitAssignOp(CodeBattlesParser::AssignOpContext* ctx)
{
    auto name = ctx->IDENTIFIER()->getText();
    auto expr = visit(ctx->expression()).as<Expression*>();
    auto assign = new AssignExpression{name, std::unique_ptr<Expression>{expr}};
    return static_cast<Expression*>(assign);
}

namespace {

antlrcpp::Any buildUnaryOp(const std::string& op, Expression* expr)
{
    Expression* unary = nullptr;

    if (op == "+") {
        unary = expr;
    } else if (op == "-") {
        unary = new NegOperator{std::unique_ptr<Expression>{expr}};
    } else if (op == "!") {
        unary = new NotOperator{std::unique_ptr<Expression>{expr}};
    }

    return static_cast<Expression*>(unary);
}

antlrcpp::Any buildBinaryOp(Expression* lhs,
                            const std::string& op,
                            Expression* rhs)
{
    Expression* binary = nullptr;

    if (op == "+") {
        binary = new AddOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "-") {
        binary = new SubOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "*") {
        binary = new MulOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "/") {
        binary = new DivOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "%") {
        binary = new ModOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "==") {
        binary = new EqOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "!=") {
        binary = new NeqOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "<") {
        binary = new LtOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == ">") {
        binary = new GtOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "<=") {
        binary = new LteOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == ">=") {
        binary = new GteOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "&&") {
        binary = new AndOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    } else if (op == "||") {
        binary = new OrOperator{std::unique_ptr<Expression>{lhs},
                                 std::unique_ptr<Expression>{rhs}};
    }

    return static_cast<Expression*>(binary);
}

} // namespace

antlrcpp::Any AstBuilder::visitUnaryOp(CodeBattlesParser::UnaryOpContext* ctx)
{
    auto expr = visit(ctx->expression()).as<Expression*>();
    auto op = ctx->op->getText();

    return buildUnaryOp(op, expr);
}

antlrcpp::Any AstBuilder::visitBinaryAddOp(CodeBattlesParser::BinaryAddOpContext* ctx)
{
    auto lhs = visit(ctx->lhs).as<Expression*>();
    auto rhs = visit(ctx->rhs).as<Expression*>();
    auto op = ctx->op->getText();

    return buildBinaryOp(lhs, op, rhs);
}

antlrcpp::Any AstBuilder::visitBinaryMulOp(CodeBattlesParser::BinaryMulOpContext* ctx)
{
    auto lhs = visit(ctx->lhs).as<Expression*>();
    auto rhs = visit(ctx->rhs).as<Expression*>();
    auto op = ctx->op->getText();

    return buildBinaryOp(lhs, op, rhs);
}

antlrcpp::Any AstBuilder::visitBinaryRelOp(CodeBattlesParser::BinaryRelOpContext* ctx)
{
    auto lhs = visit(ctx->lhs).as<Expression*>();
    auto rhs = visit(ctx->rhs).as<Expression*>();
    auto op = ctx->op->getText();

    return buildBinaryOp(lhs, op, rhs);
}

antlrcpp::Any AstBuilder::visitBinaryLogOp(CodeBattlesParser::BinaryLogOpContext* ctx)
{
    auto lhs = visit(ctx->lhs).as<Expression*>();
    auto rhs = visit(ctx->rhs).as<Expression*>();
    auto op = ctx->op->getText();

    return buildBinaryOp(lhs, op, rhs);
}
