#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_EXECUTION_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_EXECUTION_HPP_

#include <memory>

#include <language/interpreter/Instruction.hpp>
#include <language/interpreter/Stack.hpp>

class Interpreter;

class Execution
{
public:
    Execution() = default;
    Execution(Interpreter* interpreter);

    Stack<double>& values();
    Stack<std::shared_ptr<Instruction>>& instructions();
    Interpreter* interpreter();

private:
    Stack<double> values_;
    Stack<std::shared_ptr<Instruction>> instructions_;
    Interpreter* interpreter_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_EXECUTION_HPP_
