#include <language/interpreter/Instruction.hpp>

#include <cmath>
#include <stdexcept>

#include <language/interpreter/Execution.hpp>
#include <language/interpreter/Interpreter.hpp>

Instruction::Instruction(std::shared_ptr<Environment> env)
    : env_{env} {}
Instruction::~Instruction() {}

void Instruction::push(const ast::Node* node)
{
    auto& builder = env_->execution()->interpreter()->builder();
    auto instruction = builder.build(node, env_);
    env_->execution()->instructions().push(instruction);
}

NoOpInstruction::NoOpInstruction(std::shared_ptr<Environment> env) : Instruction{env} {}

void NoOpInstruction::execute() {}

bool NoOpInstruction::complete() const
{
    return true;
}

ExprStmtInstruction::ExprStmtInstruction(std::shared_ptr<Environment> env, const ast::ExpressionStatement* stmt)
    : Instruction{env}, expr_{stmt->expression()}, state_{State::EVALUATE} {}

void ExprStmtInstruction::execute()
{
    if (state_ == State::EVALUATE) {
        push(expr_);
        state_ = State::CLEAR;
    } else if (state_ == State::CLEAR) {
        env_->execution()->values().clear();
        state_ = State::FINISH;
    }
}

bool ExprStmtInstruction::complete() const
{
    return state_ == State::FINISH;
}

BlockInstruction::BlockInstruction(std::shared_ptr<Environment> env, const ast::BlockStatement* block)
    : Instruction{env}, stmts_{}, state_{0}
{
    for (auto it = block->statements()->cbegin(); it != block->statements()->cend(); ++it)
        stmts_.push_back(it->get());
}

void BlockInstruction::execute()
{
    if (state_ < stmts_.size()) {
        push(stmts_[state_++]);
    }
}

bool BlockInstruction::complete() const
{
    return state_ == stmts_.size();
}

IfInstruction::IfInstruction(std::shared_ptr<Environment> env, const ast::BranchStatement* branch)
    : Instruction{env},
      cond_{branch->condition()},
      then_{branch->thenBranch()},
      else_{branch->elseBranch()},
      state_{State::EVALUATE} {}

void IfInstruction::execute()
{
    if (state_ == State::EVALUATE) {
        push(cond_);
        state_ = State::BRANCH;
    } else if (state_ == State::BRANCH) {
        bool cond = static_cast<bool>(env_->execution()->values().pop());
        if (cond) {
            if (then_)
                push(then_);
        } else {
            if (else_)
                push(else_);
        }
        state_ = State::FINISH;
    }
}

bool IfInstruction::complete() const
{
    return state_ == State::FINISH;
}

WhileInstruction::WhileInstruction(std::shared_ptr<Environment> env, const ast::LoopStatement* loop)
    : Instruction{env},
      cond_{loop->condition()},
      loop_{loop->loop()},
      state_{State::EVALUATE} {}

void WhileInstruction::execute()
{
    if (state_ == State::EVALUATE) {
        push(cond_);
        state_ = State::LOOP;
    } else if (state_ == State::LOOP) {
        bool loop = static_cast<bool>(env_->execution()->values().pop());
        if (loop) {
            push(loop_);
            state_ = State::EVALUATE;
        } else {
            state_ = State::FINISH;
        }
    }
}

bool WhileInstruction::complete() const
{
    return state_ == State::FINISH;
}

NumberInstruction::NumberInstruction(std::shared_ptr<Environment> env, const ast::NumberExpression* number)
    : Instruction{env}, num_{number->number()}, pushed_{false} {}

void NumberInstruction::execute()
{
    env_->execution()->values().push(num_);
    pushed_ = true;
}

bool NumberInstruction::complete() const
{
    return pushed_;
}

VariableInstruction::VariableInstruction(std::shared_ptr<Environment> env, const ast::VariableExpression* variable)
    : Instruction{env}, var_{variable->variable()}, pushed_{false} {}

void VariableInstruction::execute()
{
    double value = env_->get(var_);
    env_->execution()->values().push(value);
    pushed_ = true;
}

bool VariableInstruction::complete() const
{
    return pushed_;
}

AssignInstruction::AssignInstruction(std::shared_ptr<Environment> env, const ast::AssignExpression* assign)
    : Instruction{env},
      var_{assign->variable()},
      expr_{assign->expression()},
      state_{State::EVALUATE} {}

void AssignInstruction::execute()
{
    if (state_ == State::EVALUATE) {
        push(expr_);
        state_ = State::ASSIGN;
    } else if (state_ == State::ASSIGN) {
        double value = env_->execution()->values().pop();
        env_->set(var_, value);
        env_->execution()->values().push(value);
        state_ = State::FINISH;
    }
}

bool AssignInstruction::complete() const
{
    return state_ == State::FINISH;
}

CallInstruction::CallInstruction(std::shared_ptr<Environment> env, const ast::CallExpression* call)
    : Instruction{env},
      fn_{call->function()},
      args_{},
      state_{State::EVALUATE}
{
    for (auto it = call->arguments()->cbegin(); it != call->arguments()->cend(); ++it)
        args_.push_back(it->get());
}

void CallInstruction::execute()
{
    if (state_ == State::EVALUATE) {
        for (auto rit = args_.crbegin(); rit != args_.crend(); ++rit)
            push(*rit);
        state_ = State::CALL;
    } else if (state_ == State::CALL) {
        std::vector<double> args(args_.size());
        for (auto rit = args.rbegin(); rit != args.rend(); ++rit)
            *rit = env_->execution()->values().pop();

        Callable* function = env_->execution()->interpreter()->function(fn_);
        function->call(env_, args);

        state_ = State::FINISH;
    }
}

bool CallInstruction::complete() const
{
    return state_ == State::FINISH;
}

UnaryInstruction::UnaryInstruction(std::shared_ptr<Environment> env, const ast::UnaryOperator* op)
    : Instruction{env},
      op_{op->expression()},
      state_{State::EVALUATE} {}

void UnaryInstruction::execute()
{
    if (state_ == State::EVALUATE) {
        push(op_);
        state_ = State::APPLY;
    } else if (state_ == State::APPLY) {
        apply();
        state_ = State::FINISH;
    }
}

bool UnaryInstruction::complete() const
{
    return state_ == State::FINISH;
}

BinaryInstruction::BinaryInstruction(std::shared_ptr<Environment> env, const ast::BinaryOperator* op)
    : Instruction{env},
      ops_{op->lhs(), op->rhs()},
      state_{State::EVALUATE_LHS} {}

void BinaryInstruction::execute()
{
    if (state_ == State::EVALUATE_LHS) {
        push(ops_[0]);
        state_ = State::EVALUATE_RHS;
    } else if (state_ == State::EVALUATE_RHS) {
        push(ops_[1]);
        state_ = State::APPLY;
    } else if (state_ == State::APPLY) {
        apply();
        state_ = State::FINISH;
    }
}

bool BinaryInstruction::complete() const
{
    return state_ == State::FINISH;
}

NotInstruction::NotInstruction(std::shared_ptr<Environment> env, const ast::NotOperator* op) : UnaryInstruction{env, op} {}
NegInstruction::NegInstruction(std::shared_ptr<Environment> env, const ast::NegOperator* op) : UnaryInstruction{env, op} {}
AddInstruction::AddInstruction(std::shared_ptr<Environment> env, const ast::AddOperator* op) : BinaryInstruction{env, op} {}
SubInstruction::SubInstruction(std::shared_ptr<Environment> env, const ast::SubOperator* op) : BinaryInstruction{env, op} {}
MulInstruction::MulInstruction(std::shared_ptr<Environment> env, const ast::MulOperator* op) : BinaryInstruction{env, op} {}
DivInstruction::DivInstruction(std::shared_ptr<Environment> env, const ast::DivOperator* op) : BinaryInstruction{env, op} {}
ModInstruction::ModInstruction(std::shared_ptr<Environment> env, const ast::ModOperator* op) : BinaryInstruction{env, op} {}
EqInstruction::EqInstruction(std::shared_ptr<Environment> env, const ast::EqOperator* op) : BinaryInstruction{env, op} {}
NeqInstruction::NeqInstruction(std::shared_ptr<Environment> env, const ast::NeqOperator* op) : BinaryInstruction{env, op} {}
LtInstruction::LtInstruction(std::shared_ptr<Environment> env, const ast::LtOperator* op) : BinaryInstruction{env, op} {}
GtInstruction::GtInstruction(std::shared_ptr<Environment> env, const ast::GtOperator* op) : BinaryInstruction{env, op} {}
LteInstruction::LteInstruction(std::shared_ptr<Environment> env, const ast::LteOperator* op) : BinaryInstruction{env, op} {}
GteInstruction::GteInstruction(std::shared_ptr<Environment> env, const ast::GteOperator* op) : BinaryInstruction{env, op} {}

template <typename In, typename Out, typename F>
void UnaryInstruction::unary(F f)
{
    In op = static_cast<In>(env_->execution()->values().pop());
    env_->execution()->values().push(static_cast<Out>(f(op)));
}

template <typename In, typename Out, typename F>
void BinaryInstruction::binary(F f)
{
    auto& values = env_->execution()->values();

    In op2 = static_cast<In>(values.pop());
    In op1 = static_cast<In>(values.pop());
    values.push(static_cast<Out>(f(op1, op2)));
}

void NotInstruction::apply()
{
    unary<bool, double, std::logical_not<bool>>();
}

void NegInstruction::apply()
{
    unary<double, double, std::negate<double>>();
}

void AddInstruction::apply()
{
    binary<double, double, std::plus<double>>();
}

void SubInstruction::apply()
{
    binary<double, double, std::minus<double>>();
}

void MulInstruction::apply()
{
    binary<double, double, std::multiplies<double>>();
}

void DivInstruction::apply()
{
    auto& values = env_->execution()->values();

    double op2 = values.pop();
    if (op2 == 0.0)
        throw std::runtime_error{"Runtime error: Divide by zero."};
    double op1 = values.pop();
    values.push(op1 / op2);
}

void ModInstruction::apply()
{
    auto& values = env_->execution()->values();

    double op2 = values.pop();
    if (op2 == 0.0)
        throw std::runtime_error{"Runtime error: Divide by zero."};
    double op1 = values.pop();
    values.push(std::fmod(op1, op2));
}

void EqInstruction::apply()
{
    binary<double, double, std::equal_to<double>>();
}

void NeqInstruction::apply()
{
    binary<double, double, std::not_equal_to<double>>();
}

void LtInstruction::apply()
{
    binary<double, double, std::less<double>>();
}

void GtInstruction::apply()
{
    binary<double, double, std::greater<double>>();
}

void LteInstruction::apply()
{
    binary<double, double, std::less_equal<double>>();
}

void GteInstruction::apply()
{
    binary<double, double, std::greater_equal<double>>();
}

AndInstruction::AndInstruction(std::shared_ptr<Environment> env, const ast::AndOperator* op)
    : Instruction{env},
      ops_{op->lhs(), op->rhs()},
      state_{State::EVALUATE_LHS} {}

void AndInstruction::execute()
{
    if (state_ == State::EVALUATE_LHS) {
        push(ops_[0]);
        state_ = State::EVALUATE_RHS;
    } else if (state_ == State::EVALUATE_RHS) {
        double value = env_->execution()->values().pop();

        if (value) {
            push(ops_[1]);
        } else {
            env_->execution()->values().push(value);
        }

        state_ = State::FINISH;
    }
}

bool AndInstruction::complete() const
{
    return state_ == State::FINISH;
}

OrInstruction::OrInstruction(std::shared_ptr<Environment> env, const ast::OrOperator* op)
    : Instruction{env},
      ops_{op->lhs(), op->rhs()},
      state_{State::EVALUATE_LHS} {}

void OrInstruction::execute()
{
    if (state_ == State::EVALUATE_LHS) {
        push(ops_[0]);
        state_ = State::EVALUATE_RHS;
    } else if (state_ == State::EVALUATE_RHS) {
        double value = env_->execution()->values().pop();

        if (!value) {
            push(ops_[1]);
        } else {
            env_->execution()->values().push(value);
        }

        state_ = State::FINISH;
    }
}

bool OrInstruction::complete() const
{
    return state_ == State::FINISH;
}
