#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_INSTRUCTION_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_INSTRUCTION_HPP_

#include <language/ast/ast.hpp>

#include <language/interpreter/Environment.hpp>

class Interpreter;

class Instruction
{
public:
    Instruction(std::shared_ptr<Environment> env);
    virtual ~Instruction();
    virtual void execute() = 0;
    virtual bool complete() const = 0;

protected:
    virtual void push(const ast::Node* node);

    std::shared_ptr<Environment> env_;
};

class NoOpInstruction : public Instruction
{
public:
    NoOpInstruction(std::shared_ptr<Environment> env);
    void execute() override;
    bool complete() const override;
};

class ExprStmtInstruction : public Instruction
{
public:
    ExprStmtInstruction(std::shared_ptr<Environment> env, const ast::ExpressionStatement* stmt);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE,
        CLEAR,
        FINISH
    };

    const ast::Expression* expr_;
    State state_;
};

class BlockInstruction : public Instruction
{
public:
    BlockInstruction(std::shared_ptr<Environment> env, const ast::BlockStatement* block);
    void execute() override;
    bool complete() const override;

private:
    std::vector<const ast::Statement*> stmts_;
    std::size_t state_;
};

class IfInstruction : public Instruction
{
public:
    IfInstruction(std::shared_ptr<Environment> env, const ast::BranchStatement* branch);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE,
        BRANCH,
        FINISH
    };

    const ast::Expression* cond_;
    const ast::Statement* then_;
    const ast::Statement* else_;
    State state_;
};

class WhileInstruction : public Instruction
{
public:
    WhileInstruction(std::shared_ptr<Environment> env, const ast::LoopStatement* loop);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE,
        LOOP,
        FINISH
    };

    const ast::Expression* cond_;
    const ast::Statement* loop_;
    State state_;
};

class NumberInstruction : public Instruction
{
public:
    NumberInstruction(std::shared_ptr<Environment> env, const ast::NumberExpression* number);
    void execute() override;
    bool complete() const override;

private:
    double num_;
    bool pushed_;
};

class VariableInstruction : public Instruction
{
public:
    VariableInstruction(std::shared_ptr<Environment> env, const ast::VariableExpression* variable);
    void execute() override;
    bool complete() const override;

private:
    std::string var_;
    bool pushed_;
};

class AssignInstruction : public Instruction
{
public:
    AssignInstruction(std::shared_ptr<Environment> env, const ast::AssignExpression* assign);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE,
        ASSIGN,
        FINISH
    };

    std::string var_;
    const ast::Expression* expr_;
    State state_;
};

class Callable;

class CallInstruction : public Instruction
{
public:
    CallInstruction(std::shared_ptr<Environment> env, const ast::CallExpression* call);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE,
        CALL,
        FINISH
    };

    std::string fn_;
    std::vector<const ast::Expression*> args_;
    State state_;
};

class UnaryInstruction : public Instruction
{
public:
    UnaryInstruction(std::shared_ptr<Environment> env, const ast::UnaryOperator* op);
    void execute() override;
    bool complete() const override;

protected:
    virtual void apply() = 0;

    template <typename In, typename Out, typename F>
    void unary(F f = F{});

private:
    enum class State
    {
        EVALUATE,
        APPLY,
        FINISH
    };

    const ast::Expression* op_;
    State state_;
};

class NotInstruction : public UnaryInstruction
{
public:
    NotInstruction(std::shared_ptr<Environment> env, const ast::NotOperator* op);

protected:
    void apply() override;
};

class NegInstruction : public UnaryInstruction
{
public:
    NegInstruction(std::shared_ptr<Environment> env, const ast::NegOperator* op);

protected:
    void apply() override;
};

class BinaryInstruction : public Instruction
{
public:
    BinaryInstruction(std::shared_ptr<Environment> env, const ast::BinaryOperator* op);
    void execute() override;
    bool complete() const override;

protected:
    virtual void apply() = 0;

    template <typename In, typename Out, typename F>
    void binary(F f = F{});

private:
    enum class State
    {
        EVALUATE_LHS,
        EVALUATE_RHS,
        APPLY,
        FINISH
    };

    const std::array<const ast::Expression*, 2> ops_;
    State state_;
};

class AddInstruction : public BinaryInstruction
{
public:
    AddInstruction(std::shared_ptr<Environment> env, const ast::AddOperator* op);

protected:
    void apply() override;
};

class SubInstruction : public BinaryInstruction
{
public:
    SubInstruction(std::shared_ptr<Environment> env, const ast::SubOperator* op);

protected:
    void apply() override;
};

class MulInstruction : public BinaryInstruction
{
public:
    MulInstruction(std::shared_ptr<Environment> env, const ast::MulOperator* op);

protected:
    void apply() override;
};

class DivInstruction : public BinaryInstruction
{
public:
    DivInstruction(std::shared_ptr<Environment> env, const ast::DivOperator* op);

protected:
    void apply() override;
};

class ModInstruction : public BinaryInstruction
{
public:
    ModInstruction(std::shared_ptr<Environment> env, const ast::ModOperator* op);

protected:
    void apply() override;
};

class EqInstruction : public BinaryInstruction
{
public:
    EqInstruction(std::shared_ptr<Environment> env, const ast::EqOperator* op);

protected:
    void apply() override;
};

class NeqInstruction : public BinaryInstruction
{
public:
    NeqInstruction(std::shared_ptr<Environment> env, const ast::NeqOperator* op);

protected:
    void apply() override;
};

class LtInstruction : public BinaryInstruction
{
public:
    LtInstruction(std::shared_ptr<Environment> env, const ast::LtOperator* op);

protected:
    void apply() override;
};

class GtInstruction : public BinaryInstruction
{
public:
    GtInstruction(std::shared_ptr<Environment> env, const ast::GtOperator* op);

protected:
    void apply() override;
};

class LteInstruction : public BinaryInstruction
{
public:
    LteInstruction(std::shared_ptr<Environment> env, const ast::LteOperator* op);

protected:
    void apply() override;
};

class GteInstruction : public BinaryInstruction
{
public:
    GteInstruction(std::shared_ptr<Environment> env, const ast::GteOperator* op);

protected:
    void apply() override;
};

class AndInstruction : public Instruction
{
public:
    AndInstruction(std::shared_ptr<Environment> env, const ast::AndOperator* op);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE_LHS,
        EVALUATE_RHS,
        FINISH
    };

    const std::array<const ast::Expression*, 2> ops_;
    State state_;
};

class OrInstruction : public Instruction
{
public:
    OrInstruction(std::shared_ptr<Environment> env, const ast::OrOperator* op);
    void execute() override;
    bool complete() const override;

private:
    enum class State
    {
        EVALUATE_LHS,
        EVALUATE_RHS,
        FINISH
    };

    const std::array<const ast::Expression*, 2> ops_;
    State state_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_INSTRUCTION_HPP_
