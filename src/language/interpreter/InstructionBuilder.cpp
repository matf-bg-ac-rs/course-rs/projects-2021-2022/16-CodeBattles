#include <language/interpreter/InstructionBuilder.hpp>

#include <stdexcept>

#include <language/ast/ast.hpp>
#include <language/interpreter/Instruction.hpp>
#include <language/interpreter/Environment.hpp>

InstructionBuilder::InstructionBuilder() : instr_{nullptr}, env_{nullptr} {}

std::shared_ptr<Instruction> InstructionBuilder::build(const ast::Node* node, std::shared_ptr<Environment> env)
{
    env_ = env;
    node->accept(this);
    return instr_;
}

void InstructionBuilder::set(std::shared_ptr<Environment> env)
{
    env_ = env;
}

void InstructionBuilder::visitEmptyStatement(const ast::EmptyStatement* /* empty */)
{
    instr_ = std::make_shared<NoOpInstruction>(env_);
}

void InstructionBuilder::visitExpressionStatement(const ast::ExpressionStatement* stmt)
{
    instr_ = std::make_shared<ExprStmtInstruction>(env_, stmt);
}

void InstructionBuilder::visitBlockStatement(const ast::BlockStatement* block)
{
    instr_ = std::make_shared<BlockInstruction>(env_, block);
}

void InstructionBuilder::visitBranchStatement(const ast::BranchStatement* branch)
{
    instr_ = std::make_shared<IfInstruction>(env_, branch);
}

void InstructionBuilder::visitLoopStatement(const ast::LoopStatement* loop)
{
    instr_ = std::make_shared<WhileInstruction>(env_, loop);
}

void InstructionBuilder::visitNumberExpression(const ast::NumberExpression* number)
{
    instr_ = std::make_shared<NumberInstruction>(env_, number);
}

void InstructionBuilder::visitVariableExpression(const ast::VariableExpression* variable)
{
    instr_ = std::make_shared<VariableInstruction>(env_, variable);
}

void InstructionBuilder::visitAssignExpression(const ast::AssignExpression* assign)
{
    instr_ = std::make_shared<AssignInstruction>(env_, assign);
}

void InstructionBuilder::visitCallExpression(const ast::CallExpression* call)
{
    instr_ = std::make_shared<CallInstruction>(env_, call);
}

void InstructionBuilder::visitNegOperator(const ast::NegOperator* neg)
{
    instr_ = std::make_shared<NegInstruction>(env_, neg);
}

void InstructionBuilder::visitAddOperator(const ast::AddOperator* add)
{
    instr_ = std::make_shared<AddInstruction>(env_, add);
}

void InstructionBuilder::visitSubOperator(const ast::SubOperator* sub)
{
    instr_ = std::make_shared<SubInstruction>(env_, sub);
}

void InstructionBuilder::visitMulOperator(const ast::MulOperator* mul)
{
    instr_ = std::make_shared<MulInstruction>(env_, mul);
}

void InstructionBuilder::visitDivOperator(const ast::DivOperator* div)
{
    instr_ = std::make_shared<DivInstruction>(env_, div);
}

void InstructionBuilder::visitModOperator(const ast::ModOperator* mod)
{
    instr_ = std::make_shared<ModInstruction>(env_, mod);
}

void InstructionBuilder::visitEqOperator(const ast::EqOperator* e)
{
    instr_ = std::make_shared<EqInstruction>(env_, e);
}

void InstructionBuilder::visitNeqOperator(const ast::NeqOperator* n)
{
    instr_ = std::make_shared<NeqInstruction>(env_, n);
}

void InstructionBuilder::visitLtOperator(const ast::LtOperator* lt)
{
    instr_ = std::make_shared<LtInstruction>(env_, lt);
}

void InstructionBuilder::visitGtOperator(const ast::GtOperator* gt)
{
    instr_ = std::make_shared<GtInstruction>(env_, gt);
}

void InstructionBuilder::visitLteOperator(const ast::LteOperator* lte)
{
    instr_ = std::make_shared<LteInstruction>(env_, lte);
}

void InstructionBuilder::visitGteOperator(const ast::GteOperator* gte)
{
    instr_ = std::make_shared<GteInstruction>(env_, gte);
}

void InstructionBuilder::visitNotOperator(const ast::NotOperator* n)
{
    instr_ = std::make_shared<NotInstruction>(env_, n);
}

void InstructionBuilder::visitAndOperator(const ast::AndOperator* a)
{
    instr_ = std::make_shared<AndInstruction>(env_, a);
}

void InstructionBuilder::visitOrOperator(const ast::OrOperator* o)
{
    instr_ = std::make_shared<OrInstruction>(env_, o);
}
