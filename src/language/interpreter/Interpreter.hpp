#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_INTERPRETER_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_INTERPRETER_HPP_

#include <memory>
#include <string>
#include <unordered_map>

#include <language/interpreter/Callable.hpp>
#include <language/interpreter/Execution.hpp>
#include <language/interpreter/Instruction.hpp>
#include <language/interpreter/InstructionBuilder.hpp>

namespace ast {
class Node;
} // namespace ast

class Callable;

class Interpreter
{
public:
    Interpreter();
    explicit Interpreter(const std::string& program);
    Interpreter(Interpreter&&) = default;
    Interpreter& operator=(Interpreter&& interpreter) = default;
    virtual ~Interpreter() = default;

    void link(const std::string& function, std::shared_ptr<Callable> implementation);

    //
    // Interpret controls
    //

    virtual void interpret();
    virtual bool step(int step_count = 30);

    //
    // Primitives
    //

    InstructionBuilder& builder();
    Callable* function(const std::string& fn);

private:
    virtual bool single_step();
    std::unique_ptr<const ast::Node> program_;
    std::unordered_map<std::string, std::shared_ptr<Callable>> functions_;
    Execution execution_;
    InstructionBuilder builder_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_INTERPRETER_HPP_
