#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_INSTRUCTIONBUILDER_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_INSTRUCTIONBUILDER_HPP_

#include <memory>

#include <language/ast/Visitor.hpp>

class Instruction;
class Environment;

class InstructionBuilder : public ast::Visitor
{
public:
    InstructionBuilder();
    std::shared_ptr<Instruction> build(const ast::Node* node, std::shared_ptr<Environment> env);
    void set(std::shared_ptr<Environment> env);

    virtual void visitEmptyStatement(const ast::EmptyStatement* empty) override;
    virtual void visitExpressionStatement(const ast::ExpressionStatement* stmt) override;
    virtual void visitBlockStatement(const ast::BlockStatement* block) override;
    virtual void visitBranchStatement(const ast::BranchStatement* branch) override;
    virtual void visitLoopStatement(const ast::LoopStatement* loop) override;

    virtual void visitNumberExpression(const ast::NumberExpression* number) override;
    virtual void visitVariableExpression(const ast::VariableExpression* variable) override;
    virtual void visitAssignExpression(const ast::AssignExpression* assign) override;
    virtual void visitCallExpression(const ast::CallExpression* print) override;

    virtual void visitNegOperator(const ast::NegOperator* neg) override;
    virtual void visitAddOperator(const ast::AddOperator* add) override;
    virtual void visitSubOperator(const ast::SubOperator* sub) override;
    virtual void visitMulOperator(const ast::MulOperator* mul) override;
    virtual void visitDivOperator(const ast::DivOperator* div) override;
    virtual void visitModOperator(const ast::ModOperator* mod) override;

    virtual void visitEqOperator(const ast::EqOperator* e) override;
    virtual void visitNeqOperator(const ast::NeqOperator* n) override;
    virtual void visitLtOperator(const ast::LtOperator* lt) override;
    virtual void visitGtOperator(const ast::GtOperator* gt) override;
    virtual void visitLteOperator(const ast::LteOperator* lte) override;
    virtual void visitGteOperator(const ast::GteOperator* gte) override;

    virtual void visitNotOperator(const ast::NotOperator* n) override;
    virtual void visitAndOperator(const ast::AndOperator* a) override;
    virtual void visitOrOperator(const ast::OrOperator* o) override;

private:
    std::shared_ptr<Instruction> instr_;
    std::shared_ptr<Environment> env_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_INSTRUCTIONBUILDER_HPP_
