#include <language/interpreter/Exception.hpp>

VariableInaccessibleException::VariableInaccessibleException(std::string variable) : variable_{variable}, message_{}
{
    message_.append("Interpreter error: Variable ");
    message_.append(variable_);
    message_.append(" is inaccessible.");
}

const std::string& VariableInaccessibleException::variable() const
{
    return variable_;
}

const char* VariableInaccessibleException::what() const throw ()
{
    return message_.c_str();
}

const char* StackEmptyException::what() const throw()
{
    return "Interpreter error: Attempted to access the top of an empty stack";
}

FunctionInaccessibleException::FunctionInaccessibleException(std::string function) : function_{function}, message_{}
{
    message_.append("Interpreter error: Function ");
    message_.append(function);
    message_.append(" is inaccessible.");
}

const std::string& FunctionInaccessibleException::function() const
{
    return function_;
}

const char* FunctionInaccessibleException::what() const throw ()
{
    return message_.c_str();
}
