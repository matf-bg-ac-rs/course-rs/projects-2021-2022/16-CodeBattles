#include <language/interpreter/Environment.hpp>

#include <memory>
#include <string>
#include <unordered_map>

#include <language/interpreter/Callable.hpp>
#include <language/interpreter/Execution.hpp>
#include <language/interpreter/Exception.hpp>

Environment::Environment(Execution* execution)
    : memory_{},
      exec_{execution} {}

double Environment::get(const std::string& variable) const
{
    auto it = memory_.find(variable);
    if (it != memory_.end())
        return it->second;
    throw VariableInaccessibleException{variable};
}

void Environment::set(const std::string& variable, double value)
{
    memory_[variable] = value;
}

Execution* Environment::execution()
{
    return exec_;
}
