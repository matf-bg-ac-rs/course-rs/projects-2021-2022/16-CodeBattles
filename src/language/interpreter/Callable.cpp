#include <language/interpreter/Callable.hpp>

#include <cmath>
#include <cassert>
#include <limits>
#include <iostream>

#include <language/interpreter/Execution.hpp>

Callable::~Callable() {}

// ------------------ UTILITIES ------------------

namespace {
double d2r(double degrees) {
    return degrees * M_PI / 180.0;
}

double r2d(double radians) {
    return radians * 180.0 / M_PI;
}
} // namespace

// ---------- LANGUAGE NATIVE FUNCTIONS ----------

void Print::call(std::shared_ptr<Environment> /* environment */, const std::vector<double>& args)
{
    if (args.size() > 0) {
        std::cout << args[0];

        for (std::size_t i = 1; i < args.size(); ++i) {
            std::cout << ", " << args[i];
        }
    }

    std::cout << std::endl;
}

void Sqrt::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(std::sqrt(args[0]));
}

void Abs::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(std::abs(args[0]));
}

void Sin::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(std::sin(d2r(args[0])));
}

void Cos::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(std::cos(d2r(args[0])));
}

void Tan::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(std::tan(d2r(args[0])));
}

void Asin::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(r2d(std::asin(args[0])));
}

void Acos::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(r2d(std::acos(args[0])));
}

void Atan::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(r2d(std::atan(args[0])));
}

void Atan2::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(r2d(std::atan2(args[0], args[1])));
}

void Floor::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    environment->execution()->values().push(std::floor(args[0]));
}

Random::Random() : random_{std::random_device{}()}, uniform_(0.0, 1.0) {}
Random::Random(unsigned int seed) : random_{seed}, uniform_(0.0, 1.0) {}
void Random::call(std::shared_ptr<Environment> environment, const std::vector<double>& args)
{
    assert(args.size() == 0);
    environment->execution()->values().push(uniform_(random_));
}
