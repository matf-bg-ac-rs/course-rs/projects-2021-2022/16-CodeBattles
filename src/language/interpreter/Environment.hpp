#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_ENVIRONMENT_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_ENVIRONMENT_HPP_

#include <memory>
#include <string>
#include <unordered_map>

class Execution;

class Environment
{
public:
    Environment(Execution* execution = nullptr);

    double get(const std::string& variable) const;
    void set(const std::string& variable, double value);
    Execution* execution();

private:
    std::unordered_map<std::string, double> memory_;
    Execution* exec_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_ENVIRONMENT_HPP_
