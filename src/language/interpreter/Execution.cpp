#include <language/interpreter/Execution.hpp>

#include <stdexcept>

#include <language/interpreter/Interpreter.hpp>

Execution::Execution(Interpreter* interpreter)
    : values_{},
      instructions_{},
      interpreter_{interpreter} {}

Stack<double>& Execution::values()
{
    return values_;
}

Stack<std::shared_ptr<Instruction>>& Execution::instructions()
{
    return instructions_;
}

Interpreter* Execution::interpreter()
{
    return interpreter_;
}
