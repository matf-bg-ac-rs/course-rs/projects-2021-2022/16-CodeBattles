#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_EXCEPTION_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_EXCEPTION_HPP_

#include <exception>
#include <string>

class Exception : std::exception
{
public:
    Exception() = default;
};

class VariableInaccessibleException : public Exception
{
public:
    VariableInaccessibleException(std::string variable);
    const char* what() const throw () override;
    const std::string& variable() const;

private:
    std::string variable_;
    std::string message_;
};

class StackEmptyException : public Exception
{
public:
    StackEmptyException() = default;
    const char* what() const throw () override;
};

class FunctionInaccessibleException : public Exception
{
public:
    FunctionInaccessibleException(std::string function);
    const char* what() const throw () override;
    const std::string& function() const;

private:
    std::string function_;
    std::string message_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_EXCEPTION_HPP_
