#include <language/interpreter/Interpreter.hpp>

#include <cmath>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>

#include <language/interpreter/Execution.hpp>

#include <antlr4-runtime.h>

#include <language/ast/ast.hpp>
#include <language/parser/AstBuilder.hpp>
#include <language/parser/generated/CodeBattlesLexer.h>
#include <language/parser/generated/CodeBattlesParser.h>

namespace {

ast::Node* textToAST(const std::string& program)
{
    std::stringstream is{program};
    antlr4::ANTLRInputStream input{is};
    CodeBattlesLexer lexer{&input};
    antlr4::CommonTokenStream stream{&lexer};
    CodeBattlesParser parser{&stream};
    auto tree = parser.program();

    AstBuilder builder;
    auto ast = builder.visit(tree).as<ast::Node*>();
    return ast;
};

} // namespace

Interpreter::Interpreter()
    : functions_{},
      execution_{this},
      builder_{}
{
    functions_.emplace("print", std::shared_ptr<Callable>{new Print});
    functions_.emplace("sqrt", std::shared_ptr<Callable>{new Sqrt});
    functions_.emplace("abs", std::shared_ptr<Callable>{new Abs});
    functions_.emplace("sin", std::shared_ptr<Callable>{new Sin});
    functions_.emplace("cos", std::shared_ptr<Callable>{new Cos});
    functions_.emplace("tan", std::shared_ptr<Callable>{new Tan});
    functions_.emplace("asin", std::shared_ptr<Callable>{new Asin});
    functions_.emplace("acos", std::shared_ptr<Callable>{new Acos});
    functions_.emplace("atan", std::shared_ptr<Callable>{new Atan});
    functions_.emplace("atan2", std::shared_ptr<Callable>{new Atan2});
    functions_.emplace("floor", std::shared_ptr<Callable>{new Floor});
    functions_.emplace("random", std::shared_ptr<Callable>{new Random(0)});
}

Interpreter::Interpreter(const std::string& program)
    :  Interpreter{}
{
    program_ = std::unique_ptr<const ast::Node>{textToAST(program)};
    if (program_ != nullptr) {
        auto env = std::make_shared<Environment>(&execution_);
        auto instruction = builder_.build(program_.get(), env);
        execution_.instructions().push(instruction);
    }
}

void Interpreter::interpret()
{
    while (step())
        ;
}

bool Interpreter::step(int step_count)
{
    for (int i = 0; i < step_count; i++) {
        if (!single_step()) {
            return false;
        }
    }
    return true;
}

bool Interpreter::single_step()
{
    if (execution_.instructions().empty())
        return false;

    auto instruction = execution_.instructions().pop();
    if (!instruction->complete()) {
        execution_.instructions().push(instruction);
        instruction->execute();
    }

    return true;
}

void Interpreter::link(const std::string& function, std::shared_ptr<Callable> implementation)
{
    functions_[function] = implementation;
}

Callable* Interpreter::function(const std::string& fn)
{
    auto it = functions_.find(fn);
    if (it == functions_.end())
        throw FunctionInaccessibleException{fn};
    return it->second.get();
}

InstructionBuilder& Interpreter::builder()
{
    return builder_;
}
