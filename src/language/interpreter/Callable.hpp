#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_CALLABLE_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_CALLABLE_HPP_

#include <memory>
#include <random>
#include <vector>

#include <language/interpreter/Environment.hpp>

class Callable
{
public:
    virtual ~Callable();
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) = 0;
};

// ---------- LANGUAGE NATIVE FUNCTIONS ----------

class Print : public Callable
{
public:
    Print() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

//
// Mathematics
//

class Sqrt : public Callable
{
public:
    Sqrt() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Abs : public Callable
{
public:
    Abs() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Sin : public Callable
{
public:
    Sin() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Cos : public Callable
{
public:
    Cos() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Tan : public Callable
{
public:
    Tan() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Asin : public Callable
{
public:
    Asin() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Acos : public Callable
{
public:
    Acos() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Atan : public Callable
{
public:
    Atan() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Atan2 : public Callable
{
public:
    Atan2() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Floor : public Callable
{
public:
    Floor() = default;
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;
};

class Random : public Callable
{
public:
    Random();
    Random(unsigned int seed);
    virtual void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override;

private:
    std::mt19937 random_;
    std::uniform_real_distribution<double> uniform_;
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_CALLABLE_HPP_
