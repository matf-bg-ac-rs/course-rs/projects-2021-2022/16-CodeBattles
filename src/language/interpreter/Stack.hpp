#ifndef CODEBATTLES_LANGUAGE_INTERPRETER_STACK_HPP_
#define CODEBATTLES_LANGUAGE_INTERPRETER_STACK_HPP_

#include <vector>

#include <language/interpreter/Exception.hpp>

template <typename T>
class Stack
{
private:
    std::vector<T> stack_;

public:
    Stack() = default;

    bool empty() const
    {
        return stack_.empty();
    }

    std::size_t size() const
    {
        return stack_.size();
    }

    void push(T e)
    {
        stack_.push_back(e);
    }

    T pop()
    {
        if (empty())
            throw StackEmptyException{};

        auto e = stack_.back();
        stack_.pop_back();
        return e;
    }

    void clear()
    {
        stack_.clear();
    }
};

#endif // CODEBATTLES_LANGUAGE_INTERPRETER_STACK_HPP_
