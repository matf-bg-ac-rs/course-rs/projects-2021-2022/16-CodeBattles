#include <language/ast/ast.hpp>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <language/ast/Visitor.hpp>

namespace ast {

Node::~Node() {}

//
// Statements
//

void EmptyStatement::accept(Visitor *visitor) const
{
    visitor->visitEmptyStatement(this);
}

ExpressionStatement::ExpressionStatement(std::unique_ptr<Expression>&& expression) : expr_{std::move(expression)} {}

void ExpressionStatement::accept(Visitor* visitor) const
{
    visitor->visitExpressionStatement(this);
}

const Expression* ExpressionStatement::expression() const
{
    return expr_.get();
}

void BlockStatement::accept(Visitor* visitor) const
{
    visitor->visitBlockStatement(this);
}

const std::vector<std::unique_ptr<Statement>>* BlockStatement::statements() const
{
    return &stmts_;
}

void BlockStatement::append(std::unique_ptr<Statement>&& statement)
{
    stmts_.push_back(std::move(statement));
}

BranchStatement::BranchStatement(std::unique_ptr<Expression>&& condition,
                                 std::unique_ptr<Statement>&& thenBranch,
                                 std::unique_ptr<Statement>&& elseBranch)
    : cond_{std::move(condition)}, then_{std::move(thenBranch)}, else_{std::move(elseBranch)} {}

void BranchStatement::accept(Visitor* visitor) const
{
    visitor->visitBranchStatement(this);
}

const Expression* BranchStatement::condition() const
{
    return cond_.get();
}
const Statement* BranchStatement::thenBranch() const
{
    return then_.get();
}
const Statement* BranchStatement::elseBranch() const
{
    return else_.get();
}

LoopStatement::LoopStatement(std::unique_ptr<Expression>&& condition, std::unique_ptr<Statement>&& loop)
    : cond_{std::move(condition)}, loop_{std::move(loop)} {}

const Expression* LoopStatement::condition() const
{
    return cond_.get();
}

const Statement* LoopStatement::loop() const
{
    return loop_.get();
}

void LoopStatement::accept(Visitor* visitor) const
{
    visitor->visitLoopStatement(this);
}

//
// Expressions
//

NumberExpression::NumberExpression(double number) : num_{number} {}

void NumberExpression::accept(Visitor* visitor) const
{
    visitor->visitNumberExpression(this);
}

double NumberExpression::number() const
{
    return num_;
}

VariableExpression::VariableExpression(std::string variable) : var_{std::move(variable)} {}

void VariableExpression::accept(Visitor* visitor) const
{
    visitor->visitVariableExpression(this);
}

const std::string& VariableExpression::variable() const
{
    return var_;
}

AssignExpression::AssignExpression(std::string variable, std::unique_ptr<Expression>&& expression)
    : var_{std::move(variable)}, expr_{std::move(expression)} {}

void AssignExpression::accept(Visitor* visitor) const
{
    visitor->visitAssignExpression(this);
}

const std::string& AssignExpression::variable() const
{
    return var_;
}

const Expression* AssignExpression::expression() const
{
    return expr_.get();
}

CallExpression::CallExpression(std::string function) : fn_{std::move(function)} {}

void CallExpression::accept(Visitor* visitor) const
{
    visitor->visitCallExpression(this);
}

const std::string& CallExpression::function() const
{
    return fn_;
}

const std::vector<std::unique_ptr<Expression>>* CallExpression::arguments() const
{
    return &args_;
}

std::vector<std::unique_ptr<Expression>>* CallExpression::arguments()
{
    return &args_;
}

UnaryOperator::UnaryOperator(std::unique_ptr<Expression>&& expression) : expr_{std::move(expression)} {}

const Expression* UnaryOperator::expression() const
{
    return expr_.get();
}

NotOperator::NotOperator(std::unique_ptr<Expression>&& expression) : UnaryOperator{std::move(expression)} {}

void NotOperator::accept(Visitor* visitor) const
{
    return visitor->visitNotOperator(this);
}

NegOperator::NegOperator(std::unique_ptr<Expression>&& expression) : UnaryOperator{std::move(expression)} {}

void NegOperator::accept(Visitor* visitor) const
{
    return visitor->visitNegOperator(this);
}

BinaryOperator::BinaryOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : lhs_{std::move(lhs)}, rhs_{std::move(rhs)} {}

const Expression* BinaryOperator::lhs() const
{
    return lhs_.get();
}

const Expression* BinaryOperator::rhs() const
{
    return rhs_.get();
}

AddOperator::AddOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void AddOperator::accept(Visitor* visitor) const
{
    visitor->visitAddOperator(this);
}

SubOperator::SubOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void SubOperator::accept(Visitor* visitor) const
{
    visitor->visitSubOperator(this);
}

MulOperator::MulOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void MulOperator::accept(Visitor* visitor) const
{
    visitor->visitMulOperator(this);
}

DivOperator::DivOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void DivOperator::accept(Visitor* visitor) const
{
    visitor->visitDivOperator(this);
}

ModOperator::ModOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void ModOperator::accept(Visitor* visitor) const
{
    visitor->visitModOperator(this);
}

EqOperator::EqOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void EqOperator::accept(Visitor* visitor) const
{
    visitor->visitEqOperator(this);
}

NeqOperator::NeqOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void NeqOperator::accept(Visitor* visitor) const
{
    visitor->visitNeqOperator(this);
}

LtOperator::LtOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void LtOperator::accept(Visitor* visitor) const
{
    visitor->visitLtOperator(this);
}

GtOperator::GtOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void GtOperator::accept(Visitor* visitor) const
{
    visitor->visitGtOperator(this);
}

LteOperator::LteOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void LteOperator::accept(Visitor* visitor) const
{
    visitor->visitLteOperator(this);
}

GteOperator::GteOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void GteOperator::accept(Visitor* visitor) const
{
    visitor->visitGteOperator(this);
}

AndOperator::AndOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void AndOperator::accept(Visitor* visitor) const
{
    visitor->visitAndOperator(this);
}

OrOperator::OrOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs)
    : BinaryOperator(std::move(lhs), std::move(rhs)) {}

void OrOperator::accept(Visitor* visitor) const
{
    visitor->visitOrOperator(this);
}

} // namespace ast
