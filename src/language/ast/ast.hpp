#ifndef CODEBATTLES_LANGUAGE_AST_AST_HPP_
#define CODEBATTLES_LANGUAGE_AST_AST_HPP_

#include <memory>
#include <string>
#include <vector>

namespace ast {

class Visitor;

class Node
{
public:
    virtual ~Node();
    virtual void accept(Visitor* visitor) const = 0;
};

class Statement;
class Expression;

//
// Statements
//

class Statement : public Node
{
};

class EmptyStatement : public Statement
{
public:
    EmptyStatement() = default;
    virtual void accept(Visitor* visitor) const override;
};

class ExpressionStatement : public Statement
{
public:
    ExpressionStatement(std::unique_ptr<Expression>&& expression);
    virtual void accept(Visitor* visitor) const override;
    const Expression* expression() const;

private:
    std::unique_ptr<Expression> expr_;
};

class BlockStatement : public Statement
{
public:
    BlockStatement() = default;
    virtual void accept(Visitor* visitor) const override;
    const std::vector<std::unique_ptr<Statement>>* statements() const;

    void append(std::unique_ptr<Statement>&& statement);

private:
    std::vector<std::unique_ptr<Statement>> stmts_;
};

class BranchStatement : public Statement
{
public:
    BranchStatement(std::unique_ptr<Expression>&& condition,
                    std::unique_ptr<Statement>&& thenBranch,
                    std::unique_ptr<Statement>&& elseBranch);
    virtual void accept(Visitor* visitor) const override;

    const Expression* condition() const;
    const Statement* thenBranch() const;
    const Statement* elseBranch() const;

private:
    std::unique_ptr<Expression> cond_;
    std::unique_ptr<Statement> then_;
    std::unique_ptr<Statement> else_;
};

class LoopStatement : public Statement
{
public:
    LoopStatement(std::unique_ptr<Expression>&& condition, std::unique_ptr<Statement>&& loop);
    virtual void accept(Visitor* visitor) const override;

    const Expression* condition() const;
    const Statement* loop() const;

private:
    std::unique_ptr<Expression> cond_;
    std::unique_ptr<Statement> loop_;
};

//
// Expressions
//

class Expression : public Node
{
};

class NumberExpression : public Expression
{
public:
    NumberExpression(double number);
    virtual void accept(Visitor* visitor) const override;
    double number() const;

private:
    double num_;
};

class VariableExpression : public Expression
{
public:
    VariableExpression(std::string variable);
    virtual void accept(Visitor* visitor) const override;
    const std::string& variable() const;

private:
    std::string var_;
};

class AssignExpression : public Expression
{
public:
    AssignExpression(std::string variable, std::unique_ptr<Expression>&& expression);
    virtual void accept(Visitor* visitor) const override;
    const std::string& variable() const;
    const Expression* expression() const;

private:
    std::string var_;
    std::unique_ptr<Expression> expr_;
};

class CallExpression : public Expression
{
public:
    CallExpression(std::string function);
    virtual void accept(Visitor* visitor) const override;
    const std::string& function() const;
    const std::vector<std::unique_ptr<Expression>>* arguments() const;
    std::vector<std::unique_ptr<Expression>>* arguments();

private:
    std::string fn_;
    std::vector<std::unique_ptr<Expression>> args_;
};

class UnaryOperator : public Expression
{
public:
    UnaryOperator(std::unique_ptr<Expression>&& expression);
    const Expression* expression() const;

private:
    std::unique_ptr<Expression> expr_;
};

class NotOperator : public UnaryOperator
{
public:
    NotOperator(std::unique_ptr<Expression>&& expression);
    virtual void accept(Visitor* visitor) const override;
};

class NegOperator : public UnaryOperator
{
public:
    NegOperator(std::unique_ptr<Expression>&& expression);
    virtual void accept(Visitor* visitor) const override;
};

class BinaryOperator : public Expression
{
public:
    BinaryOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    const Expression* lhs() const;
    const Expression* rhs() const;

protected:
    std::unique_ptr<Expression> lhs_;
    std::unique_ptr<Expression> rhs_;
};

class AddOperator : public BinaryOperator
{
public:
    AddOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class SubOperator : public BinaryOperator
{
public:
    SubOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class MulOperator : public BinaryOperator
{
public:
    MulOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class DivOperator : public BinaryOperator
{
public:
    DivOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class ModOperator : public BinaryOperator
{
public:
    ModOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class EqOperator : public BinaryOperator
{
public:
    EqOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class NeqOperator : public BinaryOperator
{
public:
    NeqOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class LtOperator : public BinaryOperator
{
public:
    LtOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class GtOperator : public BinaryOperator
{
public:
    GtOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class LteOperator : public BinaryOperator
{
public:
    LteOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class GteOperator : public BinaryOperator
{
public:
    GteOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class AndOperator : public BinaryOperator
{
public:
    AndOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

class OrOperator : public BinaryOperator
{
public:
    OrOperator(std::unique_ptr<Expression>&& lhs, std::unique_ptr<Expression>&& rhs);
    virtual void accept(Visitor* visitor) const override;
};

} // namespace ast

#endif  // CODEBATTLES_LANGUAGE_AST_AST_HPP_
