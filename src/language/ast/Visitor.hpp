#ifndef CODEBATTLES_LANGUAGE_AST_VISITOR_HPP_
#define CODEBATTLES_LANGUAGE_AST_VISITOR_HPP_

#include <language/ast/ast.hpp>

namespace ast {

class Visitor
{
public:
    virtual ~Visitor() = default;

    virtual void visitEmptyStatement(const EmptyStatement* empty) = 0;
    virtual void visitExpressionStatement(const ExpressionStatement* stmt) = 0;
    virtual void visitBlockStatement(const BlockStatement* block) = 0;
    virtual void visitBranchStatement(const BranchStatement* branch) = 0;
    virtual void visitLoopStatement(const LoopStatement* loop) = 0;

    virtual void visitNumberExpression(const NumberExpression* number) = 0;
    virtual void visitVariableExpression(const VariableExpression* variable) = 0;
    virtual void visitAssignExpression(const AssignExpression* assign) = 0;
    virtual void visitCallExpression(const CallExpression* print) = 0;

    virtual void visitNegOperator(const NegOperator* neg) = 0;
    virtual void visitAddOperator(const AddOperator* add) = 0;
    virtual void visitSubOperator(const SubOperator* sub) = 0;
    virtual void visitMulOperator(const MulOperator* mul) = 0;
    virtual void visitDivOperator(const DivOperator* div) = 0;
    virtual void visitModOperator(const ModOperator* mod) = 0;

    virtual void visitEqOperator(const EqOperator* e) = 0;
    virtual void visitNeqOperator(const NeqOperator* n) = 0;
    virtual void visitLtOperator(const LtOperator* lt) = 0;
    virtual void visitGtOperator(const GtOperator* gt) = 0;
    virtual void visitLteOperator(const LteOperator* lte) = 0;
    virtual void visitGteOperator(const GteOperator* gte) = 0;

    virtual void visitNotOperator(const NotOperator* n) = 0;
    virtual void visitAndOperator(const AndOperator* a) = 0;
    virtual void visitOrOperator(const OrOperator* o) = 0;
};

} // namespace ast

#endif // CODEBATTLES_LANGUAGE_AST_VISITOR_HPP_
