#include "GameEngine.hpp"
#include <iostream>
#include "Entity.hpp"
#include "Component.hpp"
#include "BattleScene.hpp"
#include "../Window.hpp"

GameEngine::GameEngine(Window* window_ptr) : window_ptr_(window_ptr)  {}

void GameEngine::run() {
    // Setup battle scene
    scenes_[SceneTag::Main] = std::make_unique<BattleScene>(this);
    changeScene(SceneTag::Main);
}

void GameEngine::update() {
    if (!running_) {
        return;
    }
    currentScene()->update();
}

void GameEngine::changeScene(SceneTag scene_tag) {
    current_scene_ = scene_tag;
}

Assets& GameEngine::assets() {
    return assets_;
}

Window* GameEngine::window() {
    return window_ptr_;
}

bool GameEngine::isRunning() {
    return running_;
}

void GameEngine::setIsRunning() {
    running_ = true;
}

Scene* GameEngine::currentScene() {
    return scenes_.at(current_scene_).get();
}
