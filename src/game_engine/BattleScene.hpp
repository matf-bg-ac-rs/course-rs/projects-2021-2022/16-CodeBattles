#ifndef BATTLESCENE_H
#define BATTLESCENE_H

#include "Scene.hpp"
#include <string>
#include <array>
#include <vector>
#include "../language/interpreter/Interpreter.hpp"
#include "BattleSceneInterpreterConfigurator.hpp"

class BattleScene final : public Scene {
public:
    BattleScene(GameEngine* game_engine_ptr);
    void update() override;
private:
    static constexpr size_t BULLET_HEALTH_DAMAGE = 5;

    Vec2 calculateBotStartingPosition(int idx) const;
    bool containsBotInbetween(Entity source, float start_angle, float end_angle);
    void spawnBots();
    void spawnBullet(Entity bot, float angle);

    void sRender() override;

    void sInterpreters();
    void sMovement();
    void sCollision();
    void sLifespan();
    void sDebug();
    // yiiiiikesssss
    friend class BattleSceneInterpreterConfigurator;

    std::unique_ptr<BattleSceneInterpreterConfigurator> configurator;
};

#endif // BATTLESCENE_H
