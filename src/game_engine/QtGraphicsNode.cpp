#include "QtGraphicsNode.hpp"

QtGraphicsNode::QtGraphicsNode(float width, float height, Node* node) : width_(width), height_(height), node_(node) {}

float QtGraphicsNode::width() const  { return width_; }

float QtGraphicsNode::height() const { return height_; }

float &QtGraphicsNode::width() 	     { return width_; }

float &QtGraphicsNode::height() 	 { return height_; }

Node *QtGraphicsNode::getQtNode() 	 { return node_; }
