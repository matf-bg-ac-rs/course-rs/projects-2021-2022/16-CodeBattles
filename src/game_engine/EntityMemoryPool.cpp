#include "EntityMemoryPool.hpp"
#include <algorithm>
#include <stdexcept>
#include "Entity.hpp"

EntityMemoryPool::EntityMemoryPool(size_t max_entities_count)
    : tags_(max_entities_count), active_(max_entities_count) {

    // initialize all of the Component vectors
    for_each_component_vector([size=max_entities_count](auto& comp_vec) {
        comp_vec.resize(size);
    });
}

EntityMemoryPool& EntityMemoryPool::Instance() {
    static EntityMemoryPool mem_pool(MAX_ENTITIES_COUNT);
    return mem_pool;
}

EntityTag EntityMemoryPool::getTag(size_t entity_id) {
    return tags_[entity_id];
}

bool EntityMemoryPool::getIsActive(size_t entity_id) {
    return active_[entity_id];
}

Entity EntityMemoryPool::addEntity(EntityTag tag) {
    size_t index = getNextEntityIndex();

    // reset all components for entity
    for_each_component_vector([index](auto& comp_vec) {
        comp_vec[index].has = false;
    });

    tags_[index] = tag;
    active_[index] = true;
    return Entity{index};
}

void EntityMemoryPool::removeEntity(size_t entity_id) {
    active_[entity_id] = false;
}

size_t EntityMemoryPool::getNextEntityIndex() {
    auto it = std::find(active_.begin(), active_.end(), false);
    if (it == active_.end()) {
        throw std::runtime_error("Memory pool full");
    }
    int index = std::distance(active_.begin(), it);
    return index;
}

