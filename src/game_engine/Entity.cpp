#include "Entity.hpp"

Entity::Entity(size_t id) : id_(id) {}

EntityTag Entity::tag() const {
    return EntityMemoryPool::Instance().getTag(id_);
}

bool Entity::isActive() const {
    return EntityMemoryPool::Instance().getIsActive(id_);
}

void Entity::destroy() {
    EntityMemoryPool::Instance().removeEntity(id_);
}

bool operator==(Entity lhs, Entity rhs) { return lhs.id_ == rhs.id_; }
bool operator!=(Entity lhs, Entity rhs) { return !(lhs == rhs); }
