#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <cstddef>
#include "EntityMemoryPool.hpp"
#include "Component.hpp"

enum class EntityTag { Bot, Bullet };

class Entity {
public:
    template <typename T, typename... TArgs> T& addComponent(TArgs&&... args) {
        auto& component = getComponent<T>();
        component = T{std::forward<TArgs>(args)...};
        component.has = true;
        return component;
    }
    template <typename T> T& getComponent() const {
        return EntityMemoryPool::Instance().getComponent<T>(id_);
    }
    template <typename T> bool hasComponent() const {
        return EntityMemoryPool::Instance().hasComponent<T>(id_);
    }
    template <typename T> void removeComponent() const {
        return EntityMemoryPool::Instance().removeComponent<T>(id_);
    }
    EntityTag tag() const;
    bool isActive() const;

    void destroy();
private:
    size_t id_;

    // Only EntityMemoryPool can create an Entity instance
    explicit Entity(size_t id);
    friend class EntityMemoryPool;

    friend bool operator==(Entity lhs, Entity rhs);
    friend bool operator!=(Entity lhs, Entity rhs);
};

#endif  // ENTITY_HPP
