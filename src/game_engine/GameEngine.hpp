#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <vector>
#include <string>
#include <memory>
#include "Scene.hpp"
#include "Assets.hpp"

class Window;

using SceneMap = std::map<SceneTag, std::unique_ptr<Scene>>;

class GameEngine {
public:
    GameEngine(Window* window_ptr);
    void run();
    void update();
    void quit();
    void changeScene(SceneTag scene_tag);
    Assets& assets();
    Window* window();

    bool isRunning();
    void setIsRunning();
private:
    Scene* currentScene();
    Window* window_ptr_;
    Assets assets_;
    SceneMap scenes_;
    SceneTag current_scene_;
    bool running_;
};

#endif // GAMEENGINE_H
