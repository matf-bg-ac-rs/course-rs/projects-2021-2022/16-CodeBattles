#ifndef ASSETS_HPP
#define ASSETS_HPP

#include <map>
#include <string>
#include "Sound.hpp"

using Program = std::string;

class Assets {
public:
    void addSound(const std::string& name, const std::string& path);
    const Sound& getSound(const std::string& name) const;
    void addProgram(const Program& program);
    const std::vector<Program>& getPrograms() const;
    // temporary for testing
    std::vector<Program>& getPrograms() { return programs_; }
private:
    std::map<std::string, Sound> sounds_;
    std::vector<Program> programs_;
};

#endif // ASSETS_HPP
