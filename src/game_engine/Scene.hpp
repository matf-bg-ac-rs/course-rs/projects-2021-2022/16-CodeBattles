#ifndef SCENE_HPP
#define SCENE_HPP

#include "EntityManager.hpp"
#include <map>
#include <string>
#include "../game_graphics/GraphicsScene.hpp"

class GameEngine;

enum class SceneTag { Main };

class Scene {
public:
    Scene(GameEngine* game_engine_ptr);
    virtual void update() = 0;
    virtual void sRender() = 0;
    virtual ~Scene() = default;

    void addEntityToScene(Entity entity);
protected:
    // Lifetime of the GameEngine is strictly greater than of Scene, therefore this is ok
    GameEngine& game_engine_;
    EntityManager entity_manager_;
    int current_frame_;
    bool has_ended_;
    GraphicsScene* graphics_scene_ptr;
};


#endif // SCENE_HPP
