#include "Scene.hpp"
#include "../game_graphics/GraphicsScene.hpp"

Scene::Scene(GameEngine* game_engine_ptr) : game_engine_(*game_engine_ptr), graphics_scene_ptr(new GraphicsScene()) {}

void Scene::addEntityToScene(Entity entity) { graphics_scene_ptr->addEntity(entity); }
