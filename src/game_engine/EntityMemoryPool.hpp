#ifndef ENTITYMEMORYPOOL_HPP
#define ENTITYMEMORYPOOL_HPP

#include <cstddef>
#include <tuple>
#include <vector>
#include "Component.hpp"

class Entity;
enum class EntityTag;

constexpr size_t MAX_ENTITIES_COUNT = 100;

class EntityMemoryPool {
public:
    static EntityMemoryPool& Instance();
    template <typename T> T& getComponent(size_t entity_id) {
        return std::get<std::vector<T>>(data_)[entity_id];
    }
    template <typename T> bool hasComponent(size_t entity_id) {
        return getComponent<T>(entity_id).has;
    }
    template <typename T> bool removeComponent(size_t entity_id) {
        return getComponent<T>(entity_id).has = false;
    }
    EntityTag getTag(size_t entity_id);
    bool getIsActive(size_t entity_id);
    Entity addEntity(EntityTag tag);
    void removeEntity(size_t entity_id);

private:
    using EntityComponentData = std::tuple<std::vector<CTransform>,
                                           std::vector<CBoundingBox>,
                                           std::vector<CHealth>,
                                           std::vector<CQtGraphicsNode>,
                                           std::vector<CInterpreter>,
                                           std::vector<CLifespan>>;
    size_t entities_count_;
    EntityComponentData data_;
    std::vector<EntityTag> tags_;
    std::vector<bool> active_;
    EntityMemoryPool(size_t max_entities_count);
    size_t getNextEntityIndex();
    template <typename Func>
    void for_each_component_vector(Func f) {
        std::apply([&](auto&&... args) { (f(args),...); }, data_);
    }
};

#endif  // ENTITYMEMORYPOOL_HPP
