#ifndef COMPONENT_HPP
#define COMPONENT_HPP

#include "Vec2.hpp"
#include "QtGraphicsNode.hpp"
#include "../language/interpreter/Interpreter.hpp"

struct Component {
    bool has;
};

struct CTransform : Component {
    Vec2 pos;
    Vec2 velocity;
    float angle;
    CTransform() = default;
    CTransform(const Vec2& _pos, const Vec2& _velocity, float _angle) : pos(_pos), velocity(_velocity), angle(_angle) {}
};

struct CBoundingBox : Component {
    Vec2 size;
    Vec2 half_size;
    CBoundingBox() = default;
    CBoundingBox(const Vec2& s) : size(s), half_size(s / 2) {}
};

struct CHealth : Component {
    int health;
    CHealth() = default;
    CHealth(int _health) : health(_health) {}
};

struct CLifespan : Component {
    int initial;
    int remaining;
    CLifespan() = default;
    CLifespan(int init) : initial(init), remaining(init) {}
};

struct CQtGraphicsNode : Component {
    QtGraphicsNode graphics_node;
    CQtGraphicsNode() = default;
    CQtGraphicsNode(const QtGraphicsNode& _graphics_node) : graphics_node(_graphics_node) {}
};

struct CInterpreter : Component {
    std::unique_ptr<Interpreter> interpreter;
    CInterpreter() = default;
    CInterpreter(const std::string& program) : interpreter(std::make_unique<Interpreter>(program)) {}
};

#endif // COMPONENT_HPP
