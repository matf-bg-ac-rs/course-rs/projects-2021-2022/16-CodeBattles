#ifndef BACKGROUNDMUSIC_HPP
#define BACKGROUNDMUSIC_HPP

#include <QMediaPlayer>
#include <QAudioOutput>
#include <string>

class Sound {
public:
    Sound() = default;
    Sound(const std::string& path);
    void play();
    void stop();
private:
    std::unique_ptr<QMediaPlayer> media_player;
    std::unique_ptr<QAudioOutput> audio_output;
};

#endif // BACKGROUNDMUSIC_HPP
