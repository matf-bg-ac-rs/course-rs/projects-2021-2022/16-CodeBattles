#ifndef VEC2_HPP
#define VEC2_HPP
#include <tuple>
#include <cmath>

class Vec2 {
public:
    Vec2() = default;
    Vec2(float x, float y);
    void operator+=(Vec2 rhs);
    void operator-=(Vec2 rhs);
    void operator*=(Vec2 rhs);
    void operator/=(Vec2 rhs);
    void operator*=(float k);
    void operator/=(float k);

    void normalize();
    float length();
    float squareLength();

    struct PolarCoords {
        float angle;
        float length;
    };

    static float dot(Vec2 lhs, Vec2 rhs);
    static float cross(Vec2 lhs, Vec2 rhs);
    static Vec2 abs(Vec2 v);
    static Vec2 polarToCartesian(PolarCoords polar);
    static PolarCoords cartesianToPolar(Vec2 v);
    static float degToRad(float degrees);
    static float radToDeg(float radians);

    float x() const;
    float y() const;
    float& x();
    float& y();
    std::tuple<int, int> toTuple() const;
private:
    template<typename Map> static Vec2 map(Vec2 v, Map f);

    float x_ = 0;
    float y_ = 0;
};

bool operator==(Vec2 lhs, Vec2 rhs);
bool operator!=(Vec2 lhs, Vec2 rhs);
Vec2 operator+(Vec2 lhs, Vec2 rhs);
Vec2 operator-(Vec2 lhs, Vec2 rhs);
Vec2 operator*(Vec2 lhs, Vec2 rhs);
Vec2 operator/(Vec2 lhs, Vec2 rhs);
Vec2 operator*(Vec2 lhs, float k);
Vec2 operator*(float k, Vec2 lhs);
Vec2 operator/(Vec2 lhs, float k);

#endif // VEC2_HPP
