#include "BattleSceneInterpreterConfigurator.hpp"
#include <memory>
#include "BattleScene.hpp"
#include "GameEngine.hpp"
#include "../Window.hpp"
#include "Physics.hpp"
#include "Vec2.hpp"

BattleSceneInterpreterConfigurator::BattleSceneInterpreterConfigurator(BattleScene *scene_ptr) : scene(scene_ptr){}

void BattleSceneInterpreterConfigurator::configureInterpreter(Entity entity) {
    auto& interpreter = entity.getComponent<CInterpreter>().interpreter;

    auto getX = std::make_shared<BattleSceneInterpreterConfigurator::GetX>([=]() -> float {
        auto src_pos = entity.getComponent<CTransform>().pos;
        auto dest_pos = toVirtualCoordinates(src_pos);
        float x = dest_pos.x();
        return x;
    });
    interpreter->link("getX", getX);

    auto getY = std::make_shared<BattleSceneInterpreterConfigurator::GetY>([=]() -> float {
        auto src_pos = entity.getComponent<CTransform>().pos;
        auto dest_pos = toVirtualCoordinates(src_pos);
        float y = dest_pos.y();
        return y;
    });
    interpreter->link("getY", getY);

    auto speed = std::make_shared<BattleSceneInterpreterConfigurator::Speed>([=]() -> float {
        auto velocity = entity.getComponent<CTransform>().velocity;
        float speed = velocity.length();
        return speed;
    });
    interpreter->link("speed", speed);

    auto health = std::make_shared<BattleSceneInterpreterConfigurator::Health>([=]() -> float {
        int health = entity.getComponent<CHealth>().health;
        return health;
    });
    interpreter->link("health", health);

    auto swim = std::make_shared<BattleSceneInterpreterConfigurator::Swim>([=](float angle, float length) -> float {
        Vec2 velocity = Vec2::polarToCartesian({Vec2::degToRad(angle), length});
        velocity.normalize();
        velocity *= 3;
        auto& transform = entity.getComponent<CTransform>();
        transform.velocity = velocity;

        // Dummy return value for the interpreter
        return -1;
    });
    interpreter->link("swim", swim);

    auto stop = std::make_shared<BattleSceneInterpreterConfigurator::Stop>([=]() -> float {
        auto& transform = entity.getComponent<CTransform>();
        transform.velocity = {0, 0};

        // Dummy return value for the interpreter
        return -1;
    });
    interpreter->link("stop", stop);

    auto cannon = std::make_shared<BattleSceneInterpreterConfigurator::Cannon>([=](float angle) -> float {
        scene->spawnBullet(entity, Vec2::degToRad(angle));

        // Dummy return value for the interpreter
        return -1;
    });
    interpreter->link("cannon", cannon);

    auto scan = std::make_shared<BattleSceneInterpreterConfigurator::Scan>([=](float start_angle, float end_angle) -> float {
        return scene->containsBotInbetween(entity, Vec2::degToRad(start_angle), Vec2::degToRad(end_angle));
    });
    interpreter->link("scan", scan);
}

Vec2 BattleSceneInterpreterConfigurator::toVirtualCoordinates(Vec2 src) {
    auto arena_size = scene->game_engine_.window()->getGraphicsSceneSize();

    const int VIRTUAL_COORDINATE_SIZE = 100;
    auto dest = src * VIRTUAL_COORDINATE_SIZE / arena_size;
    return dest;
}
