#include "Assets.hpp"

void Assets::addSound(const std::string& name, const std::string& path) {
    sounds_[name] = Sound{path};
}

const Sound& Assets::getSound(const std::string& name) const {
    return sounds_.at(name);
}

void Assets::addProgram(const Program& program) {
    programs_.push_back(program);
}

const std::vector<Program> &Assets::getPrograms() const {
    return programs_;
}
