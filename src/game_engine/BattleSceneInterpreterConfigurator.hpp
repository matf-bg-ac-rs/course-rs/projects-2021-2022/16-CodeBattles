#ifndef BATTLESCENEINTERPRETERCONFIGURATOR_H
#define BATTLESCENEINTERPRETERCONFIGURATOR_H

#include "Entity.hpp"
#include "../language/interpreter/Interpreter.hpp"
#include "../language/interpreter/Callable.hpp"

class BattleScene;

// TODO: find better naming than Facade
class BattleSceneInterpreterConfigurator {
public:
    BattleSceneInterpreterConfigurator(BattleScene* scene_ptr);
    void configureInterpreter(Entity entity);
private:

    // TODO: possibly can generate using recursive templates
    using ZeroArgFunc = std::function<float()>;
    using OneArgFunc = std::function<float(float)>;
    using TwoArgFunc = std::function<float(float, float)>;

    struct ZeroArgCallable : Callable {
        ZeroArgFunc func;
        ZeroArgCallable(const ZeroArgFunc& _func) : Callable(), func(_func) {};
        ZeroArgCallable(ZeroArgFunc&& _func) : Callable(), func(std::move(_func)) {};
        void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override {
            assert(args.size() == 0);

            auto ret = func();
            environment->execution()->values().push(ret);
        }
    };

    struct OneArgCallable : Callable {
        OneArgFunc func;
        OneArgCallable(const OneArgFunc& _func) : Callable(), func(_func) {};
        OneArgCallable(OneArgFunc&& _func) : Callable(), func(std::move(_func)) {};
        void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override {
            assert(args.size() >= 1);

            auto ret = func(args[0]);
            environment->execution()->values().push(ret);
        }
    };

    struct TwoArgCallable : Callable {
        TwoArgFunc func;
        TwoArgCallable(const TwoArgFunc& _func) : Callable(), func(_func) {};
        TwoArgCallable(TwoArgFunc&& _func) : Callable(), func(std::move(_func)) {};
        void call(std::shared_ptr<Environment> environment, const std::vector<double>& args) override {
            assert(args.size() == 2);

            auto ret = func(args[0], args[1]);
            environment->execution()->values().push(ret);
        }
    };
public:
    using GetX = ZeroArgCallable;
    using GetY = ZeroArgCallable;
    using Speed = ZeroArgCallable;
    using Health = ZeroArgCallable;
    using Stop = ZeroArgCallable;
    using Cannon = OneArgCallable;
    using Swim = TwoArgCallable;
    using Scan = TwoArgCallable;
private:
    BattleScene* scene;
    Vec2 toVirtualCoordinates(Vec2 src);
};

#endif // BATTLESCENEINTERPRETERCONFIGURATOR_H
