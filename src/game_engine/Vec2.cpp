#include "Vec2.hpp"
#include <cmath>

Vec2::Vec2(float x, float y) : x_(x), y_(y) {}

void Vec2::operator+=(Vec2 rhs) {
    x_ += rhs.x_;
    y_ += rhs.y_;
}

void Vec2::operator-=(Vec2 rhs) {
    x_ -= rhs.x_;
    y_ -= rhs.y_;
}

void Vec2::operator*=(Vec2 rhs) {
    x_ *= rhs.x_;
    y_ *= rhs.y_;
}

void Vec2::operator/=(Vec2 rhs) {
    x_ /= rhs.x_;
    y_ /= rhs.y_;
}

void Vec2::operator*=(float k) {
    x_ *= k;
    y_ *= k;
}

void Vec2::operator/=(float k) {
    *this *= (1 / k);
}

void Vec2::normalize() {
    *this /= length();
}

float Vec2::length() {
    return std::sqrt(squareLength());
}

float Vec2::squareLength() {
    return x_ * x_ + y_ * y_;
}

float Vec2::dot(Vec2 lhs, Vec2 rhs) {
    return lhs.x_ * rhs.x_ + lhs.y_ * rhs.y_;
}

float Vec2::cross(Vec2 lhs, Vec2 rhs) {
    return lhs.x_ * rhs.y_ - lhs.y_ * rhs.x_;
}

template<typename Map>
Vec2 Vec2::map(Vec2 v, Map f) {
    return {f(v.x_), f(v.y_)};
}

Vec2 Vec2::abs(Vec2 v) {
    return map(v, [](float coord) { return std::fabs(coord); });
}

Vec2 Vec2::polarToCartesian(Vec2::PolarCoords polar) {
    float x = polar.length * std::cos(polar.angle);
    float y = polar.length * std::sin(polar.angle);
    return {x, y};
}

Vec2::PolarCoords Vec2::cartesianToPolar(Vec2 v) {
    float angle = std::atan2(v.y(), v.x());
    float length = std::sqrt(v.x() * v.x() + v.y() * v.y());
    return {angle, length};
}

float Vec2::degToRad(float degrees) { return degrees * M_PI / 180.0; }

float Vec2::radToDeg(float radians) { return radians * 180.0 / M_PI; }

float Vec2::x() const {
    return x_;
}

float Vec2::y() const {
    return y_;
}

float& Vec2::x() {
    return x_;
}

float& Vec2::y() {
    return y_;
}

std::tuple<int, int> Vec2::toTuple() const {
    return {x_, y_};
}

bool operator==(Vec2 lhs, Vec2 rhs) {
    return lhs.x() == rhs.x() && lhs.y() == rhs.y();
}

bool operator!=(Vec2 lhs, Vec2 rhs) {
    return !(lhs == rhs);
}

Vec2 operator+(Vec2 lhs, Vec2 rhs) {
    lhs += rhs;
    return lhs;
}

Vec2 operator-(Vec2 lhs, Vec2 rhs) {
    lhs -= rhs;
    return lhs;
}

Vec2 operator*(Vec2 lhs, Vec2 rhs) {
    lhs *= rhs;
    return lhs;
}

Vec2 operator/(Vec2 lhs, Vec2 rhs) {
    lhs /= rhs;
    return lhs;
}

Vec2 operator*(Vec2 lhs, float k) {
    lhs *= k;
    return lhs;
}

Vec2 operator*(float k, Vec2 lhs) {
    return lhs * k;
}

Vec2 operator/(Vec2 lhs, float k) {
    lhs /= k;
    return lhs;
}
