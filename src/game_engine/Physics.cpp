#include "Physics.hpp"
#include "Component.hpp"
#include <cmath>

std::optional<Vec2> Physics::detectCollision(Entity a, Entity b) {
    if (!a.hasComponent<CTransform>() || !b.hasComponent<CTransform>()) {
        throw std::runtime_error("Entity is missing positional information");
    }
    if (!a.hasComponent<CBoundingBox>() || !b.hasComponent<CBoundingBox>()) {
        throw std::runtime_error("Entity is missing the bounding box");
    }
    auto& a_pos = a.getComponent<CTransform>().pos;
    auto& a_half_size = a.getComponent<CBoundingBox>().half_size;
    auto& b_pos = b.getComponent<CTransform>().pos;
    auto& b_half_size = b.getComponent<CBoundingBox>().half_size;

    auto delta = Vec2::abs(a_pos - b_pos);

    auto overlap = a_half_size + b_half_size - delta;
    if (overlap.x() <= 0 || overlap.y() <= 0) {
        return std::nullopt;
    }
    return overlap;
}

void Physics::resolveNonPassableCollision(Entity moving, Entity stationary) {
    auto overlap_opt = Physics::detectCollision(moving, stationary);
    if (!overlap_opt) {
        return;
    }
    moving.getComponent<CTransform>().pos += *overlap_opt;
}

bool Physics::isCollision(Entity a, Entity b) {
    return detectCollision(a, b).has_value();
}
