#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#include "Entity.hpp"
#include "Vec2.hpp"
#include <optional>

namespace Physics {

std::optional<Vec2> detectCollision(Entity a, Entity b);

// TODO: think of a better name
void resolveNonPassableCollision(Entity moving, Entity stationary);

bool isCollision(Entity a, Entity b);

}

#endif // PHYSICS_HPP
