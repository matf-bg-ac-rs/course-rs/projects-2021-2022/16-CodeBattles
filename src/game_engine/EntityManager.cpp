#include "EntityManager.hpp"
#include "EntityMemoryPool.hpp"
#include <algorithm>
#include <iostream>

EntityManager::EntityManager() {}

void EntityManager::update() {
    for (Entity entity : entities_to_add_) {
        entities_.push_back(entity);
        entity_map[entity.tag()].push_back(entity);
    }
    entities_to_add_.clear();

    removeInactiveEntities(&entities_);
    for (auto& [tag, map_entities] : entity_map) {
        removeInactiveEntities(&map_entities);
    }
}

Entity EntityManager::addEntity(EntityTag tag) {
    Entity entity = EntityMemoryPool::Instance().addEntity(tag);
    entities_to_add_.push_back(entity);
    return entity;
}

EntityVec& EntityManager::getEntities() {
    return entities_;
}

EntityVec& EntityManager::getEntities(EntityTag tag) {
    return entity_map[tag];
}

void EntityManager::removeInactiveEntities(EntityVec *entities_ptr) {
    auto& entities = *entities_ptr;
    auto isInactive = [](Entity entity) { return !entity.isActive(); };
    auto it = std::remove_if(entities.begin(), entities.end(), isInactive);
    if (it != entities.end()) {
        entities.erase(it);
    }
}
