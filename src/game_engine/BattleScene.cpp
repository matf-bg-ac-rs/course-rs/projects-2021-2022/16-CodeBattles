#include "BattleScene.hpp"
#include "Component.hpp"
#include "GameEngine.hpp"
#include "QKeyEvent"
#include "Physics.hpp"
#include <iostream>
#include "Entity.hpp"
#include "../game_graphics/AlienShipNode.hpp"
#include "../game_graphics/RocketNode.hpp"
#include "../game_graphics/ProjectilePixmapNode.hpp"
#include <array>
#include "Vec2.hpp"
#include "../Window.hpp"
#include "./BattleSceneInterpreterConfigurator.hpp"

BattleScene::BattleScene(GameEngine* game_engine_ptr) : Scene(game_engine_ptr), configurator(std::make_unique<BattleSceneInterpreterConfigurator>(this)) {
    game_engine_.window()->setupGraphicsScene(graphics_scene_ptr);

    spawnBots();

    game_engine_ptr->setIsRunning();
}

void BattleScene::update() {
    entity_manager_.update();

    sInterpreters();

    sMovement();
    sLifespan();
    sCollision();

    sRender();
}

Vec2 BattleScene::calculateBotStartingPosition(int idx) const {
    auto [view_w, view_h] = game_engine_.window()->getGraphicsSceneSize().toTuple();
    switch (idx) {
        case 0: return Vec2(view_w * 0.15, view_h * 0.15);
        case 1: return Vec2(view_w * 0.15, view_h * 0.85);
        case 2: return Vec2(view_w * 0.85, view_h * 0.15);
        case 3: return Vec2(view_w * 0.85, view_h * 0.85);
    }

    throw std::out_of_range("Bot index out of range");
}

bool BattleScene::containsBotInbetween(Entity source, float start_angle, float end_angle) {
    Vec2 source_pos = source.getComponent<CTransform>().pos;

    auto& entities = entity_manager_.getEntities(EntityTag::Bot);

    for (Entity entity : entities) {
        if (entity != source) {
            Vec2 dest_pos = entity.getComponent<CTransform>().pos;
            Vec2 entity_pos = dest_pos - source_pos;
            float entity_angle = Vec2::cartesianToPolar(entity_pos).angle;
            if (start_angle <= entity_angle && entity_angle <= end_angle) {
                return true;
            }
        }
    }
    return false;
}

void BattleScene::spawnBots() {
    // initialize interpreters
    auto& programs = game_engine_.assets().getPrograms();
    // Spawn bots
    for (size_t i = 0; i < programs.size(); i++) {
        Entity bot = entity_manager_.addEntity(EntityTag::Bot);

        // Add position, velocity and angle
        auto start_pos = calculateBotStartingPosition(i);
        bot.addComponent<CTransform>(start_pos, Vec2{0, 0}, 0.0f);

        // Add health
        bot.addComponent<CHealth>(100);

        // Add Qt graphics support
        auto graphics_node = [i, bot]() -> QtGraphicsNode {
            if (i % 2 == 0) {
                return {60, 60, new AlienShipNode(bot)};
            } else {
                return {50, 70, new RocketNode(bot)};
            }
        }();
        bot.addComponent<CQtGraphicsNode>(graphics_node);
        addEntityToScene(bot);

        // Add bounding box
        bot.addComponent<CBoundingBox>(graphics_node.size());

        // Add program interpreter
        auto program = programs[i];
        bot.addComponent<CInterpreter>(program);
        configurator->configureInterpreter(bot);
    }
}

void BattleScene::spawnBullet(Entity bot, float angle) {
    auto& bot_transform = bot.getComponent<CTransform>();
    Entity bullet = entity_manager_.addEntity(EntityTag::Bullet);

    // Add position, velocity and angle
    Vec2 pos = bot_transform.pos;
    Vec2 velocity = Vec2::polarToCartesian({angle, 1});
    velocity.normalize();
    velocity *= 3;
    bullet.addComponent<CTransform>(pos, velocity, 0.0f);

    // Add Qt graphics support
    auto graphics_node = QtGraphicsNode{20, 20, new ProjectilePixmapNode(bullet)};
    bullet.addComponent<CQtGraphicsNode>(graphics_node);

    bullet.addComponent<CBoundingBox>(graphics_node.size());
    addEntityToScene(bullet);
}

void BattleScene::sRender() {
    for (Entity entity : entity_manager_.getEntities()) {
        if (entity.hasComponent<CQtGraphicsNode>()) {
            auto& graphics_node = entity.getComponent<CQtGraphicsNode>().graphics_node;
            graphics_node.getQtNode()->refreshDisplay();
        }
    }
}

void BattleScene::sInterpreters() {
    for (auto entity : entity_manager_.getEntities(EntityTag::Bot)) {
        auto& interpreter = entity.getComponent<CInterpreter>().interpreter;
        interpreter->step();
    }
}

void BattleScene::sMovement() {
    for (Entity entity : entity_manager_.getEntities()) {
        if (entity.hasComponent<CTransform>()) {
            // Update positions
            auto& transform = entity.getComponent<CTransform>();
            transform.pos += transform.velocity;

            // Update angle
            transform.angle = [velocity=transform.velocity]() {
                float angle = Vec2::radToDeg(Vec2::cartesianToPolar(velocity).angle);
                return angle + 90;
            }();
        }
    }
}

void BattleScene::sCollision() {
    auto& bots = entity_manager_.getEntities(EntityTag::Bot);
    auto& bullets = entity_manager_.getEntities(EntityTag::Bullet);
    for (Entity bot : bots) {
        for (Entity bullet : bullets) {
            auto& bot_health = bot.getComponent<CHealth>();
            if (Physics::detectCollision(bot, bullet)) {
                bot_health.health -= BULLET_HEALTH_DAMAGE;
//                bullet.destroy();
            }
            if (bot_health.health <= 0) {
                // TODO: change to removing CTransform and animating
                bot.destroy();
            }
        }
    }
    // don't allow leaving the screen
    auto scene_size = game_engine_.window()->getGraphicsSceneSize();
    for (Entity entity : entity_manager_.getEntities()) {
        auto& transform = entity.getComponent<CTransform>();
        Vec2 next_velocity = transform.velocity;
        if (transform.pos.x() <= 0 || transform.pos.x() >= scene_size.x()) {
            next_velocity *= Vec2{-1, 1};
        }
        if (transform.pos.y() <= 0 || transform.pos.y() >= scene_size.y()) {
            next_velocity *= Vec2{1, -1};
        }
        transform.velocity = next_velocity;
    }
}

void BattleScene::sLifespan() {
    for (Entity entity : entity_manager_.getEntities()) {
        if (entity.hasComponent<CLifespan>()) {
            auto& lifespan = entity.getComponent<CLifespan>();
            if (lifespan.remaining > 0) {
                --lifespan.remaining;
            }
            if (lifespan.remaining == 0) {
                entity.destroy();
            }
        }
    }

}
