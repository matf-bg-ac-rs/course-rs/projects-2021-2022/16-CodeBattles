#ifndef QTGRAPHICSNODE_H
#define QTGRAPHICSNODE_H

#include <cstddef>
#include <string>
#include "Vec2.hpp"
#include "../game_graphics/Node.hpp"

class QtGraphicsNode {
public:
    QtGraphicsNode() = default;
    QtGraphicsNode(float width, float height, Node* node);
    float width() const;
    float height() const;
    float& width();
    float& height();
    Vec2 size() { return Vec2{width_, height_}; }
    Node* getQtNode();
private:
    float width_;
    float height_;
    Node* node_;
};

#endif // QTGRAPHICSNODE_H
