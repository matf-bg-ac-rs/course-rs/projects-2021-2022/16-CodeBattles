#ifndef ENTITYMANAGER_HPP
#define ENTITYMANAGER_HPP

#include <map>
#include <vector>
#include "Entity.hpp"

using EntityVec = std::vector<Entity>;
using EntityMap = std::map<EntityTag, EntityVec>;

class EntityManager {
public:
    EntityManager();
    void update();
    Entity addEntity(EntityTag tag);
    EntityVec& getEntities();
    EntityVec& getEntities(EntityTag tag);

private:
    EntityVec entities_;
    EntityMap entity_map;

    EntityVec entities_to_add_;

    void removeInactiveEntities(EntityVec* entities_ptr);
};

#endif  // ENTITYMANAGER_HPP
