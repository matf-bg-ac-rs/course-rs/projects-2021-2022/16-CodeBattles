#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <QMainWindow>
#include "ui_Window.h"

#include "visual_programming/VisualProgramming.hpp"
#include "game_engine/GameEngine.hpp"
#include "game_engine/Vec2.hpp"


QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class Window : public QMainWindow {
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);
    ~Window();

    Vec2 getGraphicsSceneSize() const;
    void setupGraphicsScene(GraphicsScene* grpahics_scene_ptr);
private Q_SLOTS:
    void disablePlayButton();
    void renderGame();
    void run(int index);

    void saveTextProgram();
    void saveAsTextProgram();
    void loadTextProgram();

private:
    void setupVisualProgramming();

    void save(const QString& path);
    void load(const QString& path);

    static constexpr int FPS = 30;

    Ui::Window *ui;
    VisualProgramming *vp;
    GameEngine game_engine;

    int tab;
    QString openBufferPath;

    Vec2 graphics_scene_size_{500, 500};
};
#endif // WINDOW_HPP
