#ifndef VISUALCOMPONENTITEMBLOCK_HPP
#define VISUALCOMPONENTITEMBLOCK_HPP

#include <QObject>
#include "VisualComponentItem.hpp"

class VisualComponentItemBlock : public VisualComponentItem
{
public:
    VisualComponentItemBlock(bool enabled = false);

    void init() override;

    QString getModelCode() override;

    void paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    virtual void setNextComponent(VisualComponentItem *nextComponent, DropArea *nextArea) override;
    virtual void removeNextComponent(DropArea *connectionArea) override;
    virtual void adjustNextComponentPosition() override;

    qreal getBlockHeight() const;
    qreal getHeight() const override;
    DropArea *daBlock() const;

protected:
    DropArea *_daBlock;

private:
    VisualComponentItem *_nextComponentBlock;
};

#endif // VISUALCOMPONENTITEMBLOCK_HPP
