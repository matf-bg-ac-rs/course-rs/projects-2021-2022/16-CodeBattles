﻿#include "VisualComponentItem.hpp"

#include <QPainter>
#include <QDebug>
#include <QCursor>
#include <QGraphicsScene>

#include <QGraphicsWidget>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLinearLayout>
#include <QLineEdit>
#include <QLabel>

VisualComponentItem::VisualComponentItem(bool enabled)
    : _enabled(enabled)
    , _vc(nullptr)
    , _da(nullptr)
    , _ca(nullptr)
    , _gw(nullptr)
    , _prevComponent(nullptr)
    , _nextComponent(nullptr)
    , _connectionTarget(nullptr)
    , _connectionToParent(nullptr)
{
    setAcceptedMouseButtons(Qt::LeftButton);

    _bg = QColor::fromRgb(160,70,70);
    _bgTargeted = QColor::fromRgb(200,100,100);
}

VisualComponentItem::~VisualComponentItem()
{
    delete _vc;
}

void VisualComponentItem::init()
{
    _da = new DropArea(this);
    scene()->addItem(_da);
    _da->setPos(0,getHeight() - _da->boundingRect().height());
    _da->setParentItem(this);

    _ca = new ConnectionArea(this);
    scene()->addItem(_ca);
    _ca->setPos(0,0);
    _ca->setParentItem(this);

    setCursor(Qt::OpenHandCursor);
    setFlags(GraphicsItemFlag::ItemIsMovable);


}

QString VisualComponentItem::getModelCode()
{
    QString res = "";
    if(_vc) {
        res += _vc->generateCode() + "\n";
    }
    if(_nextComponent) {
        res += _nextComponent->getModelCode();
    }
    return res;
}

QRectF VisualComponentItem::boundingRect() const {
    return QRectF(0,0,getWidth(),getHeight());
}

void VisualComponentItem::paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget)

//    painter->setPen(QPen(Qt::blue,1));
//    painter->drawRect(boundingRect());

    painter->setPen(QPen(QColor::fromRgb(40,40,40),1));
    painter->setBrush(QBrush(_bg));

    if(_targeted) {
        painter->setBrush(QBrush(_bgTargeted));
    }

    QPointF points[10] = {
        QPointF(0, 0),

        QPointF(90,0),
        QPointF(100,5),
        QPointF(110,0),

        QPointF(getWidth(), 0),
        QPointF(getWidth(), getHeight()-5),

        QPointF(110, getHeight()-5),
        QPointF(100, getHeight()),
        QPointF(90, getHeight()-5),

        QPointF(0, getHeight()-5)
    };
    painter->drawConvexPolygon(points, 10);

    if(!_enabled) {
        painter->drawText(QRectF(10,0,getWidth()-20,VisualComponentItem::getHeight()), Qt::AlignVCenter, _vc->generateCode());
    }

}

void VisualComponentItem::setPrevComponent(DropArea *prevArea) {
    setParentItem(prevArea->_parentVCI);
    _prevComponent = prevArea->_parentVCI;
    _connectionToParent = prevArea;
}

void VisualComponentItem::removePrevComponent()
{
    _prevComponent = nullptr;
    _connectionToParent = nullptr;
    setParentItem(0);
}

void VisualComponentItem::setNextComponent(VisualComponentItem *nextComponent, DropArea *nextArea) {
    if(nextComponent == nullptr || _nextComponent == nextComponent) {
        return;
    }
    nextComponent->setNextComponent(_nextComponent, nextComponent->da());
    nextComponent->setPrevComponent(nextArea);
    _nextComponent = nextComponent;
    adjustNextComponentPosition();
    prepareAllRelevantGeometry();
}

void VisualComponentItem::removeNextComponent(DropArea *connectionArea) {
    Q_UNUSED(connectionArea)
    _nextComponent->removePrevComponent();
    _nextComponent = nullptr;
    prepareAllRelevantGeometry();
}

void VisualComponentItem::prepareAllRelevantGeometry()
{
    prepareGeometryChange();
    adjustNextComponentPosition();
    if(_prevComponent) {
        _prevComponent->prepareAllRelevantGeometry();
    }
}

void VisualComponentItem::adjustNextComponentPosition() {
    if(_da) {
        _da->setPos(0,getHeight() - _da->boundingRect().height());
    }
    if(_nextComponent != nullptr) {
        _nextComponent->setPos(QPointF(0,boundingRect().height() - 5));
    }
}

void VisualComponentItem::findAndSetConnectionTarget() {
    if(_connectionTarget) {
        _connectionTarget->untargetThisArea();
        _connectionTarget = nullptr;
    }

    auto ci = _ca->collidingItems();
    DropArea * foundArea = nullptr;
    for(auto i : ci)
    {
        DropArea * area = dynamic_cast<DropArea*>(i);
        if (area)
        {
            if(area->_parentVCI != this && area->_parentVCI != _nextComponent)
            {
                foundArea = area;
            }
        }
    }
    if(foundArea) {
        _connectionTarget = foundArea;
        _connectionTarget->targetThisArea();
    }
}

bool VisualComponentItem::targeted() const
{
    return _targeted;
}

VisualComponentItem *VisualComponentItem::nextComponent() const
{
    return _nextComponent;
}

DropArea *VisualComponentItem::da() const
{
    return _da;
}

void VisualComponentItem::setTargeted(bool newTargeted, DropArea *targetArea)
{
    Q_UNUSED(targetArea)
    _targeted = newTargeted;
    setZValue(0);
    prepareGeometryChange();
}

qreal VisualComponentItem::getHeight() const
{
    if(_gw) {
        return _gw->boundingRect().height() + 5;
    } else {
        return 50;
    }
}

qreal VisualComponentItem::getWidth() const
{
    qreal res = 250;
    if(_gw) {
        res = _gw->boundingRect().width() + 5;
    }
    return res < 250 ? 250 : res;
}
void VisualComponentItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(_enabled) {
        if(_connectionToParent) {
            _connectionToParent->disconnectComponent();
            setPos(event->scenePos()-event->pos());
        }
        findAndSetConnectionTarget();

        setCursor(Qt::ClosedHandCursor);
        setZValue(1);
        QGraphicsItem::mouseMoveEvent(event);
    }
}

void VisualComponentItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(_enabled) {
        setCursor(Qt::ClosedHandCursor);
        QGraphicsItem::mousePressEvent(event);
    } else {
        VisualComponentPicker* vcp = dynamic_cast<VisualComponentPicker*>(scene());
        if(vcp) {
            vcp->pick(this);
        }
    }
}

void VisualComponentItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(_enabled) {
        if(_connectionTarget) {
            _connectionTarget->untargetThisArea();
            _connectionTarget->connectComponent(this);
            _connectionTarget = nullptr;
        }

        setCursor(Qt::OpenHandCursor);
        QGraphicsItem::mouseReleaseEvent(event);
    }
}

DropArea::DropArea(VisualComponentItem *parentVCI)
    : _parentVCI(parentVCI)
{

}

DropArea::~DropArea()
{

}

QRectF DropArea::boundingRect() const {
    return QRectF(0,0,_parentVCI->getWidth(),20);
}

void DropArea::paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget)
    Q_UNUSED(painter)
//    painter->setPen(Qt::red);
//    painter->drawRect(boundingRect());
}

void DropArea::targetThisArea() {
    _parentVCI->setTargeted(true, this);
}

void DropArea::untargetThisArea() {
    _parentVCI->setTargeted(false, this);
}

void DropArea::connectComponent(VisualComponentItem *component) {
    _parentVCI->setNextComponent(component,this);
}

void DropArea::disconnectComponent() {
    _parentVCI->removeNextComponent(this);
}

ConnectionArea::ConnectionArea(VisualComponentItem *parentVCI)
    : _parentVCI(parentVCI)
{

}

ConnectionArea::~ConnectionArea()
{

}

QRectF ConnectionArea::boundingRect() const {
    return QRectF(0,0,_parentVCI->getWidth(),20);
}

void ConnectionArea::paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget)
    Q_UNUSED(painter)
//    painter->setPen(Qt::green);
//    painter->drawRect(boundingRect());
}

