#ifndef VISUALCOMPONENTPICKER_HPP
#define VISUALCOMPONENTPICKER_HPP

#include <QGraphicsScene>
#include <QObject>
#include "../VisualProgramming.hpp"

class VisualComponent;
class VisualComponentItem;
class VisualGraphScene;

class VisualComponentPicker : public QGraphicsScene
{
public:
    explicit VisualComponentPicker(const QRectF &sceneRect, QObject *parent = nullptr);
    ~VisualComponentPicker();

    void init(QList<VisualComponent*> &components, VisualGraphScene *vgs);

    VisualGraphScene *_vgs;
    void pick(VisualComponentItem* component);
};

#endif // VISUALCOMPONENTPICKER_HPP
