#ifndef VISUALGRAPHSCENE_HPP
#define VISUALGRAPHSCENE_HPP

#include <QGraphicsScene>
#include <QObject>

#include "../VisualProgramming.hpp"
class VisualComponentItem;

class VisualGraphScene : public QGraphicsScene
{
public:
    explicit VisualGraphScene(const QRectF &sceneRect, QObject *parent = nullptr);
    ~VisualGraphScene();

    void addComponent(VisualComponentItem *vci, qreal x = 0, qreal y = 0);
    VisualComponentItem *start() const;

private:
    VisualGraph *vg;

    VisualComponentItem *_start;
};

#endif // VISUALGRAPHSCENE_HPP
