#include "VisualComponentPicker.hpp"

VisualComponentPicker::VisualComponentPicker(const QRectF &sceneRect, QObject *parent)
    : QGraphicsScene{sceneRect, parent}
{

}

VisualComponentPicker::~VisualComponentPicker()
{

}

void VisualComponentPicker::init(QList<VisualComponent *> &components, VisualGraphScene *vgs)
{
    qreal pos = 50;
    for(auto c : components) {
        VisualComponentItem *vci = c->getViewItem();
        this->addItem(vci);
        vci->setPos(0,pos);
        pos+= vci->getHeight() + 20;
    }
    _vgs = vgs;
    setSceneRect(QRectF(0,0,250,pos+20));
}

void VisualComponentPicker::pick(VisualComponentItem *component)
{
    VisualComponentItem *vci = component->_vc->getViewItem();
    vci->_enabled = true;
    _vgs->addComponent(vci,400,0);
}
