#ifndef VISUALCOMPONENTITEM_HPP
#define VISUALCOMPONENTITEM_HPP

#include <QGraphicsItem>
#include <QObject>
#include "../VisualProgramming.hpp"
class VisualComponent;

class DropArea;
class ConnectionArea;

class VisualComponentItem : public QGraphicsItem
{
public:
    VisualComponentItem(bool enabled = false);
    virtual ~VisualComponentItem();

    virtual void init();

    virtual QString getModelCode();

    QRectF boundingRect() const override;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void setPrevComponent(DropArea *prevArea);
    void removePrevComponent();
    virtual void setNextComponent(VisualComponentItem *nextComponent, DropArea *nextArea);
    virtual void removeNextComponent(DropArea *connectionArea);
    void prepareAllRelevantGeometry();
    virtual void adjustNextComponentPosition();

    virtual void setTargeted(bool newTargeted, DropArea *targetArea);
    bool targeted() const;

    bool _enabled;

    VisualComponent *_vc;

    virtual qreal getHeight() const;
    virtual qreal getWidth() const;

    DropArea *da() const;

    VisualComponentItem *nextComponent() const;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

    void findAndSetConnectionTarget();

    bool _targeted = false;
    DropArea *_da;
    ConnectionArea *_ca;

    QGraphicsWidget *_gw;

    QColor _bg;
    QColor _bgTargeted;

private:

    VisualComponentItem *_prevComponent;
    VisualComponentItem *_nextComponent;

    DropArea *_connectionTarget;
    DropArea *_connectionToParent;
};

class DropArea : public QGraphicsItem
{
public:
    DropArea(VisualComponentItem *parentVCI);
    ~DropArea();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void targetThisArea();
    void untargetThisArea();

    void connectComponent(VisualComponentItem *component);
    void disconnectComponent();

    VisualComponentItem *_parentVCI;
};

class ConnectionArea : public QGraphicsItem
{
public:
    ConnectionArea(VisualComponentItem *parentVCI);
    ~ConnectionArea();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    VisualComponentItem *_parentVCI;
};

#endif // VISUALCOMPONENTITEM_HPP
