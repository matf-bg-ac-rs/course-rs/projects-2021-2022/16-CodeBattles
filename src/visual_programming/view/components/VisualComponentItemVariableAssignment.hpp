#ifndef VISUALCOMPONENTITEMVARIABLEASSIGNMENT_HPP
#define VISUALCOMPONENTITEMVARIABLEASSIGNMENT_HPP

#include <QObject>
#include "../VisualComponentItem.hpp"

class VisualComponentItemVariableAssignment : public VisualComponentItem
{
public:
    VisualComponentItemVariableAssignment();

    QString getModelCode() override;

    void init() override;
};

#endif // VISUALCOMPONENTITEMVARIABLEASSIGNMENT_HPP
