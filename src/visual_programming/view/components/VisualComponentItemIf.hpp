#ifndef VISUALCOMPONENTITEMIF_HPP
#define VISUALCOMPONENTITEMIF_HPP

#include <QObject>
#include "../VisualComponentItemBlock.hpp"

class VisualComponentItemIf : public VisualComponentItemBlock
{
public:
    VisualComponentItemIf();

    QString getModelCode() override;

    void init() override;
};

#endif // VISUALCOMPONENTITEMIF_HPP
