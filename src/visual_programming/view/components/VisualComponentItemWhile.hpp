#ifndef VISUALCOMPONENTITEMWHILE_HPP
#define VISUALCOMPONENTITEMWHILE_HPP

#include <QObject>
#include "../VisualComponentItemBlock.hpp"

class VisualComponentItemWhile : public VisualComponentItemBlock
{
public:
    VisualComponentItemWhile();

    QString getModelCode() override;

    void init() override;
};

#endif // VISUALCOMPONENTITEMWHILE_HPP
