#include "VisualComponentItemFunctionCall.hpp"
#include "../../model/components/VisualComponentFunctionCall.hpp"

#include <QGraphicsWidget>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLinearLayout>
#include <QLineEdit>
#include <QLabel>
#include <QList>

VisualComponentItemFunctionCall::VisualComponentItemFunctionCall()
    : VisualComponentItem()
{

}

QString VisualComponentItemFunctionCall::getModelCode()
{
    VisualComponentFunctionCall *vc = dynamic_cast<VisualComponentFunctionCall*>(_vc);
    int argIndex = 0;
    for(int i = 0; i < _gw->layout()->count(); i++)
    {
        auto item = dynamic_cast<QLineEdit*>(dynamic_cast<QGraphicsProxyWidget*>(_gw->layout()->itemAt(i))->widget());
        if(item) {
            vc->_functionArguments.replace(argIndex, item->text());
            argIndex++;
        }
    }
    return VisualComponentItem::getModelCode();
}

void VisualComponentItemFunctionCall::init()
{
    VisualComponentItem::init();

    VisualComponentFunctionCall *vc = dynamic_cast<VisualComponentFunctionCall*>(_vc);

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout();

    QWidget *text = new QLabel(vc->_functionName + "(");
    text->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");
    layout->addItem(scene()->addWidget(text));

    bool prvi = true;
    for(auto &arg : vc->_functionArguments) {
        if(!prvi) {
            QWidget *comma = new QLabel(",");
            comma->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");

            layout->addItem(scene()->addWidget(comma));
        } else {
            prvi = false;
        }
        QLineEdit *edit = new QLineEdit("");
        edit->setPlaceholderText(arg);
        edit->setStyleSheet("background-color: rgba(255,255,255,50); font-family: mono; height: 20px;");

        layout->addItem(scene()->addWidget(edit));
    }

    QWidget *textAfter = new QLabel(");");
    textAfter->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");
    layout->addItem(scene()->addWidget(textAfter));

    _gw = new QGraphicsWidget();
    _gw->setLayout(layout);
    scene()->addItem(_gw);
    _gw->setParentItem(this);
    _gw->setPos(0,0);
}
