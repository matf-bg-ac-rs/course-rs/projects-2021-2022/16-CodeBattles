#include "VisualComponentItemVariableAssignment.hpp"
#include "../../model/components/VisualComponentVariableAssignment.hpp"

#include <QGraphicsWidget>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLinearLayout>
#include <QLineEdit>
#include <QLabel>
#include <QList>

VisualComponentItemVariableAssignment::VisualComponentItemVariableAssignment()
    : VisualComponentItem()
{
    _bg = QColor::fromRgb(70,160,70);
    _bgTargeted = QColor::fromRgb(100,200,100);
}

QString VisualComponentItemVariableAssignment::getModelCode()
{
    VisualComponentVariableAssignment *vc = dynamic_cast<VisualComponentVariableAssignment*>(_vc);
    auto item = dynamic_cast<QLineEdit*>(dynamic_cast<QGraphicsProxyWidget*>(_gw->layout()->itemAt(0))->widget());
    if(item) {
        vc->_variableName = item->text();
    }
    item = dynamic_cast<QLineEdit*>(dynamic_cast<QGraphicsProxyWidget*>(_gw->layout()->itemAt(2))->widget());
    if(item) {
        vc->_variableValue = item->text();
    }
    return VisualComponentItem::getModelCode();
}

void VisualComponentItemVariableAssignment::init()
{
    VisualComponentItem::init();

    VisualComponentVariableAssignment *vc = dynamic_cast<VisualComponentVariableAssignment*>(_vc);

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout();

    QLineEdit *editName = new QLineEdit("");
    editName->setPlaceholderText(vc->_variableName);
    editName->setStyleSheet("background-color: rgba(255,255,255,50); font-family: mono; height: 20px;");
    layout->addItem(scene()->addWidget(editName));

    QWidget *text = new QLabel(" = ");
    text->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");
    layout->addItem(scene()->addWidget(text));

    QLineEdit *editValue = new QLineEdit("");
    editValue->setPlaceholderText(vc->_variableValue);
    editValue->setStyleSheet("background-color: rgba(255,255,255,50); font-family: mono; height: 20px;");
    layout->addItem(scene()->addWidget(editValue));

    QWidget *textAfter = new QLabel(";");
    textAfter->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");
    layout->addItem(scene()->addWidget(textAfter));

    _gw = new QGraphicsWidget();
    _gw->setLayout(layout);
    scene()->addItem(_gw);
    _gw->setParentItem(this);
    _gw->setPos(0,0);
}
