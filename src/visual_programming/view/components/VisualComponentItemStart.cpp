#include "VisualComponentItemStart.hpp"

VisualComponentItemStart::VisualComponentItemStart()
    : VisualComponentItem(true)
{
    _bg = QColor::fromRgb(160,160,160);
    _bgTargeted = QColor::fromRgb(200,200,200);
}

void VisualComponentItemStart::paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget)

//    painter->setPen(QPen(Qt::blue,1));
//    painter->drawRect(boundingRect());

    painter->setPen(QPen(QColor::fromRgb(40,40,40),3));
    painter->setBrush(QBrush(_bg));

    if(_targeted) {
        painter->setBrush(QBrush(_bgTargeted));
    }

    QPointF points[7] = {
        QPointF(0, 0),

        QPointF(getWidth(), 0),
        QPointF(getWidth(), getHeight()-5),

        QPointF(110, getHeight()-5),
        QPointF(100, getHeight()),
        QPointF(90, getHeight()-5),

        QPointF(0, getHeight()-5)
    };
    painter->drawConvexPolygon(points, 7);

    painter->drawText(QRectF(10,0,getWidth()-20,VisualComponentItem::getHeight()), Qt::AlignVCenter, "START");

}

void VisualComponentItemStart::init()
{
    VisualComponentItem::init();

    delete _ca;

    setAcceptedMouseButtons(Qt::NoButton);
    setCursor(Qt::ArrowCursor);
    setFlags(GraphicsItemFlag());
}
