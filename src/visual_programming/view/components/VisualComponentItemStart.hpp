#ifndef VISUALCOMPONENTITEMSTART_HPP
#define VISUALCOMPONENTITEMSTART_HPP

#include <QObject>
#include "../VisualComponentItem.hpp"

class VisualComponentItemStart : public VisualComponentItem
{
public:
    VisualComponentItemStart();
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void init() override;
};

#endif // VISUALCOMPONENTITEMSTART_HPP
