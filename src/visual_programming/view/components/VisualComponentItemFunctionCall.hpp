#ifndef VISUALCOMPONENTITEMFUNCTIONCALL_HPP
#define VISUALCOMPONENTITEMFUNCTIONCALL_HPP

#include <QObject>
#include "../VisualComponentItem.hpp"

class VisualComponentItemFunctionCall : public VisualComponentItem
{
public:
    VisualComponentItemFunctionCall();

    QString getModelCode() override;

    void init() override;
};

#endif // VISUALCOMPONENTITEMFUNCTIONCALL_HPP
