#include "VisualComponentItemIf.hpp"
#include "../../model/components/VisualComponentIf.hpp"

#include <QGraphicsWidget>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLinearLayout>
#include <QLineEdit>
#include <QLabel>
#include <QList>

VisualComponentItemIf::VisualComponentItemIf()
    : VisualComponentItemBlock()
{

}

QString VisualComponentItemIf::getModelCode()
{
    VisualComponentIf *vc = dynamic_cast<VisualComponentIf*>(_vc);
    auto item = dynamic_cast<QLineEdit*>(dynamic_cast<QGraphicsProxyWidget*>(_gw->layout()->itemAt(1))->widget());
    if(item) {
        vc->_condition = item->text();
    }
    return VisualComponentItemBlock::getModelCode();
}

void VisualComponentItemIf::init()
{
    VisualComponentItemBlock::init();

    VisualComponentIf *vc = dynamic_cast<VisualComponentIf*>(_vc);
    Q_UNUSED(vc);

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout();

    QWidget *text = new QLabel("if(");
    text->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");
    layout->addItem(scene()->addWidget(text));

    QLineEdit *edit = new QLineEdit("");
    edit->setPlaceholderText("condition");
    edit->setStyleSheet("background-color: rgba(255,255,255,50); font-family: mono; height: 20px;");
    layout->addItem(scene()->addWidget(edit));

    QWidget *textAfter = new QLabel(")");
    textAfter->setStyleSheet("background-color: rgba(0,0,0,0); font-family: mono;");
    layout->addItem(scene()->addWidget(textAfter));

    _gw = new QGraphicsWidget();
    _gw->setLayout(layout);
    scene()->addItem(_gw);
    _gw->setParentItem(this);
    _gw->setPos(0,0);
}
