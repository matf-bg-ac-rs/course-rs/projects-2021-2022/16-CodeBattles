#include "VisualComponentItemBlock.hpp"

#include <QPainter>
#include <QGraphicsScene>

VisualComponentItemBlock::VisualComponentItemBlock(bool enabled)
    : VisualComponentItem(enabled)
    , _daBlock(nullptr)
    , _nextComponentBlock(nullptr)
{
    _bg = QColor::fromRgb(100,100,160);
    _bgTargeted = QColor::fromRgb(150,150,200);
}

void VisualComponentItemBlock::init()
{
    _daBlock = new DropArea(this);
    scene()->addItem(_daBlock);
    _daBlock->setPos(0,VisualComponentItem::getHeight() - _daBlock->boundingRect().height());
    _daBlock->setParentItem(this);

    VisualComponentItem::init();
}

QString VisualComponentItemBlock::getModelCode()
{
    QString res = "";
    if(_vc) {
        res += _vc->generateCode() + "\n";
    }
    res += "{\n";
    if(_nextComponentBlock) {
        res += _nextComponentBlock->getModelCode();
    }
    res += "}\n";
    if(nextComponent()) {
        res += nextComponent()->getModelCode();
    }
    return res;
}

void VisualComponentItemBlock::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

//    painter->setPen(QPen(Qt::blue,1));
//    painter->drawRect(boundingRect());

    painter->setPen(QPen(QColor::fromRgb(40,40,40),1));
    painter->setBrush(QBrush(_bg));

    if(_targeted) {
        painter->setBrush(QBrush(_bgTargeted));
    }

    QPointF points[21] = {
            QPointF(0, 0),

            QPointF(90,0),
            QPointF(100,5),
            QPointF(110,0),

            QPointF(getWidth(), 0),
            QPointF(getWidth(), VisualComponentItem::getHeight()-5),

            QPointF(120, VisualComponentItem::getHeight()-5),
            QPointF(110, VisualComponentItem::getHeight()),
            QPointF(100, VisualComponentItem::getHeight()-5),

            QPointF(10, VisualComponentItem::getHeight()-5),
            QPointF(10, VisualComponentItem::getHeight()-5 + getBlockHeight()),

            QPointF(100, VisualComponentItem::getHeight()-5 + getBlockHeight()),
            QPointF(110, VisualComponentItem::getHeight() + getBlockHeight()),
            QPointF(120, VisualComponentItem::getHeight()-5 + getBlockHeight()),

            QPointF(200, VisualComponentItem::getHeight()-5 + getBlockHeight()),
            QPointF(200, VisualComponentItem::getHeight()+10 + getBlockHeight()),

            QPointF(110, VisualComponentItem::getHeight()+10 + getBlockHeight()),
            QPointF(100, VisualComponentItem::getHeight()+15 + getBlockHeight()),
            QPointF(90, VisualComponentItem::getHeight()+10 + getBlockHeight()),

            QPointF(0, VisualComponentItem::getHeight()+10 + getBlockHeight()),

            QPointF(0, VisualComponentItem::getHeight()-5)
        };
    painter->drawConvexPolygon(points, 21);

    if(!_enabled) {
        painter->drawText(QRectF(10,0,getWidth()-20,VisualComponentItem::getHeight()), Qt::AlignVCenter, _vc->generateCode());
    }
}

void VisualComponentItemBlock::setNextComponent(VisualComponentItem *nextComponent, DropArea *nextArea)
{
    if(nextArea == _daBlock) {
        nextComponent->setNextComponent(_nextComponentBlock, nextComponent->da());
        nextComponent->setPrevComponent(nextArea);
        _nextComponentBlock = nextComponent;
        adjustNextComponentPosition();
        prepareAllRelevantGeometry();
    } else {
        VisualComponentItem::setNextComponent(nextComponent, nextArea);
    }
}

void VisualComponentItemBlock::removeNextComponent(DropArea *connectionArea)
{
    if(connectionArea == _daBlock) {
        _nextComponentBlock->removePrevComponent();
        _nextComponentBlock = nullptr;
        prepareAllRelevantGeometry();
    } else {
        VisualComponentItem::removeNextComponent(connectionArea);
    }
}

void VisualComponentItemBlock::adjustNextComponentPosition()
{
    VisualComponentItem::adjustNextComponentPosition();

    _daBlock->setPos(0,VisualComponentItem::getHeight() - _daBlock->boundingRect().height());
    if(_nextComponentBlock != nullptr) {
        _nextComponentBlock->setPos(QPointF(10,VisualComponentItem::getHeight()-5));
    }
}

qreal VisualComponentItemBlock::getBlockHeight() const
{
    qreal h = 0;
    VisualComponentItem *vci = _nextComponentBlock;
    while(vci != nullptr) {
        h += vci->getHeight() - 5;
        vci = vci->nextComponent();
    }
    return h>0 ? h : VisualComponentItem::getHeight() - 5;
}

qreal VisualComponentItemBlock::getHeight() const
{
    return VisualComponentItem::getHeight() + 15 + getBlockHeight();
}

DropArea *VisualComponentItemBlock::daBlock() const
{
    return _daBlock;
}
