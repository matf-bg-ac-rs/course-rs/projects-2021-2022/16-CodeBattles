#include "VisualGraphScene.hpp"
#include "components/VisualComponentItemStart.hpp"

#include <QDebug>

VisualGraphScene::VisualGraphScene(const QRectF &sceneRect, QObject *parent)
    : QGraphicsScene{sceneRect, parent}
{
    _start = new VisualComponentItemStart();
    addComponent(_start);
}

VisualGraphScene::~VisualGraphScene()
{
}

void VisualGraphScene::addComponent(VisualComponentItem *vci, qreal x, qreal y)
{
    this->addItem(vci);
    vci->setPos(x,y);
    vci->init();
}

VisualComponentItem *VisualGraphScene::start() const
{
    return _start;
}
