#include "VisualProgramming.hpp"
#include "model/components/VisualComponentFunctionCall.hpp"
#include "model/components/VisualComponentIf.hpp"
#include "model/components/VisualComponentWhile.hpp"
#include "model/components/VisualComponentVariableAssignment.hpp"

#include <QDebug>

VisualProgramming::VisualProgramming(QGraphicsView *gvGraphScene, QGraphicsView *gvComponentPicker)
    : _vgs(new VisualGraphScene(gvGraphScene->rect()))
    , _vcp(new VisualComponentPicker(gvComponentPicker->rect()))
{
    gvGraphScene->setScene(_vgs);
    gvComponentPicker->setScene(_vcp);

    components.append(new VisualComponentIf("condition"));
    components.append(new VisualComponentWhile("condition"));
    components.append(new VisualComponentVariableAssignment("variable","value"));
    components.append(new VisualComponentFunctionCall("cannon",{"angle"}));
    components.append(new VisualComponentFunctionCall("scan",{"angle","width"}));
    components.append(new VisualComponentFunctionCall("swim",{"angle","speed"}));
    components.append(new VisualComponentFunctionCall("position",{}));
    components.append(new VisualComponentFunctionCall("speed",{}));
    components.append(new VisualComponentFunctionCall("health",{}));
    components.append(new VisualComponentFunctionCall("stop",{}));
    components.append(new VisualComponent());

    _vcp->init(components, _vgs);
}

VisualProgramming::~VisualProgramming()
{
    delete _vgs;
    delete _vcp;
}

QString VisualProgramming::generateCode()
{
    return _vgs->start()->getModelCode();
}

VisualGraphScene *VisualProgramming::vgs() const
{
    return _vgs;
}

VisualComponentPicker *VisualProgramming::vcp() const
{
    return _vcp;
}
