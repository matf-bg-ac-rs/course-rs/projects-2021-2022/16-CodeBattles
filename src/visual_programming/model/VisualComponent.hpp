#ifndef VISUALCOMPONENT_HPP
#define VISUALCOMPONENT_HPP

#include <QString>
#include "../VisualProgramming.hpp"
class VisualComponentItem;

enum ComponentType {
    Single,
    Block,
    DoubleBlock
};

class VisualComponent
{
public:
    VisualComponent();
    virtual VisualComponent* clone();
    virtual ~VisualComponent();

    virtual QString generateCode();
    virtual VisualComponentItem* getViewItem();

    ComponentType componentType;
};

#endif // VISUALCOMPONENT_HPP
