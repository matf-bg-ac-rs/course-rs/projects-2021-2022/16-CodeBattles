#include "VisualComponentVariableAssignment.hpp"
#include "../../view/components/VisualComponentItemVariableAssignment.hpp"

VisualComponentVariableAssignment::VisualComponentVariableAssignment(QString variableName, QString variableValue)
    : VisualComponent()
    , _variableName(variableName)
    , _variableValue(variableValue)
{
    componentType = ComponentType::Single;
}

VisualComponentVariableAssignment* VisualComponentVariableAssignment::clone()
{
    VisualComponentVariableAssignment* ret = new VisualComponentVariableAssignment(_variableName,_variableValue);
    return ret;
}

QString VisualComponentVariableAssignment::generateCode()
{
    QString res(_variableName);
    res += " = ";
    res += _variableValue;
    res += ";";
    return res;
}

VisualComponentItem *VisualComponentVariableAssignment::getViewItem()
{
    VisualComponentItem *ret = new VisualComponentItemVariableAssignment();
    ret->_vc = clone();
    return ret;
}
