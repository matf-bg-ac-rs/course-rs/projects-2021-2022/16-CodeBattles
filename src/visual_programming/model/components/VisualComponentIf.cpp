#include "VisualComponentIf.hpp"
#include "../../view/components/VisualComponentItemIf.hpp"
#include "QDebug"

VisualComponentIf::VisualComponentIf(QString condition)
    : VisualComponent()
    , _condition(condition)
{
    componentType = ComponentType::Block;
}

VisualComponentIf* VisualComponentIf::clone()
{
    VisualComponentIf* ret = new VisualComponentIf(_condition);
    return ret;
}

QString VisualComponentIf::generateCode()
{
    return "if(" + _condition + ")";
}

VisualComponentItem *VisualComponentIf::getViewItem()
{
    VisualComponentItem *ret = new VisualComponentItemIf();
    ret->_vc = clone();
    return ret;
}
