#ifndef VISUALCOMPONENTIF_HPP
#define VISUALCOMPONENTIF_HPP

#include "../VisualComponent.hpp"

class VisualComponentIf : public VisualComponent
{
public:
    VisualComponentIf(QString condition);
    VisualComponentIf* clone() override;
    QString generateCode() override;

    VisualComponentItem* getViewItem() override;

    QString _condition;
};

#endif // VISUALCOMPONENTIF_HPP
