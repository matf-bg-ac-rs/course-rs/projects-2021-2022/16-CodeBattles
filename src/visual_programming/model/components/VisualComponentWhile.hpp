#ifndef VISUALCOMPONENTWHILE_HPP
#define VISUALCOMPONENTWHILE_HPP

#include "../VisualComponent.hpp"

class VisualComponentWhile : public VisualComponent
{
public:
    VisualComponentWhile(QString condition);
    VisualComponentWhile* clone() override;
    QString generateCode() override;

    VisualComponentItem* getViewItem() override;

    QString _condition;
};

#endif // VISUALCOMPONENTWHILE_HPP
