#ifndef VISUALCOMPONENTVARIABLEASSIGNMENT_HPP
#define VISUALCOMPONENTVARIABLEASSIGNMENT_HPP

#include "../VisualComponent.hpp"

class VisualComponentVariableAssignment : public VisualComponent
{
public:
    VisualComponentVariableAssignment(QString variableName, QString variableValue);
    VisualComponentVariableAssignment* clone() override;
    QString generateCode() override;

    VisualComponentItem* getViewItem() override;

    QString _variableName;
    QString _variableValue;
};

#endif // VISUALCOMPONENTVARIABLEASSIGNMENT_HPP
