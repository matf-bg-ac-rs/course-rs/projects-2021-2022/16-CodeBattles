#include "VisualComponentFunctionCall.hpp"
#include "../../view/components/VisualComponentItemFunctionCall.hpp"

VisualComponentFunctionCall::VisualComponentFunctionCall(QString functionName, QList<QString> functionArguments)
    : VisualComponent()
    , _functionName(functionName)
    , _functionArguments(functionArguments)
{
    componentType = ComponentType::Single;
}

VisualComponentFunctionCall* VisualComponentFunctionCall::clone()
{
    VisualComponentFunctionCall* ret = new VisualComponentFunctionCall(_functionName,_functionArguments);
    return ret;
}

QString VisualComponentFunctionCall::generateCode()
{
    QString res(_functionName);
    res += "(";
    res += _functionArguments.join(", ");
    res += ");";
    return res;
}

VisualComponentItem *VisualComponentFunctionCall::getViewItem()
{
    VisualComponentItem *ret = new VisualComponentItemFunctionCall();
    ret->_vc = clone();
    return ret;
}
