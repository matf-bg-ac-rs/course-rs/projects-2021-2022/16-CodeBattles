#include "VisualComponentWhile.hpp"
#include "../../view/components/VisualComponentItemWhile.hpp"

VisualComponentWhile::VisualComponentWhile(QString condition)
    : VisualComponent()
    , _condition(condition)
{
    componentType = ComponentType::Block;
}

VisualComponentWhile* VisualComponentWhile::clone()
{
    VisualComponentWhile* ret = new VisualComponentWhile(_condition);
    return ret;
}

QString VisualComponentWhile::generateCode()
{
    return "while(" + _condition + ")";
}

VisualComponentItem *VisualComponentWhile::getViewItem()
{
    VisualComponentItem *ret = new VisualComponentItemWhile();
    ret->_vc = clone();
    return ret;
}
