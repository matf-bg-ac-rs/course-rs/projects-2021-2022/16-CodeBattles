#ifndef VISUALCOMPONENTFUNCTIONCALL_HPP
#define VISUALCOMPONENTFUNCTIONCALL_HPP

#include "../VisualComponent.hpp"
#include <QList>

class VisualComponentFunctionCall : public VisualComponent
{
public:
    VisualComponentFunctionCall(QString functionName, QList<QString> functionArguments);
    VisualComponentFunctionCall* clone() override;
    QString generateCode() override;

    VisualComponentItem* getViewItem() override;

    QString _functionName;
    QStringList _functionArguments;
};

#endif // VISUALCOMPONENTFUNCTIONCALL_HPP
