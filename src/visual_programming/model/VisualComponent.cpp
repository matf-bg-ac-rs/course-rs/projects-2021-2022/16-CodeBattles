#include "VisualComponent.hpp"

VisualComponent::VisualComponent()
{

}

VisualComponent* VisualComponent::clone()
{
    VisualComponent *ret = new VisualComponent();
    ret->componentType = componentType;
    return ret;
}

VisualComponent::~VisualComponent()
{

}

QString VisualComponent::generateCode()
{
    return "";
}

VisualComponentItem* VisualComponent::getViewItem()
{
    VisualComponentItem *ret = new VisualComponentItem();
    ret->_vc = clone();
    return ret;
}
