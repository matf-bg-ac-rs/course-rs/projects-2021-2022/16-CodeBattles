#ifndef VISUALPROGRAMMING_HPP
#define VISUALPROGRAMMING_HPP

#include "model/VisualGraph.hpp"
#include "view/VisualGraphScene.hpp"
#include "view/VisualComponentPicker.hpp"

#include "model/VisualComponent.hpp"
#include "view/VisualComponentItem.hpp"

#include <QObject>
#include <QGraphicsView>
class VisualGraphScene;
class VisualComponentPicker;

class VisualProgramming : public QObject
{
public:
    VisualProgramming(QGraphicsView *gvGraphScene, QGraphicsView *gvComponentPicker);
    ~VisualProgramming();

    QString generateCode();

    VisualGraphScene *vgs() const;
    VisualComponentPicker *vcp() const;

private:
    VisualGraph *_vg;
    VisualGraphScene *_vgs;
    VisualComponentPicker *_vcp;

    QList<VisualComponent*> components;
};

#endif // VISUALPROGRAMMING_HPP
