# CodeBattles Programming Language

In order to program the robot for the game, we need a language to express
our programs in.

We've decided to design and implement a small programming language used for
programming the robots.

## Goals

This custom language has a few goals and non-goals.

Goals:
* **Simple:** The language isn't meant to be used outside of this application,
  and the current use case doesn't require complex language constructs.
* **Familiar:** The language should have syntax that looks like the popular
  programming languages. Having similar behaviour is preferable, as well.

Non-goals:
* **Creative:** The language doesn't need to be revolutionary, it just needs
  to get the job done.

## Look and feel

The language should feel similar to the language used by the inspiration for
this project ([Blockly Games](https://blockly.games/)), `JavaScript`, with
simplicity in mind.

Here are some of the constructs the language *should* support.

### Types

Only double-precision floating point numbers are supported.

Since there is only one data type, types aren't explicit.
### Variables

Variables can be assigned to like this

```js
variable = value;
```

Of course, their value can also be accessed using their name
in expressions.

All variables are global.

Variables need to be assigned to, before they are used.
### Expressions

The usual suite of operators is supported:

Arithmetic:
* Addition (`+`)
* Subtraction (`-`)
* Multiplication (`*`)
* Division (`/`)
* Remainder (`%`)

Relational:
* Equal (`==`)
* Not equal (`!=`)
* Less than (`<`)
* Greater than (`>`)
* Less than or equal (`<=`)
* Greater than or equal (`>=`)

Logic:
* NOT (`!`)
* AND (`&&`)
* OR (`||`)

Zero is a *falsey* value, the rest are *truthy*.

### Control flow

The language supports `if` and `while` statements.

### Interaction with the environment

A `print` statement is supported for debugging. It should evaluate
and print the value of an expression to the console.
