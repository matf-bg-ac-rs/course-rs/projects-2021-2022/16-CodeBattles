#!/usr/bin/env bash

pushd .
cd "${BASH_SOURCE%/*}/../src/language/grammar" || exit
antlr4 -Dlanguage=Cpp -no-listener -visitor *.g4 -o ../parser/generated
popd

