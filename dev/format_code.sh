#!/usr/bin/env bash

pushd .
cd "${BASH_SOURCE%/*}/.." || exit
find src/ \
    \( -name '*.c' \
    -o -name '*.cpp' \
    -o -name '*.h' \
    -o -name '*.hpp' \
    \) \
    -not -path '*/generated/*' \
    -exec clang-format -style=file -i {} \;
popd
